
| Name             | Version            |
| -----------------| -------------------|
| Java             | 1.8                |
| IntelliJ IDEA    | 2016.2.1           |
| Maven            | 4.0.0              |
| Spring Boot      | 1.4.0.RELEASE      |
| Spring Boot Data | 1.4.0.RELEASE      |
| JPA              | 2.1                |
| Hibernate        | 5.0.9.Final        |
| Zookeeper        | 3.4.8              |
| Kafka            | 0.10.0.0           |
| Kafka Manager    | 1.3.0.8            |
| Storm            | 0.10.1             |
| Storm Kafka      | 0.10.1             |
| Cassandra        | 3.0.3              |
| Solr             | 6.1.0              |
| Redis            | 2.3.3              |
| Tomcat           | 8                  |
| Ubuntu           | 16.04.1            |







