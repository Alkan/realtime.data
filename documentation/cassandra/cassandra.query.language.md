
###### connect Cassandra console client
```
cqlsh 192.168.100.55
cqlsh 213.243.38.122

cqlsh --request-timeout 500 192.168.100.55;
```

###### connect database 
```
use hurriyetemlak;
```

######  Create database 
```
CREATE KEYSPACE IF NOT EXISTS hurriyetemlak WITH replication = {'class':'SimpleStrategy', 'replication_factor':2};
CREATE KEYSPACE IF NOT EXISTS betahurriyetemlak WITH replication = {'class':'SimpleStrategy', 'replication_factor':1};
```

###### Delete Table
```
DROP TABLE realty_interaction_counts;
DROP TABLE realty_total_interaction_counts;
```

###### Row Table
```
DELETE FROM realty_total_interaction_counts WHERE realty_id= 22222389;
```

###### List all the tables 
```
describe tables;
desc table realty_total_interaction_counts
```

###### update script
```
UPDATE hurriyetemlak.realty_total_interaction_counts SET realty_visit_count = realty_visit_count + 1 WHERE realty_id=1;
UPDATE hurriyetemlak.realty_interaction_counts SET realty_visit_count = realty_visit_count + 1 WHERE realty_id=1 AND  realty_interaction_date = '2011-02-03';
```

###### Batc update script
```
BEGIN UNLOGGED BATCH
UPDATE realty_total_interaction_counts SET realty_visit_count = realty_visit_count + 16 WHERE realty_id=21440258;
UPDATE realty_interaction_counts SET realty_visit_count = realty_visit_count + 1 WHERE realty_id=2 AND realty_interaction_date = '2016-08-10';
APPLY BATCH;
```

###### select
```
SELECT * FROM hurriyetemlak.realty_total_interaction_counts  WHERE realty_id= 2;
SELECT * FROM realty_interaction_counts limit 10;
SELECT * FROM realty_total_interaction_counts limit 10;
SELECT * FROM realty_interaction_counts where realty_id = 2 AND  realty_interaction_date <= '2016-08-10';
SELECT * FROM realty_total_interaction_counts where realty_id = 30920;
SELECT * FROM realty_interaction_counts where realty_id = 22942903;

select count(*) from realty_interaction_counts

SELECT * FROM hurriyetemlak.realty_phone_click_log WHERE visit_date >= '2016-09-09' ALLOW FILTERING; //range query'lerinde allow filtering gerekir.
SELECT * FROM hurriyetemlak.realty_phone_click_log WHERE visit_date >= '2016-09-09' and visit_date <= '2016-09-10' ALLOW FILTERING;
```

###### insert
```
INSERT INTO hurriyetemlak.realty_phone_click_log (pk, realty_id, user_ip, session_id, device_type, visit_date) VALUES (uuid(), 1, '127.0.0.1', 'mySessionId', 'Android', '2016-09-09 11:25:14+0000');
```








