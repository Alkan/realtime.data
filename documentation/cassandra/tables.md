

######  Realty Total Interaction
```
CREATE TABLE hurriyetemlak.realty_total_interaction_counts(
   realty_visit_count counter,
   realty_message_count counter,
   realty_favorite_count counter,
   realty_id int,
   PRIMARY KEY (realty_id)
);
```

######  Realty Total Interaction
```
CREATE TABLE hurriyetemlak.realty_interaction_counts(
   realty_id int,
   realty_interaction_date date,
   realty_visit_count counter,
   realty_message_count counter,
   realty_favorite_count counter,
   PRIMARY KEY ((realty_id), realty_interaction_date )
)
WITH CLUSTERING ORDER BY (realty_interaction_date DESC);
```

######  Phone Click Log
```
create table hurriyetemlak.realty_phone_click_log(
	pk uuid,
	realty_id int,
	user_ip varchar,
	session_id varchar,
	device_type varchar,
	visit_date timestamp,
	PRIMARY KEY (pk)
);

CREATE INDEX ix_realty_id ON hurriyetemlak.realty_phone_click_log(realty_id);
CREATE INDEX ix_visit_date ON hurriyetemlak.realty_phone_click_log(visit_date);

```

######  Alter Table
```
ALTER TABLE hurriyetemlak.realty_interaction_counts ADD realty_phone_click_count counter;
ALTER TABLE hurriyetemlak.realty_total_interaction_counts ADD realty_phone_click_count counter;
```
