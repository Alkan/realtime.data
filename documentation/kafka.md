
##### Kafka Server Start
```
$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
```

##### Kafka Topic 
```
$KAFKA_HOME/bin/kafka-topics.sh --create --zookeeper 192.168.100.51:2181 --replication-factor 3 --partitions 3 --topic dev-realty-visit-topic-2
```

##### Kafka Topic Delete 
```
$KAFKA_HOME/bin/kafka-topics.sh --zookeeper 192.168.100.51:2181 --delete --topic dev-realty-visit-topic-1
```

##### Kafka Topic List
```
$KAFKA_HOME/bin/kafka-topics.sh --f --zookeeper  192.168.100.51:2181
```

##### Kafka Producer
```
$ $KAFKA_HOME/bin/kafka-console-producer.sh --zookeeper 192.168.100.51:2181 --topic dev-realty-visit-topic
$ $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list 192.168.100.51:9092 192.168.100.51:9092 192.168.100.51:9092 --topic dev-realty-visit-topic-3
```

##### Kafka Topic Describe
```
$ $KAFKA_HOME/bin/kafka-topics.sh --zookeeper localhost:2181 --describe --topic dev-realty-visit-topic-1
```

##### Kafka Consumer
```
$ $KAFKA_HOME/bin/kafka-console-consumer.sh --zookeeper 192.168.100.51:2181, 192.168.100.52:2181, 192.168.100.53:2181 --from-beginning --topic dev-phone-click-topic

$ $KAFKA_HOME/bin/kafka-run-class.sh kafka.tools.ConsumerOffsetChecker --zkconnect 213.243.38.108:2181,213.243.38.109:2181,213.243.38.110:2181 --group realty-visit-group

$ $KAFKA_HOME/bin/kafka-consumer-groups.sh --zookeeper 213.243.38.108:2181,213.243.38.109:2181,213.243.38.110:2181 --list
```