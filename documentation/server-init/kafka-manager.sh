author "Ahmet Esmer"
description "sadece ana server ustunde calisacak"
 
start on runlevel [2345]
stop on runlevel [^2345]
 
console log
chdir /home/developer
setuid developer
setgid developer
 
respawn
# respawn the job up to 20 times within a 5 second period.
respawn limit 20 5
 
exec /home/developer/kafka/kafka-manager-1.3.0.8/bin/kafka-manager

# sudo nano /etc/init/kafka-manager.conf
