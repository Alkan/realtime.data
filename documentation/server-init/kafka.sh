author "Ahmet Esmer"
description "start and stop storm supervisor"
 
start on runlevel [2345]
stop on runlevel [^2345]
 
console log
chdir /home/developer
setuid developer
setgid developer
 
respawn
# respawn the job up to 20 times within a 5 second period.
respawn limit 20 5
 
exec /home/developer/kafka/kafka_2.11-0.10.0.0/bin/kafka-server-start.sh /home/developer/kafka/kafka_2.11-0.10.0.0/config/server.properties

# sudo nano /etc/init/kafka.conf