author "Ahmet Esmer"
description "start and stop storm ui"
 
start on runlevel [2345]
stop on runlevel [^2345]
 
console log
chdir /home/developer
setuid developer
setgid developer
 
respawn
# respawn the job up to 20 times within a 5 second period.
respawn limit 20 5
 
exec /home/developer/storm/apache-storm-0.10.1/bin/storm logviewer

# sudo nano /etc/init/storm-logviewer.conf