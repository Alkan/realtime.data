author "Ahmet Esmer"
description "start and stop storm supervisor"

start on runlevel [2345]
stop on runlevel [^2345]

console log
chdir /home/developer
setuid developer
setgid developer

respawn
# respawn the job up to 20 times within a 5 second period.
respawn limit 20 5

exec /home/developer/zookeepers/zookeeper-3.4.8/bin/zkServer.sh start

# sudo nano /etc/init/zookeeper.conf
