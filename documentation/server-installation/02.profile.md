
##### Profile Export

```
$ nano ~/.profile
```

```
export KAFKA_HOME="/home/developer/kafka/kafka_2.11-0.10.0.0"
export ZK_HOME="/home/developer/zookeepers/zookeeper-3.4.8"
export STORM_HOME="/home/developer/storm/apache-storm-0.10.1"
export PATH=$PATH:$STORM_HOME/bin:$ZK_HOME/bin:$KAFKA_HOME/bin
```


_Kafka manager sadece ana sunucuda çalışacak_
Kafka Manager developer server üstünde kurulu 

```
export KAFKA_MANAGER_HOME="/home/developer/kafka/kafka_2.11-0.10.0.0/kafka-manager-1.3.0.8"
export PATH=$PATH:KAFKA_MANAGER_HOME/bin
```

