
###### Zookeeper Install

```
$ mkdir zookeepers
```

```
$ cd zookeepers
```

```
$ wget http://ftp.itu.edu.tr/Mirror/Apache/zookeeper/zookeeper-3.4.8/zookeeper-3.4.8.tar.gz 
```

```
$ tar xvzf zookeeper-3.4.8.tar.gz
```

```
$ mkdir zookeeper-3.4.8/data
```

###### Servers id File
```
SERVER_1$ echo "1" > zookeeper-3.4.8/data/myid
```
```
SERVER_2$ echo "2" > zookeeper-3.4.8/data/myid
```
```
SERVER_3$ echo "3" > zookeeper-3.4.8/data/myid
```

###### Servers Config
```
$ cp zookeeper-3.4.8/conf/zoo_sample.cfg zookeeper-3.4.8/conf/zoo.cfg 
```
```
$ nano $ZK_HOME/conf/zoo.cfg
```
 
```
clientPort=2181
dataDir=  /home/developer/zookeepers/zookeeper-3.4.8/data/
server.1=213.243.38.108:2888:3888
server.2=213.243.38.109:2888:3888
server.3=213.243.38.110:2888:3888
```

###### Log File
```
$ sudo mkdir /var/log/zookeeper
$ sudo chown developer:staff /var/log/zookeeper*
$ sudo chown developer:staff /var/log*
```
```
$ echo "ZOO_LOG_DIR=/var/log/zookeeper" > $ZK_HOME/conf/zookeeper-env.sh
```

###### Start
```
$ zkServer.sh start
$ zkServer.sh stop
$ zkServer.sh status
```



###### Folder log Size
```
autopurge.purgeInterval=1
du -hs $ZK_HOME/data/
```