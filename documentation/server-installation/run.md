
######  Tomcat
```
sudo systemctl start tomcat
sudo systemctl stop tomcat
sudo systemctl status tomcat
sudo systemctl restart tomcat

```

######  Zookeeper
```
zkServer.sh start
zkServer.sh stop
zkServer.sh status

```
###### _Zookeeper Alternative_
```
sudo {start,stop,status} zookeeper
```

###### Kafka
```
nohup kafka-server-start.sh $KAFKA_HOME/config/server.properties > ~/log/kafka.out 2>&1 &
```
###### _Kafka Alternative_
```
sudo {start,stop,status} kafka
sudo restart kafka
```


###### Storm
```
nohup storm nimbus > ~/log/nimbus.out 2>&1 &
nohup storm supervisor > ~/log/supervisor.out 2>&1 &
nohup storm ui > ~/log/storm-ui.out 2>&1 &
```

###### _Storm Alternative_
```
sudo {start,stop,status} nimbus
sudo {start,stop,status} supervisor
sudo {start,stop,status} storm-ui
sudo {start,stop,status} storm-logviewer
```

###### Kafka Manager
```
nohup kafka-manager  > ~/log/kafka-manager.out 2>&1 &
```

###### _Kafka Manager Alternative_
```
sudo {start,stop,status} kafka-manager
```


###### Kafka Offset Monitor
```
nohup java -cp KafkaOffsetMonitor-assembly-0.2.1.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk 192.168.100.51:2181,192.168.100.52:2181,192.168.100.53:2181 --port 9001 --refresh 10.seconds --retain 7.days &
```


###### Console Job
```

java -Dspring.profiles.active=dev -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=favorite-detail
java -Dspring.profiles.active=dev -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=message-detail
java -Dspring.profiles.active=dev -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=visit-detail

java -Dspring.profiles.active=prod -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=favorite-detail
java -Dspring.profiles.active=prod -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=message-detail
java -Dspring.profiles.active=prod -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=visit-detail

java -Dspring.profiles.active=prod -jar hurriyet-emlak-jobs-0.0.1-SNAPSHOT.jar --job=realty-solr --type=delta

```

