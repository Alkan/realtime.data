

##### Storm Command Line Parameters 

| Parameter Name | Valid Values      | Default Value    |
| -------------- | ----------------- |----------------- |
| -environment   | [dev, prod]       | prod             |
| -topology      | [local, server]   | server           |


##### Topology Deployment: Development

```
$ storm jar ~/storm-repo/storm-topology-realty-visit-1.0.jar com.hurriyet.emlak.storm.VisitTopology -environment dev 
$ storm jar ~/storm-repo/storm-topology-realty-message-1.0.jar com.hurriyet.emlak.storm.MessageTopology -environment dev 
$ storm jar ~/storm-repo/storm-topology-realty-favorite-1.0.jar com.hurriyet.emlak.storm.FavoriteTopology -environment dev 
$ storm jar ~/storm-repo/storm-topology-phone-click-1.0.jar com.hurriyet.emlak.storm.PhoneClickTopology -environment dev
```


##### Topology Deployment: Production
```
$ storm jar ~/storm-repo/storm-topology-realty-visit-1.0.jar com.hurriyet.emlak.storm.VisitTopology
$ storm jar ~/storm-repo/storm-topology-realty-message-1.0.jar com.hurriyet.emlak.storm.MessageTopology
$ storm jar ~/storm-repo/storm-topology-realty-favorite-1.0.jar com.hurriyet.emlak.storm.FavoriteTopology
$ storm jar ~/storm-repo/storm-topology-phone-click-1.0.jar com.hurriyet.emlak.storm.PhoneClickTopology
```
