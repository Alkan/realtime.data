#### Development and Production configuration settings

| Parameter Name                | Valid Values                   | Default Value    |
| ------------------------------| -------------------------------|----------------- |
| -Dspring.profiles.active      | [development, production]      | production       |


##### Local (IntelliJ) Development:
**Path**: _Edit configuration => Spring Boot => VM options_

```
-Dspring.profiles.active=development
```


##### Tomcat System Settings
```
$ sudo nano /opt/tomcat/bin/setenv.sh
```

**setenv.sh content** 

```
JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=development"
```


