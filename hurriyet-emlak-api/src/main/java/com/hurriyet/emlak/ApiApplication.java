package com.hurriyet.emlak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class ApiApplication extends SpringBootServletInitializer {
    /*Tomcat te çalışması için springboot un SpringBootServletInitializer extends edilmesi lazım */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApiApplication.class);
    }
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApiApplication.class, args);
    }

}


