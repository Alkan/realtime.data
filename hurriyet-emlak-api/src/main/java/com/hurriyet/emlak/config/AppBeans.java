package com.hurriyet.emlak.config;

import com.hurriyet.emlak.service.CassandraRealtyInteractionService;
import com.hurriyet.emlak.service.Impl.CassandraRealtyInteractionServiceImpl;
import com.hurriyet.emlak.service.KafkaProducerService;
import com.hurriyet.emlak.service.utility.CassandraSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;


@Configuration
public class AppBeans {

    @Autowired
    Environment environment;

    @Bean
    public KafkaProducerService initProducer() {
        return new com.hurriyet.emlak.service.Impl.KafkaProducerService(environment.getKafkaBroker(), environment.getKafkaAsync());
    }

    @Bean
    public CassandraSessionManager sessionManager() {
        return new CassandraSessionManager(environment.getCassandraContactPoints(), environment.getCassandraKeyspace());
    }


    @Bean
    public CassandraRealtyInteractionService realtyInteractionService() {
        return new CassandraRealtyInteractionServiceImpl(sessionManager().getSession());
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }
}
