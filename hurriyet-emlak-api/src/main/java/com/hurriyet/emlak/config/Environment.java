package com.hurriyet.emlak.config;

public class Environment {

    private String name;

    private String kafkaBroker;

    private String kafkaVisitTopicName;

    private String kafkaFavoriteTopicName;

    private String kafkaPhoneClickTopicName;

    private String kafkaMessageTopicName;

    private String cassandraContactPoints;

    private String cassandraPort;

    private String cassandraKeyspace;

    private boolean kafkaAsync;

    public Environment(String name,
                       String kafkaBroker,
                       boolean kafkaAsync,
                       String kafkaVisitTopicName,
                       String kafkaFavoriteTopicName,
                       String kafkaMessageTopicName,
                       String kafkaPhoneClickTopicName,
                       String cassandraContactPoints,
                       String cassandraPort,
                       String cassandraKeyspace) {

        this.setName(name);
        this.setKafkaBroker(kafkaBroker);
        this.setKafkaVisitTopicName(kafkaVisitTopicName);
        this.setKafkaFavoriteTopicName(kafkaFavoriteTopicName);
        this.setKafkaMessageTopicName(kafkaMessageTopicName);
        this.setKafkaPhoneClickTopicName(kafkaPhoneClickTopicName);
        this.setCassandraContactPoints(cassandraContactPoints);
        this.setCassandraPort(cassandraPort);
        this.setCassandraKeyspace(cassandraKeyspace);
        this.setKafkaAsync(kafkaAsync);
    }

    public String getKafkaPhoneClickTopicName() {
        return kafkaPhoneClickTopicName;
    }

    public void setKafkaPhoneClickTopicName(String kafkaPhoneClickTopicName) {
        this.kafkaPhoneClickTopicName = kafkaPhoneClickTopicName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKafkaBroker() {
        return kafkaBroker;
    }

    public void setKafkaBroker(String kafkaBroker) {
        this.kafkaBroker = kafkaBroker;
    }

    public String getKafkaVisitTopicName() {
        return kafkaVisitTopicName;
    }

    public void setKafkaVisitTopicName(String kafkaVisitTopicName) {
        this.kafkaVisitTopicName = kafkaVisitTopicName;
    }

    public String getKafkaFavoriteTopicName() {
        return kafkaFavoriteTopicName;
    }

    public void setKafkaFavoriteTopicName(String kafkaFavoriteTopicName) {
        this.kafkaFavoriteTopicName = kafkaFavoriteTopicName;
    }

    public String getKafkaMessageTopicName() {
        return kafkaMessageTopicName;
    }

    public void setKafkaMessageTopicName(String kafkaMessageTopicName) {
        this.kafkaMessageTopicName = kafkaMessageTopicName;
    }

    public String getCassandraContactPoints() {
        return cassandraContactPoints;
    }

    public void setCassandraContactPoints(String cassandraContactPoints) {
        this.cassandraContactPoints = cassandraContactPoints;
    }

    public String getCassandraPort() {
        return cassandraPort;
    }

    public void setCassandraPort(String cassandraPort) {
        this.cassandraPort = cassandraPort;
    }

    public String getCassandraKeyspace() {
        return cassandraKeyspace;
    }

    public void setCassandraKeyspace(String cassandraKeyspace) {
        this.cassandraKeyspace = cassandraKeyspace;
    }

    public boolean getKafkaAsync() {
        return kafkaAsync;
    }

    public void setKafkaAsync(boolean kafkaAsync) {
        this.kafkaAsync = kafkaAsync;
    }
}
