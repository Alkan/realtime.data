package com.hurriyet.emlak.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
public class EnvironmentConfig {

    @Value("${config.name}")
    String name;

    @Value("${kafka.broker.list}")
    String kafkaBroker;

    @Value("${kafka.visit.topic.name}")
    String kafkaVisitTopicName;

    @Value("${kafka.favorite.topic.name}")
    String kafkaFavoriteTopicName;

    @Value("${kafka.message.topic.name}")
    String kafkaMessageTopicName;

    @Value("${kafka.phone.click.topic.name}")
    String kafkaPhoneClickTopicName;

    @Value("${kafka.producer.async}")
    boolean kafkaAsync;

    @Value("${cassandra.contact.points}")
    String cassandraContactPoints;

    @Value("${cassandra.port}")
    String cassandraPort;

    @Value("${cassandra.keyspace}")
    String cassandraKeyspace;

    @Bean
    public Environment environment() {
        return new Environment(name, kafkaBroker, kafkaAsync, kafkaVisitTopicName, kafkaFavoriteTopicName, kafkaMessageTopicName, kafkaPhoneClickTopicName,
                cassandraContactPoints, cassandraPort, cassandraKeyspace);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
        Resource resource;
        String activeProfile;

        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer =  new PropertySourcesPlaceholderConfigurer();
        activeProfile = System.getProperty("spring.profiles.active");

        // choose different property files for different active profile
        if ("development".equals(activeProfile)) {
            resource = new ClassPathResource("application-dev.properties");
        } else {
            resource = new ClassPathResource("application-prod.properties");
        }

        // load the property file
        propertySourcesPlaceholderConfigurer.setLocation(resource);

        return propertySourcesPlaceholderConfigurer;
    }
}