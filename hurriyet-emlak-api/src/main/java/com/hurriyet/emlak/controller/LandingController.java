package com.hurriyet.emlak.controller;

import com.hurriyet.emlak.config.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Configuration
public class LandingController {

    @Autowired
    Environment environment;

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${logging.level.org.springframework}")
    private String loggingLevel;

    @Value("${debug}")
    private String debug;

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @Value("${spring.datasource.username}")
    private String datasourceUser;

    @Value("${spring.jpa.show-sql}")
    private String jpaShowSql;


    @RequestMapping(method = RequestMethod.GET)
    public String Landing() {

        StringBuilder env = new StringBuilder();
        env.append("<h2>Hurriyet Emlak Message Queue Api v0.0.7</h2>");
        env.append("<h3>Environment Config: " + environment.getName() +"</h3>");

        env.append("<h5>Spring Config:</h5>");
        env.append("spring.application.name = ");
        env.append(applicationName);
        env.append("</br>clogging.level.org.springframework = ");
        env.append(loggingLevel);
        env.append("</br>debug = ");
        env.append(debug);

        env.append("<h5>Kafka Config:</h5>");
        env.append("kafka.broker.list = ");
        env.append(environment.getKafkaBroker());
        env.append("</br>kafka.visit.topic.name = ");
        env.append(environment.getKafkaVisitTopicName());
        env.append("</br>kafka.favorite.topic.name = ");
        env.append(environment.getKafkaFavoriteTopicName());
        env.append("</br>kafka.message.topic.name = ");
        env.append(environment.getKafkaMessageTopicName());
        env.append("</br>kafka.phone.click.topic.name = ");
        env.append(environment.getKafkaPhoneClickTopicName());

        env.append("<h5>Cassandra Config:</h5>");
        env.append("cassandra.contact.points = ");
        env.append(environment.getCassandraContactPoints());
        env.append("</br>cassandra.port = ");
        env.append(environment.getCassandraPort());
        env.append("</br>cassandra.keyspace = ");
        env.append(environment.getCassandraKeyspace());

        env.append("<h5>Database Config:</h5>");
        env.append("spring.datasource.url = ");
        env.append(datasourceUrl);
        env.append("</br>spring.datasource.username  = ");
        env.append(datasourceUser);
        env.append("</br>spring.jpa.show-sql = ");
        env.append(jpaShowSql);


        return env.toString();
    }


}
