package com.hurriyet.emlak.controller;

import com.hurriyet.emlak.config.Environment;
import com.hurriyet.emlak.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

/**
 * Created by ydemir on 07.09.2016.
 */
@RestController
@RequestMapping("/phoneclick")
public class PhoneClickController {

    @Autowired
    Environment environment;

    @Autowired
    KafkaProducerService kafkaProducerService;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> sendPhoneClick(@RequestBody String postRawData) throws ExecutionException, InterruptedException{
        kafkaProducerService.send(environment.getKafkaPhoneClickTopicName(), postRawData);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}
