package com.hurriyet.emlak.controller;


import com.hurriyet.emlak.domain.model.RealtyInteraction;
import com.hurriyet.emlak.domain.model.RealtyTotalInteraction;
import com.hurriyet.emlak.model.ListByDayCountRequest;
import com.hurriyet.emlak.model.RealtyInteractionRequest;
import com.hurriyet.emlak.service.CassandraRealtyInteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/realtyinteraction")
@Validated
public class RealtyInteractionController {

    @Autowired
    CassandraRealtyInteractionService cassandraRealtyInteractionService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<List<RealtyTotalInteraction>> getList(@RequestBody RealtyInteractionRequest requestModel) {
        return new ResponseEntity<>(cassandraRealtyInteractionService.getList(requestModel.getRealtyIdList()), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/list")
    public ResponseEntity<List<RealtyInteraction>> getListByDayCountPost(@RequestBody @Valid ListByDayCountRequest requestModel){
        return ResponseEntity.ok(cassandraRealtyInteractionService.getListByDate(requestModel.getRealtyId(), requestModel.getDayCount()));
    }

}
