package com.hurriyet.emlak.controller;

import com.hurriyet.emlak.config.Environment;
import com.hurriyet.emlak.service.KafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping("/realtymessage")
public class RealtyMessageController {

    @Autowired
    Environment environment;

    @Autowired
    KafkaProducerService kafkaProducerService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> sendRealtyVisit(@RequestBody String postRawData)  throws ExecutionException, InterruptedException{

        kafkaProducerService.send(environment.getKafkaMessageTopicName(), postRawData);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    /*@RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity<String>> sendRealtyMessage(@RequestBody String postRawData) {

        Runnable runnable;
        runnable = () -> {
            KafkaSenderService kafkaSender = new KafkaSenderService();
            kafkaSender.sendKafkaMessage(postRawData, environment.getKafkaMessageTopicName());
        };

        Thread thread = new Thread(runnable);
        thread.start();

        return () -> new ResponseEntity<>("OK", HttpStatus.OK);
    }*/

}
