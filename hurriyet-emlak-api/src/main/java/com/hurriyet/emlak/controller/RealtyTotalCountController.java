package com.hurriyet.emlak.controller;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;


@RestController
@RequestMapping("/realtytotalcount")

public class RealtyTotalCountController {

    private final AtomicLong counter = new AtomicLong();
    @RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity<String>> sendRealtyVisit(@RequestBody String postRawData) {
        HttpHeaders responseHeaders = new HttpHeaders();
        Runnable runnable = () -> {

        };

        Thread thread = new Thread(runnable);
        thread.start();
        String numberAsString = String.valueOf( counter.incrementAndGet());
        responseHeaders.set("requestCount", numberAsString);
        return () -> new ResponseEntity<>("OK", responseHeaders, HttpStatus.OK);
    }
}
