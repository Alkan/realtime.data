package com.hurriyet.emlak.controller;

import com.hurriyet.emlak.config.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import com.hurriyet.emlak.service.*;


@RestController
@RequestMapping("/realtyvisit")
public class RealtyVisitController {

    @Autowired
    Environment environment;

    @Autowired
    KafkaProducerService kafkaProducerService;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> sendRealtyVisit(@RequestBody String postRawData)  throws ExecutionException, InterruptedException{

        kafkaProducerService.send(environment.getKafkaVisitTopicName(), postRawData);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }


    /*@RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity<String>> sendRealtyVisit(@RequestBody String postRawData) {

        Runnable runnable;
        runnable = () -> {
            KafkaSenderService kafkaSender = new KafkaSenderService();
            kafkaSender.sendKafkaMessage(postRawData, environment.getKafkaVisitTopicName());
        };

        Thread thread = new Thread(runnable);
        thread.start();

        return () -> new ResponseEntity<>("OK", HttpStatus.OK);
    }*/

}
