package com.hurriyet.emlak.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by ydemir on 28.09.2016.
 */

public class ListByDayCountRequest{

    public ListByDayCountRequest() {
    }

    private int realtyId;
    @Min(value = 1, message = "Min : 1")
    @Max(value = 90, message = "Max : 90")
    private int dayCount;

    public ListByDayCountRequest(int realtyId, int dayCount) {
        this.realtyId = realtyId;
        this.dayCount = dayCount;
    }

    public int getRealtyId() {
        return realtyId;
    }

    public void setRealtyId(int realtyId) {
        this.realtyId = realtyId;
    }

    public int getDayCount() {
        return dayCount;
    }

    public void setDayCount(int dayCount) {
        this.dayCount = dayCount;
    }
}