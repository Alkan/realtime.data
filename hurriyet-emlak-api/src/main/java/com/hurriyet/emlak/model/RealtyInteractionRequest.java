package com.hurriyet.emlak.model;

import java.util.List;

/**
 * Created by ydemir on 26.07.2016.
 */
public class RealtyInteractionRequest {
    private List<Integer> realtyIdList;

    public List<Integer> getRealtyIdList() {
        return realtyIdList;
    }

    public void setRealtyIdList(List<Integer> realtyIdList) {
        this.realtyIdList = realtyIdList;
    }
    public RealtyInteractionRequest(){

    }
    public RealtyInteractionRequest(List<Integer>realtyIdList){
        setRealtyIdList(realtyIdList);
    }
}
