package com.hurriyet.emlak.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Entity
@Table(name = "tblRealty")
public class RealtyEntity {
    private int realtyId;
    private Integer realtyCategoryId;
    private Integer realtySubCategoryId;
    private Integer realtyNo;
    private String realtyTitle;
    private String realtyDescription;
    private String realtyKeywords;
    private BigDecimal realtyPrice;
    private Timestamp realtyStartDateTime;
    private Timestamp realtyEndDateTime;
    private Integer realtyPublishId;
    private String realtyPlace;
    private String realtyAddress;
    private Boolean realtyAddressShow;
    private Integer realtyResidenceId;
    private String realtyTimeShareName;
    private Integer realtyTimeShareTerm;
    private Integer realtyRoom;
    private Integer realtyLivingRoom;
    private Integer realtyBathroom;
    private Integer realtyBedCount;
    private Integer realtyStarId;
    private String realtyLandMunicipality;
    private String realtyLandIsland;
    private String realtyLandParcel;
    private Integer realtySqm;
    private BigDecimal realtySqmPrice;
    private Integer realtySqmIndoor;
    private Integer realtySqmOutdoor;
    private Integer realtyFloorCount;
    private Integer realtyFloorId;
    private Integer realtyAge;
    private Integer realtyHeatingId;
    private Integer realtyFuelId;
    private Integer realtyBuildId;
    private Integer realtyBuildStateId;
    private Integer realtyUsageId;
    private Integer realtyCreditId;
    private BigDecimal realtyDeposit;
    private BigDecimal realtyFee;
    private Boolean realtyIsStudentOrSingle;
    private Boolean realtyIsFlatReceived;
    private Boolean realtyIsBarter;
    private Boolean realtyByTransfer;
    private String realtySideIDs;
    private Integer realtyRegisterId;
    private Boolean realtyIsPopulated;
    private BigDecimal realtyRental;
    private Boolean realtyIsFeatured;
    private Integer realtyActivateId;
    private Integer realtyCloseId;
    private String realtyCloseDescription;
    private String realtyProjectName;
    private String realtyProjectSlogan;
    private Integer realtyTotalProjectArea;
    private Integer realtyTotalHouseCount;
    private Timestamp realtyProjectCompletionDateTime;
    private boolean realtyIsDeleted;
    private int realtyCreatedUserId;
    private Timestamp realtyCreatedDateTime;
    private Integer realtyUpdatedUserId;
    private Timestamp realtyUpdatedDateTime;
    private Integer realtyAdvertiseOwnerId;
    private BigDecimal realtyMapLatitude;
    private BigDecimal realtyMapLongtitude;
    private Integer realtyMapZoomLevel;
    private Integer realtyRoomCountId;
    private Boolean realtyPriceShow;
    private String realtyContactName;
    private String realtyContactPhone1;
    private String realtyContactPhone2;
    private Boolean realtyUseContactInfo;
    private Boolean realtyShowEys;
    private String realtyProjectAbout;
    private BigDecimal realtySortOrder;
    private Timestamp realtyBatchUpdatedDateTime;
    private String realtyContactPhone3;
    private String realtyContactPhone4;
    private String realtyProjectCompletionString;
    private Integer realtyFlatReceivedId;
    private Integer realtyBarterId;
    private Integer realtyNewLocationId;
    private Integer realtyImagePrinted;
    private String realtyAreaIDs;
    private String realtyClientIp;
    private Boolean realtyIsHousingComplex;
    private Integer realtyHousingComplexId;
    private String realtyHousingComplexName;
    private Boolean realtyIsBid;
    private Boolean realtyIsCityTransformation;
    private Integer realtyDormitoryCapacity;
    private String realtyDormitoryRoomCapacityIDs;
    private String realtyDormitoryRoomPriceRangeIDs;
    private Boolean realtyIsAuthorizedRealtor;
    private Integer realtyOldLocationId;
    private String realtyOldAreaIDs;
    private Integer realtyTempNewLocationId;
    private String realtyTempNewAreaIDs;

    @Id
    @Column(name = "RealtyID")
    public int getRealtyId() {
        return realtyId;
    }

    public void setRealtyId(int realtyId) {
        this.realtyId = realtyId;
    }

    @Basic
    @Column(name = "RealtyCategoryID")
    public Integer getRealtyCategoryId() {
        return realtyCategoryId;
    }

    public void setRealtyCategoryId(Integer realtyCategoryId) {
        this.realtyCategoryId = realtyCategoryId;
    }

    @Basic
    @Column(name = "RealtySubCategoryID")
    public Integer getRealtySubCategoryId() {
        return realtySubCategoryId;
    }

    public void setRealtySubCategoryId(Integer realtySubCategoryId) {
        this.realtySubCategoryId = realtySubCategoryId;
    }

    @Basic
    @Column(name = "RealtyNo")
    public Integer getRealtyNo() {
        return realtyNo;
    }

    public void setRealtyNo(Integer realtyNo) {
        this.realtyNo = realtyNo;
    }

    @Basic
    @Column(name = "RealtyTitle")
    public String getRealtyTitle() {
        return realtyTitle;
    }

    public void setRealtyTitle(String realtyTitle) {
        this.realtyTitle = realtyTitle;
    }

    @Basic
    @Column(name = "RealtyDescription")
    public String getRealtyDescription() {
        return realtyDescription;
    }

    public void setRealtyDescription(String realtyDescription) {
        this.realtyDescription = realtyDescription;
    }

    @Basic
    @Column(name = "RealtyKeywords")
    public String getRealtyKeywords() {
        return realtyKeywords;
    }

    public void setRealtyKeywords(String realtyKeywords) {
        this.realtyKeywords = realtyKeywords;
    }

    @Basic
    @Column(name = "RealtyPrice")
    public BigDecimal getRealtyPrice() {
        return realtyPrice;
    }

    public void setRealtyPrice(BigDecimal realtyPrice) {
        this.realtyPrice = realtyPrice;
    }

    @Basic
    @Column(name = "RealtyStartDateTime")
    public Timestamp getRealtyStartDateTime() {
        return realtyStartDateTime;
    }

    public void setRealtyStartDateTime(Timestamp realtyStartDateTime) {
        this.realtyStartDateTime = realtyStartDateTime;
    }

    @Basic
    @Column(name = "RealtyEndDateTime")
    public Timestamp getRealtyEndDateTime() {
        return realtyEndDateTime;
    }

    public void setRealtyEndDateTime(Timestamp realtyEndDateTime) {
        this.realtyEndDateTime = realtyEndDateTime;
    }

    @Basic
    @Column(name = "RealtyPublishID")
    public Integer getRealtyPublishId() {
        return realtyPublishId;
    }

    public void setRealtyPublishId(Integer realtyPublishId) {
        this.realtyPublishId = realtyPublishId;
    }

    @Basic
    @Column(name = "RealtyPlace")
    public String getRealtyPlace() {
        return realtyPlace;
    }

    public void setRealtyPlace(String realtyPlace) {
        this.realtyPlace = realtyPlace;
    }

    @Basic
    @Column(name = "RealtyAddress")
    public String getRealtyAddress() {
        return realtyAddress;
    }

    public void setRealtyAddress(String realtyAddress) {
        this.realtyAddress = realtyAddress;
    }

    @Basic
    @Column(name = "RealtyAddressShow")
    public Boolean getRealtyAddressShow() {
        return realtyAddressShow;
    }

    public void setRealtyAddressShow(Boolean realtyAddressShow) {
        this.realtyAddressShow = realtyAddressShow;
    }

    @Basic
    @Column(name = "RealtyResidenceID")
    public Integer getRealtyResidenceId() {
        return realtyResidenceId;
    }

    public void setRealtyResidenceId(Integer realtyResidenceId) {
        this.realtyResidenceId = realtyResidenceId;
    }

    @Basic
    @Column(name = "RealtyTimeShareName")
    public String getRealtyTimeShareName() {
        return realtyTimeShareName;
    }

    public void setRealtyTimeShareName(String realtyTimeShareName) {
        this.realtyTimeShareName = realtyTimeShareName;
    }

    @Basic
    @Column(name = "RealtyTimeShareTerm")
    public Integer getRealtyTimeShareTerm() {
        return realtyTimeShareTerm;
    }

    public void setRealtyTimeShareTerm(Integer realtyTimeShareTerm) {
        this.realtyTimeShareTerm = realtyTimeShareTerm;
    }

    @Basic
    @Column(name = "RealtyRoom")
    public Integer getRealtyRoom() {
        return realtyRoom;
    }

    public void setRealtyRoom(Integer realtyRoom) {
        this.realtyRoom = realtyRoom;
    }

    @Basic
    @Column(name = "RealtyLivingRoom")
    public Integer getRealtyLivingRoom() {
        return realtyLivingRoom;
    }

    public void setRealtyLivingRoom(Integer realtyLivingRoom) {
        this.realtyLivingRoom = realtyLivingRoom;
    }

    @Basic
    @Column(name = "RealtyBathroom")
    public Integer getRealtyBathroom() {
        return realtyBathroom;
    }

    public void setRealtyBathroom(Integer realtyBathroom) {
        this.realtyBathroom = realtyBathroom;
    }

    @Basic
    @Column(name = "RealtyBedCount")
    public Integer getRealtyBedCount() {
        return realtyBedCount;
    }

    public void setRealtyBedCount(Integer realtyBedCount) {
        this.realtyBedCount = realtyBedCount;
    }

    @Basic
    @Column(name = "RealtyStarID")
    public Integer getRealtyStarId() {
        return realtyStarId;
    }

    public void setRealtyStarId(Integer realtyStarId) {
        this.realtyStarId = realtyStarId;
    }

    @Basic
    @Column(name = "RealtyLandMunicipality")
    public String getRealtyLandMunicipality() {
        return realtyLandMunicipality;
    }

    public void setRealtyLandMunicipality(String realtyLandMunicipality) {
        this.realtyLandMunicipality = realtyLandMunicipality;
    }

    @Basic
    @Column(name = "RealtyLandIsland")
    public String getRealtyLandIsland() {
        return realtyLandIsland;
    }

    public void setRealtyLandIsland(String realtyLandIsland) {
        this.realtyLandIsland = realtyLandIsland;
    }

    @Basic
    @Column(name = "RealtyLandParcel")
    public String getRealtyLandParcel() {
        return realtyLandParcel;
    }

    public void setRealtyLandParcel(String realtyLandParcel) {
        this.realtyLandParcel = realtyLandParcel;
    }

    @Basic
    @Column(name = "RealtySqm")
    public Integer getRealtySqm() {
        return realtySqm;
    }

    public void setRealtySqm(Integer realtySqm) {
        this.realtySqm = realtySqm;
    }

    @Basic
    @Column(name = "RealtySqmPrice")
    public BigDecimal getRealtySqmPrice() {
        return realtySqmPrice;
    }

    public void setRealtySqmPrice(BigDecimal realtySqmPrice) {
        this.realtySqmPrice = realtySqmPrice;
    }

    @Basic
    @Column(name = "RealtySqmIndoor")
    public Integer getRealtySqmIndoor() {
        return realtySqmIndoor;
    }

    public void setRealtySqmIndoor(Integer realtySqmIndoor) {
        this.realtySqmIndoor = realtySqmIndoor;
    }

    @Basic
    @Column(name = "RealtySqmOutdoor")
    public Integer getRealtySqmOutdoor() {
        return realtySqmOutdoor;
    }

    public void setRealtySqmOutdoor(Integer realtySqmOutdoor) {
        this.realtySqmOutdoor = realtySqmOutdoor;
    }

    @Basic
    @Column(name = "RealtyFloorCount")
    public Integer getRealtyFloorCount() {
        return realtyFloorCount;
    }

    public void setRealtyFloorCount(Integer realtyFloorCount) {
        this.realtyFloorCount = realtyFloorCount;
    }

    @Basic
    @Column(name = "RealtyFloorID")
    public Integer getRealtyFloorId() {
        return realtyFloorId;
    }

    public void setRealtyFloorId(Integer realtyFloorId) {
        this.realtyFloorId = realtyFloorId;
    }

    @Basic
    @Column(name = "RealtyAge")
    public Integer getRealtyAge() {
        return realtyAge;
    }

    public void setRealtyAge(Integer realtyAge) {
        this.realtyAge = realtyAge;
    }

    @Basic
    @Column(name = "RealtyHeatingID")
    public Integer getRealtyHeatingId() {
        return realtyHeatingId;
    }

    public void setRealtyHeatingId(Integer realtyHeatingId) {
        this.realtyHeatingId = realtyHeatingId;
    }

    @Basic
    @Column(name = "RealtyFuelID")
    public Integer getRealtyFuelId() {
        return realtyFuelId;
    }

    public void setRealtyFuelId(Integer realtyFuelId) {
        this.realtyFuelId = realtyFuelId;
    }

    @Basic
    @Column(name = "RealtyBuildID")
    public Integer getRealtyBuildId() {
        return realtyBuildId;
    }

    public void setRealtyBuildId(Integer realtyBuildId) {
        this.realtyBuildId = realtyBuildId;
    }

    @Basic
    @Column(name = "RealtyBuildStateID")
    public Integer getRealtyBuildStateId() {
        return realtyBuildStateId;
    }

    public void setRealtyBuildStateId(Integer realtyBuildStateId) {
        this.realtyBuildStateId = realtyBuildStateId;
    }

    @Basic
    @Column(name = "RealtyUsageID")
    public Integer getRealtyUsageId() {
        return realtyUsageId;
    }

    public void setRealtyUsageId(Integer realtyUsageId) {
        this.realtyUsageId = realtyUsageId;
    }

    @Basic
    @Column(name = "RealtyCreditID")
    public Integer getRealtyCreditId() {
        return realtyCreditId;
    }

    public void setRealtyCreditId(Integer realtyCreditId) {
        this.realtyCreditId = realtyCreditId;
    }

    @Basic
    @Column(name = "RealtyDeposit")
    public BigDecimal getRealtyDeposit() {
        return realtyDeposit;
    }

    public void setRealtyDeposit(BigDecimal realtyDeposit) {
        this.realtyDeposit = realtyDeposit;
    }

    @Basic
    @Column(name = "RealtyFee")
    public BigDecimal getRealtyFee() {
        return realtyFee;
    }

    public void setRealtyFee(BigDecimal realtyFee) {
        this.realtyFee = realtyFee;
    }

    @Basic
    @Column(name = "RealtyIsStudentOrSingle")
    public Boolean getRealtyIsStudentOrSingle() {
        return realtyIsStudentOrSingle;
    }

    public void setRealtyIsStudentOrSingle(Boolean realtyIsStudentOrSingle) {
        this.realtyIsStudentOrSingle = realtyIsStudentOrSingle;
    }

    @Basic
    @Column(name = "RealtyIsFlatReceived")
    public Boolean getRealtyIsFlatReceived() {
        return realtyIsFlatReceived;
    }

    public void setRealtyIsFlatReceived(Boolean realtyIsFlatReceived) {
        this.realtyIsFlatReceived = realtyIsFlatReceived;
    }

    @Basic
    @Column(name = "RealtyIsBarter")
    public Boolean getRealtyIsBarter() {
        return realtyIsBarter;
    }

    public void setRealtyIsBarter(Boolean realtyIsBarter) {
        this.realtyIsBarter = realtyIsBarter;
    }

    @Basic
    @Column(name = "RealtyByTransfer")
    public Boolean getRealtyByTransfer() {
        return realtyByTransfer;
    }

    public void setRealtyByTransfer(Boolean realtyByTransfer) {
        this.realtyByTransfer = realtyByTransfer;
    }

    @Basic
    @Column(name = "RealtySideIDs")
    public String getRealtySideIDs() {
        return realtySideIDs;
    }

    public void setRealtySideIDs(String realtySideIDs) {
        this.realtySideIDs = realtySideIDs;
    }

    @Basic
    @Column(name = "RealtyRegisterID")
    public Integer getRealtyRegisterId() {
        return realtyRegisterId;
    }

    public void setRealtyRegisterId(Integer realtyRegisterId) {
        this.realtyRegisterId = realtyRegisterId;
    }

    @Basic
    @Column(name = "RealtyIsPopulated")
    public Boolean getRealtyIsPopulated() {
        return realtyIsPopulated;
    }

    public void setRealtyIsPopulated(Boolean realtyIsPopulated) {
        this.realtyIsPopulated = realtyIsPopulated;
    }

    @Basic
    @Column(name = "RealtyRental")
    public BigDecimal getRealtyRental() {
        return realtyRental;
    }

    public void setRealtyRental(BigDecimal realtyRental) {
        this.realtyRental = realtyRental;
    }

    @Basic
    @Column(name = "RealtyIsFeatured")
    public Boolean getRealtyIsFeatured() {
        return realtyIsFeatured;
    }

    public void setRealtyIsFeatured(Boolean realtyIsFeatured) {
        this.realtyIsFeatured = realtyIsFeatured;
    }

    @Basic
    @Column(name = "RealtyActivateID")
    public Integer getRealtyActivateId() {
        return realtyActivateId;
    }

    public void setRealtyActivateId(Integer realtyActivateId) {
        this.realtyActivateId = realtyActivateId;
    }

    @Basic
    @Column(name = "RealtyCloseID")
    public Integer getRealtyCloseId() {
        return realtyCloseId;
    }

    public void setRealtyCloseId(Integer realtyCloseId) {
        this.realtyCloseId = realtyCloseId;
    }

    @Basic
    @Column(name = "RealtyCloseDescription")
    public String getRealtyCloseDescription() {
        return realtyCloseDescription;
    }

    public void setRealtyCloseDescription(String realtyCloseDescription) {
        this.realtyCloseDescription = realtyCloseDescription;
    }

    @Basic
    @Column(name = "RealtyProjectName")
    public String getRealtyProjectName() {
        return realtyProjectName;
    }

    public void setRealtyProjectName(String realtyProjectName) {
        this.realtyProjectName = realtyProjectName;
    }

    @Basic
    @Column(name = "RealtyProjectSlogan")
    public String getRealtyProjectSlogan() {
        return realtyProjectSlogan;
    }

    public void setRealtyProjectSlogan(String realtyProjectSlogan) {
        this.realtyProjectSlogan = realtyProjectSlogan;
    }

    @Basic
    @Column(name = "RealtyTotalProjectArea")
    public Integer getRealtyTotalProjectArea() {
        return realtyTotalProjectArea;
    }

    public void setRealtyTotalProjectArea(Integer realtyTotalProjectArea) {
        this.realtyTotalProjectArea = realtyTotalProjectArea;
    }

    @Basic
    @Column(name = "RealtyTotalHouseCount")
    public Integer getRealtyTotalHouseCount() {
        return realtyTotalHouseCount;
    }

    public void setRealtyTotalHouseCount(Integer realtyTotalHouseCount) {
        this.realtyTotalHouseCount = realtyTotalHouseCount;
    }

    @Basic
    @Column(name = "RealtyProjectCompletionDateTime")
    public Timestamp getRealtyProjectCompletionDateTime() {
        return realtyProjectCompletionDateTime;
    }

    public void setRealtyProjectCompletionDateTime(Timestamp realtyProjectCompletionDateTime) {
        this.realtyProjectCompletionDateTime = realtyProjectCompletionDateTime;
    }

    @Basic
    @Column(name = "RealtyIsDeleted")
    public boolean isRealtyIsDeleted() {
        return realtyIsDeleted;
    }

    public void setRealtyIsDeleted(boolean realtyIsDeleted) {
        this.realtyIsDeleted = realtyIsDeleted;
    }

    @Basic
    @Column(name = "RealtyCreatedUserID")
    public int getRealtyCreatedUserId() {
        return realtyCreatedUserId;
    }

    public void setRealtyCreatedUserId(int realtyCreatedUserId) {
        this.realtyCreatedUserId = realtyCreatedUserId;
    }

    @Basic
    @Column(name = "RealtyCreatedDateTime")
    public Timestamp getRealtyCreatedDateTime() {
        return realtyCreatedDateTime;
    }

    public void setRealtyCreatedDateTime(Timestamp realtyCreatedDateTime) {
        this.realtyCreatedDateTime = realtyCreatedDateTime;
    }

    @Basic
    @Column(name = "RealtyUpdatedUserID")
    public Integer getRealtyUpdatedUserId() {
        return realtyUpdatedUserId;
    }

    public void setRealtyUpdatedUserId(Integer realtyUpdatedUserId) {
        this.realtyUpdatedUserId = realtyUpdatedUserId;
    }

    @Basic
    @Column(name = "RealtyUpdatedDateTime")
    public Timestamp getRealtyUpdatedDateTime() {
        return realtyUpdatedDateTime;
    }

    public void setRealtyUpdatedDateTime(Timestamp realtyUpdatedDateTime) {
        this.realtyUpdatedDateTime = realtyUpdatedDateTime;
    }

    @Basic
    @Column(name = "RealtyAdvertiseOwnerID")
    public Integer getRealtyAdvertiseOwnerId() {
        return realtyAdvertiseOwnerId;
    }

    public void setRealtyAdvertiseOwnerId(Integer realtyAdvertiseOwnerId) {
        this.realtyAdvertiseOwnerId = realtyAdvertiseOwnerId;
    }

    @Basic
    @Column(name = "RealtyMapLatitude")
    public BigDecimal getRealtyMapLatitude() {
        return realtyMapLatitude;
    }

    public void setRealtyMapLatitude(BigDecimal realtyMapLatitude) {
        this.realtyMapLatitude = realtyMapLatitude;
    }

    @Basic
    @Column(name = "RealtyMapLongtitude")
    public BigDecimal getRealtyMapLongtitude() {
        return realtyMapLongtitude;
    }

    public void setRealtyMapLongtitude(BigDecimal realtyMapLongtitude) {
        this.realtyMapLongtitude = realtyMapLongtitude;
    }

    @Basic
    @Column(name = "RealtyMapZoomLevel")
    public Integer getRealtyMapZoomLevel() {
        return realtyMapZoomLevel;
    }

    public void setRealtyMapZoomLevel(Integer realtyMapZoomLevel) {
        this.realtyMapZoomLevel = realtyMapZoomLevel;
    }

    @Basic
    @Column(name = "RealtyRoomCountID")
    public Integer getRealtyRoomCountId() {
        return realtyRoomCountId;
    }

    public void setRealtyRoomCountId(Integer realtyRoomCountId) {
        this.realtyRoomCountId = realtyRoomCountId;
    }

    @Basic
    @Column(name = "RealtyPriceShow")
    public Boolean getRealtyPriceShow() {
        return realtyPriceShow;
    }

    public void setRealtyPriceShow(Boolean realtyPriceShow) {
        this.realtyPriceShow = realtyPriceShow;
    }

    @Basic
    @Column(name = "RealtyContactName")
    public String getRealtyContactName() {
        return realtyContactName;
    }

    public void setRealtyContactName(String realtyContactName) {
        this.realtyContactName = realtyContactName;
    }

    @Basic
    @Column(name = "RealtyContactPhone1")
    public String getRealtyContactPhone1() {
        return realtyContactPhone1;
    }

    public void setRealtyContactPhone1(String realtyContactPhone1) {
        this.realtyContactPhone1 = realtyContactPhone1;
    }

    @Basic
    @Column(name = "RealtyContactPhone2")
    public String getRealtyContactPhone2() {
        return realtyContactPhone2;
    }

    public void setRealtyContactPhone2(String realtyContactPhone2) {
        this.realtyContactPhone2 = realtyContactPhone2;
    }

    @Basic
    @Column(name = "RealtyUseContactInfo")
    public Boolean getRealtyUseContactInfo() {
        return realtyUseContactInfo;
    }

    public void setRealtyUseContactInfo(Boolean realtyUseContactInfo) {
        this.realtyUseContactInfo = realtyUseContactInfo;
    }

    @Basic
    @Column(name = "RealtyShowEys")
    public Boolean getRealtyShowEys() {
        return realtyShowEys;
    }

    public void setRealtyShowEys(Boolean realtyShowEys) {
        this.realtyShowEys = realtyShowEys;
    }

    @Basic
    @Column(name = "RealtyProjectAbout")
    public String getRealtyProjectAbout() {
        return realtyProjectAbout;
    }

    public void setRealtyProjectAbout(String realtyProjectAbout) {
        this.realtyProjectAbout = realtyProjectAbout;
    }

    @Basic
    @Column(name = "RealtySortOrder")
    public BigDecimal getRealtySortOrder() {
        return realtySortOrder;
    }

    public void setRealtySortOrder(BigDecimal realtySortOrder) {
        this.realtySortOrder = realtySortOrder;
    }

    @Basic
    @Column(name = "RealtyBatchUpdatedDateTime")
    public Timestamp getRealtyBatchUpdatedDateTime() {
        return realtyBatchUpdatedDateTime;
    }

    public void setRealtyBatchUpdatedDateTime(Timestamp realtyBatchUpdatedDateTime) {
        this.realtyBatchUpdatedDateTime = realtyBatchUpdatedDateTime;
    }

    @Basic
    @Column(name = "RealtyContactPhone3")
    public String getRealtyContactPhone3() {
        return realtyContactPhone3;
    }

    public void setRealtyContactPhone3(String realtyContactPhone3) {
        this.realtyContactPhone3 = realtyContactPhone3;
    }

    @Basic
    @Column(name = "RealtyContactPhone4")
    public String getRealtyContactPhone4() {
        return realtyContactPhone4;
    }

    public void setRealtyContactPhone4(String realtyContactPhone4) {
        this.realtyContactPhone4 = realtyContactPhone4;
    }

    @Basic
    @Column(name = "RealtyProjectCompletionString")
    public String getRealtyProjectCompletionString() {
        return realtyProjectCompletionString;
    }

    public void setRealtyProjectCompletionString(String realtyProjectCompletionString) {
        this.realtyProjectCompletionString = realtyProjectCompletionString;
    }

    @Basic
    @Column(name = "RealtyFlatReceivedID")
    public Integer getRealtyFlatReceivedId() {
        return realtyFlatReceivedId;
    }

    public void setRealtyFlatReceivedId(Integer realtyFlatReceivedId) {
        this.realtyFlatReceivedId = realtyFlatReceivedId;
    }

    @Basic
    @Column(name = "RealtyBarterID")
    public Integer getRealtyBarterId() {
        return realtyBarterId;
    }

    public void setRealtyBarterId(Integer realtyBarterId) {
        this.realtyBarterId = realtyBarterId;
    }

    @Basic
    @Column(name = "RealtyNewLocationID")
    public Integer getRealtyNewLocationId() {
        return realtyNewLocationId;
    }

    public void setRealtyNewLocationId(Integer realtyNewLocationId) {
        this.realtyNewLocationId = realtyNewLocationId;
    }

    @Basic
    @Column(name = "RealtyImagePrinted")
    public Integer getRealtyImagePrinted() {
        return realtyImagePrinted;
    }

    public void setRealtyImagePrinted(Integer realtyImagePrinted) {
        this.realtyImagePrinted = realtyImagePrinted;
    }

    @Basic
    @Column(name = "RealtyAreaIDs")
    public String getRealtyAreaIDs() {
        return realtyAreaIDs;
    }

    public void setRealtyAreaIDs(String realtyAreaIDs) {
        this.realtyAreaIDs = realtyAreaIDs;
    }

    @Basic
    @Column(name = "RealtyClientIP")
    public String getRealtyClientIp() {
        return realtyClientIp;
    }

    public void setRealtyClientIp(String realtyClientIp) {
        this.realtyClientIp = realtyClientIp;
    }

    @Basic
    @Column(name = "RealtyIsHousingComplex")
    public Boolean getRealtyIsHousingComplex() {
        return realtyIsHousingComplex;
    }

    public void setRealtyIsHousingComplex(Boolean realtyIsHousingComplex) {
        this.realtyIsHousingComplex = realtyIsHousingComplex;
    }

    @Basic
    @Column(name = "RealtyHousingComplexID")
    public Integer getRealtyHousingComplexId() {
        return realtyHousingComplexId;
    }

    public void setRealtyHousingComplexId(Integer realtyHousingComplexId) {
        this.realtyHousingComplexId = realtyHousingComplexId;
    }

    @Basic
    @Column(name = "RealtyHousingComplexName")
    public String getRealtyHousingComplexName() {
        return realtyHousingComplexName;
    }

    public void setRealtyHousingComplexName(String realtyHousingComplexName) {
        this.realtyHousingComplexName = realtyHousingComplexName;
    }

    @Basic
    @Column(name = "RealtyIsBid")
    public Boolean getRealtyIsBid() {
        return realtyIsBid;
    }

    public void setRealtyIsBid(Boolean realtyIsBid) {
        this.realtyIsBid = realtyIsBid;
    }

    @Basic
    @Column(name = "RealtyIsCityTransformation")
    public Boolean getRealtyIsCityTransformation() {
        return realtyIsCityTransformation;
    }

    public void setRealtyIsCityTransformation(Boolean realtyIsCityTransformation) {
        this.realtyIsCityTransformation = realtyIsCityTransformation;
    }

    @Basic
    @Column(name = "RealtyDormitoryCapacity")
    public Integer getRealtyDormitoryCapacity() {
        return realtyDormitoryCapacity;
    }

    public void setRealtyDormitoryCapacity(Integer realtyDormitoryCapacity) {
        this.realtyDormitoryCapacity = realtyDormitoryCapacity;
    }

    @Basic
    @Column(name = "RealtyDormitoryRoomCapacityIDs")
    public String getRealtyDormitoryRoomCapacityIDs() {
        return realtyDormitoryRoomCapacityIDs;
    }

    public void setRealtyDormitoryRoomCapacityIDs(String realtyDormitoryRoomCapacityIDs) {
        this.realtyDormitoryRoomCapacityIDs = realtyDormitoryRoomCapacityIDs;
    }

    @Basic
    @Column(name = "RealtyDormitoryRoomPriceRangeIDs")
    public String getRealtyDormitoryRoomPriceRangeIDs() {
        return realtyDormitoryRoomPriceRangeIDs;
    }

    public void setRealtyDormitoryRoomPriceRangeIDs(String realtyDormitoryRoomPriceRangeIDs) {
        this.realtyDormitoryRoomPriceRangeIDs = realtyDormitoryRoomPriceRangeIDs;
    }

    @Basic
    @Column(name = "RealtyIsAuthorizedRealtor")
    public Boolean getRealtyIsAuthorizedRealtor() {
        return realtyIsAuthorizedRealtor;
    }

    public void setRealtyIsAuthorizedRealtor(Boolean realtyIsAuthorizedRealtor) {
        this.realtyIsAuthorizedRealtor = realtyIsAuthorizedRealtor;
    }

    @Basic
    @Column(name = "RealtyOldLocationID")
    public Integer getRealtyOldLocationId() {
        return realtyOldLocationId;
    }

    public void setRealtyOldLocationId(Integer realtyOldLocationId) {
        this.realtyOldLocationId = realtyOldLocationId;
    }

    @Basic
    @Column(name = "RealtyOldAreaIDs")
    public String getRealtyOldAreaIDs() {
        return realtyOldAreaIDs;
    }

    public void setRealtyOldAreaIDs(String realtyOldAreaIDs) {
        this.realtyOldAreaIDs = realtyOldAreaIDs;
    }

    @Basic
    @Column(name = "RealtyTempNewLocationID")
    public Integer getRealtyTempNewLocationId() {
        return realtyTempNewLocationId;
    }

    public void setRealtyTempNewLocationId(Integer realtyTempNewLocationId) {
        this.realtyTempNewLocationId = realtyTempNewLocationId;
    }

    @Basic
    @Column(name = "RealtyTempNewAreaIDs")
    public String getRealtyTempNewAreaIDs() {
        return realtyTempNewAreaIDs;
    }

    public void setRealtyTempNewAreaIDs(String realtyTempNewAreaIDs) {
        this.realtyTempNewAreaIDs = realtyTempNewAreaIDs;
    }
 
}
