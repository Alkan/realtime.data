package com.hurriyet.emlak.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Entity
@Table(name = "tblAddChoose")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "spRealtyFavoriteTotal",
                procedureName = "spRealtyFavoriteTotal",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "CreatedDate", type = String.class)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "spRealtyFavoriteDetail",
                procedureName = "spRealtyFavoriteDetail",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "StartDate", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "EndDate", type = String.class)
                }
        )
})
public class RealtyFavoriteEntity {
    private int addChooseId;
    private Integer addChooseFirmUserId;
    private boolean addChooseIsSelected;
    private boolean addChooseIsDeleted;
    private int addChooseCreatedUserId;
    private Timestamp addChooseCreatedDate;
    private Integer addChooseUpdatedUserId;
    private Timestamp addChooseUpdatedDate;
    private String addChooseNote;
    private BigDecimal addChooseRealtyPrice;
    private Integer addChooseRealtyPriceCurrencyId;

    @Id
    @Column(name = "AddChooseID")
    public int getAddChooseId() {
        return addChooseId;
    }

    public void setAddChooseId(int addChooseId) {
        this.addChooseId = addChooseId;
    }

    @Basic
    @Column(name = "AddChooseFirmUserID")
    public Integer getAddChooseFirmUserId() {
        return addChooseFirmUserId;
    }

    public void setAddChooseFirmUserId(Integer addChooseFirmUserId) {
        this.addChooseFirmUserId = addChooseFirmUserId;
    }

    @Basic
    @Column(name = "AddChooseIsSelected")
    public boolean isAddChooseIsSelected() {
        return addChooseIsSelected;
    }

    public void setAddChooseIsSelected(boolean addChooseIsSelected) {
        this.addChooseIsSelected = addChooseIsSelected;
    }

    @Basic
    @Column(name = "AddChooseIsDeleted")
    public boolean isAddChooseIsDeleted() {
        return addChooseIsDeleted;
    }

    public void setAddChooseIsDeleted(boolean addChooseIsDeleted) {
        this.addChooseIsDeleted = addChooseIsDeleted;
    }

    @Basic
    @Column(name = "AddChooseCreatedUserID")
    public int getAddChooseCreatedUserId() {
        return addChooseCreatedUserId;
    }

    public void setAddChooseCreatedUserId(int addChooseCreatedUserId) {
        this.addChooseCreatedUserId = addChooseCreatedUserId;
    }

    @Basic
    @Column(name = "AddChooseCreatedDate")
    public Timestamp getAddChooseCreatedDate() {
        return addChooseCreatedDate;
    }

    public void setAddChooseCreatedDate(Timestamp addChooseCreatedDate) {
        this.addChooseCreatedDate = addChooseCreatedDate;
    }

    @Basic
    @Column(name = "AddChooseUpdatedUserID")
    public Integer getAddChooseUpdatedUserId() {
        return addChooseUpdatedUserId;
    }

    public void setAddChooseUpdatedUserId(Integer addChooseUpdatedUserId) {
        this.addChooseUpdatedUserId = addChooseUpdatedUserId;
    }

    @Basic
    @Column(name = "AddChooseUpdatedDate")
    public Timestamp getAddChooseUpdatedDate() {
        return addChooseUpdatedDate;
    }

    public void setAddChooseUpdatedDate(Timestamp addChooseUpdatedDate) {
        this.addChooseUpdatedDate = addChooseUpdatedDate;
    }

    @Basic
    @Column(name = "AddChooseNote")
    public String getAddChooseNote() {
        return addChooseNote;
    }

    public void setAddChooseNote(String addChooseNote) {
        this.addChooseNote = addChooseNote;
    }

    @Basic
    @Column(name = "AddChooseRealtyPrice")
    public BigDecimal getAddChooseRealtyPrice() {
        return addChooseRealtyPrice;
    }

    public void setAddChooseRealtyPrice(BigDecimal addChooseRealtyPrice) {
        this.addChooseRealtyPrice = addChooseRealtyPrice;
    }

    @Basic
    @Column(name = "AddChooseRealtyPriceCurrencyID")
    public Integer getAddChooseRealtyPriceCurrencyId() {
        return addChooseRealtyPriceCurrencyId;
    }

    public void setAddChooseRealtyPriceCurrencyId(Integer addChooseRealtyPriceCurrencyId) {
        this.addChooseRealtyPriceCurrencyId = addChooseRealtyPriceCurrencyId;
    }

}
