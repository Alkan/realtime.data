package com.hurriyet.emlak.domain.entity;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "tblFirmUserMessage")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "spRealtyMessageTotal",
                procedureName = "spRealtyMessageTotal",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "CreatedDate", type = String.class)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "spRealtyMessageDetail",
                procedureName = "spRealtyMessageDetail",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "StartDate", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "EndDate", type = String.class)
                }
        )
})
public class RealtyMessageEntity {
    private int firmUserMessageId;
    private int firmUserMessageFromFirmUserId;
    private Integer firmUserMessageRelationId;
    private Integer firmUserMessageReplyId;
    private boolean firmUserMessageIsRead;
    private boolean firmUserMessageIsAdmin;
    private String firmUserMessageSubject;
    private String firmUserMessageText;
    private Integer firmUserMessageRealtyId;
    private boolean firmUserMessageIsDeleted;
    private Integer firmUserMessageCreatedUserId;
    private Timestamp firmUserMessageCreatedDateTime;
    private Integer firmUserMessageUpdatedUserId;
    private Timestamp firmUserMessageUpdatedDateTime;
    private String firmUserMessagePhone;
    private String firmUserMessageEmail;
    private String firmUserMessageSenderIp;

    @Id
    @Column(name = "FirmUserMessageID")
    public int getFirmUserMessageId() {
        return firmUserMessageId;
    }

    public void setFirmUserMessageId(int firmUserMessageId) {
        this.firmUserMessageId = firmUserMessageId;
    }

    @Basic
    @Column(name = "FirmUserMessageFromFirmUserID")
    public int getFirmUserMessageFromFirmUserId() {
        return firmUserMessageFromFirmUserId;
    }

    public void setFirmUserMessageFromFirmUserId(int firmUserMessageFromFirmUserId) {
        this.firmUserMessageFromFirmUserId = firmUserMessageFromFirmUserId;
    }

    @Basic
    @Column(name = "FirmUserMessageRelationID")
    public Integer getFirmUserMessageRelationId() {
        return firmUserMessageRelationId;
    }

    public void setFirmUserMessageRelationId(Integer firmUserMessageRelationId) {
        this.firmUserMessageRelationId = firmUserMessageRelationId;
    }

    @Basic
    @Column(name = "FirmUserMessageReplyID")
    public Integer getFirmUserMessageReplyId() {
        return firmUserMessageReplyId;
    }

    public void setFirmUserMessageReplyId(Integer firmUserMessageReplyId) {
        this.firmUserMessageReplyId = firmUserMessageReplyId;
    }

    @Basic
    @Column(name = "FirmUserMessageIsRead")
    public boolean isFirmUserMessageIsRead() {
        return firmUserMessageIsRead;
    }

    public void setFirmUserMessageIsRead(boolean firmUserMessageIsRead) {
        this.firmUserMessageIsRead = firmUserMessageIsRead;
    }

    @Basic
    @Column(name = "FirmUserMessageIsAdmin")
    public boolean isFirmUserMessageIsAdmin() {
        return firmUserMessageIsAdmin;
    }

    public void setFirmUserMessageIsAdmin(boolean firmUserMessageIsAdmin) {
        this.firmUserMessageIsAdmin = firmUserMessageIsAdmin;
    }

    @Basic
    @Column(name = "FirmUserMessageSubject")
    public String getFirmUserMessageSubject() {
        return firmUserMessageSubject;
    }

    public void setFirmUserMessageSubject(String firmUserMessageSubject) {
        this.firmUserMessageSubject = firmUserMessageSubject;
    }

    @Basic
    @Column(name = "FirmUserMessageText")
    public String getFirmUserMessageText() {
        return firmUserMessageText;
    }

    public void setFirmUserMessageText(String firmUserMessageText) {
        this.firmUserMessageText = firmUserMessageText;
    }

    @Basic
    @Column(name = "FirmUserMessageRealtyID")
    public Integer getFirmUserMessageRealtyId() {
        return firmUserMessageRealtyId;
    }

    public void setFirmUserMessageRealtyId(Integer firmUserMessageRealtyId) {
        this.firmUserMessageRealtyId = firmUserMessageRealtyId;
    }

    @Basic
    @Column(name = "FirmUserMessageIsDeleted")
    public boolean isFirmUserMessageIsDeleted() {
        return firmUserMessageIsDeleted;
    }

    public void setFirmUserMessageIsDeleted(boolean firmUserMessageIsDeleted) {
        this.firmUserMessageIsDeleted = firmUserMessageIsDeleted;
    }

    @Basic
    @Column(name = "FirmUserMessageCreatedUserID")
    public Integer getFirmUserMessageCreatedUserId() {
        return firmUserMessageCreatedUserId;
    }

    public void setFirmUserMessageCreatedUserId(Integer firmUserMessageCreatedUserId) {
        this.firmUserMessageCreatedUserId = firmUserMessageCreatedUserId;
    }

    @Basic
    @Column(name = "FirmUserMessageCreatedDateTime")
    public Timestamp getFirmUserMessageCreatedDateTime() {
        return firmUserMessageCreatedDateTime;
    }

    public void setFirmUserMessageCreatedDateTime(Timestamp firmUserMessageCreatedDateTime) {
        this.firmUserMessageCreatedDateTime = firmUserMessageCreatedDateTime;
    }

    @Basic
    @Column(name = "FirmUserMessageUpdatedUserID")
    public Integer getFirmUserMessageUpdatedUserId() {
        return firmUserMessageUpdatedUserId;
    }

    public void setFirmUserMessageUpdatedUserId(Integer firmUserMessageUpdatedUserId) {
        this.firmUserMessageUpdatedUserId = firmUserMessageUpdatedUserId;
    }

    @Basic
    @Column(name = "FirmUserMessageUpdatedDateTime")
    public Timestamp getFirmUserMessageUpdatedDateTime() {
        return firmUserMessageUpdatedDateTime;
    }

    public void setFirmUserMessageUpdatedDateTime(Timestamp firmUserMessageUpdatedDateTime) {
        this.firmUserMessageUpdatedDateTime = firmUserMessageUpdatedDateTime;
    }

    @Basic
    @Column(name = "FirmUserMessagePhone")
    public String getFirmUserMessagePhone() {
        return firmUserMessagePhone;
    }

    public void setFirmUserMessagePhone(String firmUserMessagePhone) {
        this.firmUserMessagePhone = firmUserMessagePhone;
    }

    @Basic
    @Column(name = "FirmUserMessageEmail")
    public String getFirmUserMessageEmail() {
        return firmUserMessageEmail;
    }

    public void setFirmUserMessageEmail(String firmUserMessageEmail) {
        this.firmUserMessageEmail = firmUserMessageEmail;
    }

    @Basic
    @Column(name = "FirmUserMessageSenderIp")
    public String getFirmUserMessageSenderIp() {
        return firmUserMessageSenderIp;
    }

    public void setFirmUserMessageSenderIp(String firmUserMessageSenderIp) {
        this.firmUserMessageSenderIp = firmUserMessageSenderIp;
    }

}
