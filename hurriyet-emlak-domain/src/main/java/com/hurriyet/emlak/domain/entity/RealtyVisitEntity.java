package com.hurriyet.emlak.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
@Table(name = "tblRealtyVisit")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "spRealtyVisitTotal",
                procedureName = "spRealtyVisitTotal",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "CreatedDate", type = String.class)
                }
        ),
        @NamedStoredProcedureQuery(
                name = "spRealtyVisitDetail",
                procedureName = "spRealtyVisitDetail",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyStartID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "RealtyEndID", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "StartDate", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "EndDate", type = String.class)
                }
        )
})
public class RealtyVisitEntity implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "RealtyVisitID")
    private int realtyVisitId;

    @Column(name = "RealtyVisitRealtyID")
    private Integer realtyVisitRealtyId;

    @Column(name = "RealtyVisitVisitDate")
    private Timestamp realtyVisitVisitDate;

    @Column(name = "RealtyVisitVisitCount")
    private Long realtyVisitVisitCount;

    @Column(name = "RealtyVisitIsDeleted")
    private boolean realtyVisitIsDeleted;

    @Column(name = "RealtyVisitCreatedUserID")
    private Integer realtyVisitCreatedUserId;

    @Column(name = "RealtyVisitCreatedDateTime")
    private Timestamp realtyVisitCreatedDateTime;

    @Column(name = "RealtyVisitUpdatedUserID")
    private Integer realtyVisitUpdatedUserId;

    @Column(name = "RealtyVisitUpdatedDateTime")
    private Timestamp realtyVisitUpdatedDateTime;


    public int getRealtyVisitId() {
        return realtyVisitId;
    }

    public void setRealtyVisitId(int realtyVisitId) {
        this.realtyVisitId = realtyVisitId;
    }

    public Integer getRealtyVisitRealtyId() {
        return realtyVisitRealtyId;
    }

    public void setRealtyVisitRealtyId(Integer realtyVisitRealtyId) {
        this.realtyVisitRealtyId = realtyVisitRealtyId;
    }

    public Timestamp getRealtyVisitVisitDate() {
        return realtyVisitVisitDate;
    }

    public void setRealtyVisitVisitDate(Timestamp realtyVisitVisitDate) {
        this.realtyVisitVisitDate = realtyVisitVisitDate;
    }

    public Long getRealtyVisitVisitCount() {
        return realtyVisitVisitCount;
    }

    public void setRealtyVisitVisitCount(Long realtyVisitVisitCount) {
        this.realtyVisitVisitCount = realtyVisitVisitCount;
    }

    public boolean isRealtyVisitIsDeleted() {
        return realtyVisitIsDeleted;
    }

    public void setRealtyVisitIsDeleted(boolean realtyVisitIsDeleted) {
        this.realtyVisitIsDeleted = realtyVisitIsDeleted;
    }

    public Integer getRealtyVisitCreatedUserId() {
        return realtyVisitCreatedUserId;
    }

    public void setRealtyVisitCreatedUserId(Integer realtyVisitCreatedUserId) {
        this.realtyVisitCreatedUserId = realtyVisitCreatedUserId;
    }

    public Timestamp getRealtyVisitCreatedDateTime() {
        return realtyVisitCreatedDateTime;
    }

    public void setRealtyVisitCreatedDateTime(Timestamp realtyVisitCreatedDateTime) {
        this.realtyVisitCreatedDateTime = realtyVisitCreatedDateTime;
    }

    public Integer getRealtyVisitUpdatedUserId() {
        return realtyVisitUpdatedUserId;
    }

    public void setRealtyVisitUpdatedUserId(Integer realtyVisitUpdatedUserId) {
        this.realtyVisitUpdatedUserId = realtyVisitUpdatedUserId;
    }


    public Timestamp getRealtyVisitUpdatedDateTime() {
        return realtyVisitUpdatedDateTime;
    }

    public void setRealtyVisitUpdatedDateTime(Timestamp realtyVisitUpdatedDateTime) {
        this.realtyVisitUpdatedDateTime = realtyVisitUpdatedDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealtyVisitEntity that = (RealtyVisitEntity) o;

        if (realtyVisitId != that.realtyVisitId) return false;
        if (realtyVisitIsDeleted != that.realtyVisitIsDeleted) return false;
        if (realtyVisitRealtyId != null ? !realtyVisitRealtyId.equals(that.realtyVisitRealtyId) : that.realtyVisitRealtyId != null)
            return false;
        if (realtyVisitVisitDate != null ? !realtyVisitVisitDate.equals(that.realtyVisitVisitDate) : that.realtyVisitVisitDate != null)
            return false;
        if (realtyVisitVisitCount != null ? !realtyVisitVisitCount.equals(that.realtyVisitVisitCount) : that.realtyVisitVisitCount != null)
            return false;
        if (realtyVisitCreatedUserId != null ? !realtyVisitCreatedUserId.equals(that.realtyVisitCreatedUserId) : that.realtyVisitCreatedUserId != null)
            return false;
        if (realtyVisitCreatedDateTime != null ? !realtyVisitCreatedDateTime.equals(that.realtyVisitCreatedDateTime) : that.realtyVisitCreatedDateTime != null)
            return false;
        if (realtyVisitUpdatedUserId != null ? !realtyVisitUpdatedUserId.equals(that.realtyVisitUpdatedUserId) : that.realtyVisitUpdatedUserId != null)
            return false;
        if (realtyVisitUpdatedDateTime != null ? !realtyVisitUpdatedDateTime.equals(that.realtyVisitUpdatedDateTime) : that.realtyVisitUpdatedDateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = realtyVisitId;
        result = 31 * result + (realtyVisitRealtyId != null ? realtyVisitRealtyId.hashCode() : 0);
        result = 31 * result + (realtyVisitVisitDate != null ? realtyVisitVisitDate.hashCode() : 0);
        result = 31 * result + (realtyVisitVisitCount != null ? realtyVisitVisitCount.hashCode() : 0);
        result = 31 * result + (realtyVisitIsDeleted ? 1 : 0);
        result = 31 * result + (realtyVisitCreatedUserId != null ? realtyVisitCreatedUserId.hashCode() : 0);
        result = 31 * result + (realtyVisitCreatedDateTime != null ? realtyVisitCreatedDateTime.hashCode() : 0);
        result = 31 * result + (realtyVisitUpdatedUserId != null ? realtyVisitUpdatedUserId.hashCode() : 0);
        result = 31 * result + (realtyVisitUpdatedDateTime != null ? realtyVisitUpdatedDateTime.hashCode() : 0);
        return result;
    }
}
