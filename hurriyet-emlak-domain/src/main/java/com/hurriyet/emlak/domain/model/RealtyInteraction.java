package com.hurriyet.emlak.domain.model;

import java.time.LocalDate;

/**
 * Created by ydemir on 28.09.2016.
 */
public class RealtyInteraction {
    private int realtyId;
    private long realtyVisitCount;
    private long realtyMessageCount;
    private long realtyFavoriteCount;
    private long realtyPhoneClickCount;
    private String realtyDate;

    public RealtyInteraction(int realtyId, long realtyVisitCount, long realtyMessageCount, long realtyFavoriteCount, long realtyPhoneClickCount, String realtyDate) {
        this.realtyId = realtyId;
        this.realtyVisitCount = realtyVisitCount;
        this.realtyMessageCount = realtyMessageCount;
        this.realtyFavoriteCount = realtyFavoriteCount;
        this.realtyPhoneClickCount = realtyPhoneClickCount;
        this.realtyDate = realtyDate;
    }
    public int getRealtyId() {
        return realtyId;
    }

    public void setRealtyId(int realtyId) {
        this.realtyId = realtyId;
    }

    public long getRealtyVisitCount() {
        return realtyVisitCount;
    }

    public void setRealtyVisitCount(long realtyVisitCount) {
        this.realtyVisitCount = realtyVisitCount;
    }

    public long getRealtyMessageCount() {
        return realtyMessageCount;
    }

    public void setRealtyMessageCount(long realtyMessageCount) {
        this.realtyMessageCount = realtyMessageCount;
    }

    public long getRealtyFavoriteCount() {
        return realtyFavoriteCount;
    }

    public void setRealtyFavoriteCount(long realtyFavoriteCount) {
        this.realtyFavoriteCount = realtyFavoriteCount;
    }

    public long getRealtyPhoneClickCount() {
        return realtyPhoneClickCount;
    }

    public void setRealtyPhoneClickCount(long realtyPhoneClickCount) {
        this.realtyPhoneClickCount = realtyPhoneClickCount;
    }

    public String getRealtyDate() {
        return realtyDate;
    }

    public void setRealtyDate(String realtyDate) {
        this.realtyDate = realtyDate;
    }


}
