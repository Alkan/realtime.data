package com.hurriyet.emlak.domain.model;


public class RealtyTotalInteraction  {

    private int realtyId;
    private long realtyVisitCount;
    private long realtyMessageCount;
    private long realtyFavoriteCount;

    public RealtyTotalInteraction(int id, long visitCount, long messageCount, long favoriteCount) {
        this.setRealtyId(id);
        this.setRealtyVisitCount(visitCount);
        this.setRealtyMessageCount(messageCount);
        this.setRealtyFavoriteCount(favoriteCount);
    }

    public int getRealtyId() {
        return realtyId;
    }

    public void setRealtyId(int realtyId) {
        this.realtyId = realtyId;
    }

    public long getRealtyVisitCount() {
        return realtyVisitCount;
    }

    public void setRealtyVisitCount(long realtyVisitCount) {
        this.realtyVisitCount = realtyVisitCount;
    }

    public long getRealtyMessageCount() {
        return realtyMessageCount;
    }

    public void setRealtyMessageCount(long realtyMessageCount) {
        this.realtyMessageCount = realtyMessageCount;
    }

    public long getRealtyFavoriteCount() {
        return realtyFavoriteCount;
    }

    public void setRealtyFavoriteCount(long realtyFavoriteCount) {
        this.realtyFavoriteCount = realtyFavoriteCount;
    }
}
