package com.hurriyet.emlak.domain.view;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Created by ahmet on 06/09/16.
 */
@Entity
@Table(name = "vwRealtySolr2")
@NamedQueries(value = {
        @NamedQuery( name = "RealtySolrView.findAll", query = "SELECT u FROM RealtySolrView u"),
        @NamedQuery( name = "RealtySolrView.RecordsSize", query = "SELECT count(u) FROM RealtySolrView u" ),
        @NamedQuery( name = "RealtySolrView.Between", query = "SELECT e FROM RealtySolrView e WHERE e.realtyId BETWEEN :startRealty AND :endRealty" ),
        @NamedQuery( name = "RealtySolrView.findByDate", query = "SELECT p FROM RealtySolrView  p WHERE p.realtyUpdatedDateTime > :date ")
})
@NamedNativeQueries(value = {
        @NamedNativeQuery(
                name = "RealtySolrDeleted.findByDate",
                query = "SELECT convert(nvarchar(25), p.RealtyID) AS realtyId FROM vwRealtySolrDeleted AS p WHERE " +
                        "p.RealtyUpdatedDateTime > :date OR " +
                        "(p.RealtyEndDateTime BETWEEN :date AND GETDATE()) OR " +
                        "(p.FirmSummaryEndDate BETWEEN :date AND GETDATE())",
                resultSetMapping = "mapping.deleted"),
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "mapping.deleted", columns = {
                @ColumnResult(name = "realtyId")})
})
public class RealtySolrView {
    private int realtyId;
    private Integer realtyNo;
    private Integer realtyFirmId;
    private Integer realtyCategoryId;
    private Integer realtySubCategoryId;
    private String realtyTitle;
    private String realtyDescription;
    private String realtyPlace;
    private int realtyRoom;
    private int realtyLivingRoom;
    private int realtyBathroom;
    private int realtySqm;
    private Integer realtyCreditId;
    private Boolean realtyPriceShow;
    private Integer realtyProjectId;
    private Timestamp realtyCreatedDateTime;
    private Timestamp realtyUpdatedDateTime;
    private Timestamp realtyEndDateTime;
    private BigDecimal realtyMapLatitude;
    private BigDecimal realtyMapLongtitude;
    private Integer realtyTimeShareTerm;
    private Integer realtyFlatReceivedId;
    private Integer realtyBedCount;
    private String realtyRoomInfo;
    private Integer realtyIsPopulated;
    private Boolean realtyIsStudentOrSingle;
    private Integer realtyFloorId;
    private Integer realtyFloorCount;
    private Integer realtyHeatingId;
    private Integer realtyFuelId;
    private Integer realtyBuildStateId;
    private Integer realtyUsageId;
    private Integer realtyBarterId;
    private Boolean realtyByTransfer;
    private String realtyKeywords;
    private BigDecimal realtyPrice;
    private Integer realtyPriceCurrencyId;
    private Integer realtyAge;
    private Integer realtyLandRegisterId;
    private Integer realtyRegisterId;
    private Integer realtyBuildId;
    private Integer realtyResidenceId;
    private Integer realtyStarId;
    private BigDecimal realtySortOrder;
    private Integer realtyPublishId;
    private Integer realtyActivateId;
    private Integer realtyFirmUserId;
    private Integer realtyMapPublishId;
    private Boolean realtyIsCityTransformation;
    private Integer realtyFirmTypeId;
    private String realtyFirmLogo;
    private int realtyFirmRealEstateOrganizationId;
    private int realtyCityId;
    private String realtyCityDescription;
    private int realtyCountyId;
    private String realtyCountyDescription;
    private int realtyDistrictId;
    private String realtyDistrictDescription;
    private String realtyAreaIDs;
    private String realtyPriceCurrency;
    private Timestamp realtyStartDateTime;
    private Integer realtyAdvertiseOwnerId;
    private Integer realtyMainCategoryId;
    private String realtyMainCategory;
    private String realtySubCategory;
    private String realtyPublish;
    private String realtyActivate;
    private String realtyResidence;
    private String realtyStar;
    private String realtyCategory;
    private String realtyAdvertiseOwner;
    private String realtyLandRegister;
    private int realtyFileCount;
    private String realtyImage;
    private String realtyVideo;
    private String realtyMultimediaIDs;
    private String realtyMultimedia;
    private int realtyProductCount;
    private int realtyProductBackgroundWithColor;
    private int realtyProductBoldTitle;
    private String realtyAttributeInIDs;
    private String realtyAttributeOutIDs;
    private String realtyAttributeLocationIDs;
    private String realtyAttributeUsageIDs;
    private String realtyAttributeInfrastructureIDs;
    private String realtyAttributeRoomIDs;
    private String realtyAttributeRoomSocialInstitutionIDs;
    private BigDecimal realtyPriceTl;
    private String realtyTagProductIDs;
    private boolean realtyFirmHasVipPacket;
    private Integer realtyFirmPackageCategoryTypeId;
    private String realtyHousingComplexName;
    private String realtyAttributeDormIDs;
    private String realtyAttributeDormLocationIDs;
    private Integer realtyDormitoryCapacity;
    private String realtyDormitoryRoomCapacityIDs;
    private String realtyDormitoryRoomPriceRangeIDs;
    private int realtyFirmBlackListTypeId;
    private boolean realtyIsHousingComplex;
    private Boolean realtyIsAuthorizedRealtor;
    private Integer realtyImageCount;
    private Timestamp superRealtyStartDateTime;
    private Timestamp superRealtyEndDateTime;
    private Integer superRealtyAreaId;
    private String firmShortName;
    private String realtyOcsProductIDs;
    private Double firmScoreTotal;
    private Double realtyScore;
    private String realtyCleanLandRegisterFilePath;

    @Id
    @Column(name = "RealtyID")
    public int getRealtyId() {
        return realtyId;
    }

    public void setRealtyId(int realtyId) {
        this.realtyId = realtyId;
    }

    @Basic
    @Column(name = "RealtyNo")
    public Integer getRealtyNo() {
        return realtyNo;
    }

    public void setRealtyNo(Integer realtyNo) {
        this.realtyNo = realtyNo;
    }

    @Basic
    @Column(name = "RealtyFirmID")
    public Integer getRealtyFirmId() {
        return realtyFirmId;
    }

    public void setRealtyFirmId(Integer realtyFirmId) {
        this.realtyFirmId = realtyFirmId;
    }

    @Basic
    @Column(name = "RealtyCategoryID")
    public Integer getRealtyCategoryId() {
        return realtyCategoryId;
    }

    public void setRealtyCategoryId(Integer realtyCategoryId) {
        this.realtyCategoryId = realtyCategoryId;
    }

    @Basic
    @Column(name = "RealtySubCategoryID")
    public Integer getRealtySubCategoryId() {
        return realtySubCategoryId;
    }

    public void setRealtySubCategoryId(Integer realtySubCategoryId) {
        this.realtySubCategoryId = realtySubCategoryId;
    }

    @Basic
    @Column(name = "RealtyTitle")
    public String getRealtyTitle() {
        return realtyTitle;
    }

    public void setRealtyTitle(String realtyTitle) {
        this.realtyTitle = realtyTitle;
    }

    @Basic
    @Column(name = "RealtyDescription")
    public String getRealtyDescription() {
        return realtyDescription;
    }

    public void setRealtyDescription(String realtyDescription) {
        this.realtyDescription = realtyDescription;
    }

    @Basic
    @Column(name = "RealtyPlace")
    public String getRealtyPlace() {
        return realtyPlace;
    }

    public void setRealtyPlace(String realtyPlace) {
        this.realtyPlace = realtyPlace;
    }

    @Basic
    @Column(name = "RealtyRoom")
    public int getRealtyRoom() {
        return realtyRoom;
    }

    public void setRealtyRoom(int realtyRoom) {
        this.realtyRoom = realtyRoom;
    }

    @Basic
    @Column(name = "RealtyLivingRoom")
    public int getRealtyLivingRoom() {
        return realtyLivingRoom;
    }

    public void setRealtyLivingRoom(int realtyLivingRoom) {
        this.realtyLivingRoom = realtyLivingRoom;
    }

    @Basic
    @Column(name = "RealtyBathroom")
    public int getRealtyBathroom() {
        return realtyBathroom;
    }

    public void setRealtyBathroom(int realtyBathroom) {
        this.realtyBathroom = realtyBathroom;
    }

    @Basic
    @Column(name = "RealtySqm")
    public int getRealtySqm() {
        return realtySqm;
    }

    public void setRealtySqm(int realtySqm) {
        this.realtySqm = realtySqm;
    }

    @Basic
    @Column(name = "RealtyCreditID")
    public Integer getRealtyCreditId() {
        return realtyCreditId;
    }

    public void setRealtyCreditId(Integer realtyCreditId) {
        this.realtyCreditId = realtyCreditId;
    }

    @Basic
    @Column(name = "RealtyPriceShow")
    public Boolean getRealtyPriceShow() {
        return realtyPriceShow;
    }

    public void setRealtyPriceShow(Boolean realtyPriceShow) {
        this.realtyPriceShow = realtyPriceShow;
    }

    @Basic
    @Column(name = "RealtyProjectID")
    public Integer getRealtyProjectId() {
        return realtyProjectId;
    }

    public void setRealtyProjectId(Integer realtyProjectId) {
        this.realtyProjectId = realtyProjectId;
    }

    @Basic
    @Column(name = "RealtyCreatedDateTime")
    public Timestamp getRealtyCreatedDateTime() {
        return realtyCreatedDateTime;
    }

    public void setRealtyCreatedDateTime(Timestamp realtyCreatedDateTime) {
        this.realtyCreatedDateTime = realtyCreatedDateTime;
    }

    @Basic
    @Column(name = "RealtyUpdatedDateTime")
    public Timestamp getRealtyUpdatedDateTime() {
        return realtyUpdatedDateTime;
    }

    public void setRealtyUpdatedDateTime(Timestamp realtyUpdatedDateTime) {
        this.realtyUpdatedDateTime = realtyUpdatedDateTime;
    }

    @Basic
    @Column(name = "RealtyEndDateTime")
    public Timestamp getRealtyEndDateTime() {
        return realtyEndDateTime;
    }

    public void setRealtyEndDateTime(Timestamp realtyEndDateTime) {
        this.realtyEndDateTime = realtyEndDateTime;
    }

    @Basic
    @Column(name = "RealtyMapLatitude")
    public BigDecimal getRealtyMapLatitude() {
        return realtyMapLatitude;
    }

    public void setRealtyMapLatitude(BigDecimal realtyMapLatitude) {
        this.realtyMapLatitude = realtyMapLatitude;
    }

    @Basic
    @Column(name = "RealtyMapLongtitude")
    public BigDecimal getRealtyMapLongtitude() {
        return realtyMapLongtitude;
    }

    public void setRealtyMapLongtitude(BigDecimal realtyMapLongtitude) {
        this.realtyMapLongtitude = realtyMapLongtitude;
    }

    @Basic
    @Column(name = "RealtyTimeShareTerm")
    public Integer getRealtyTimeShareTerm() {
        return realtyTimeShareTerm;
    }

    public void setRealtyTimeShareTerm(Integer realtyTimeShareTerm) {
        this.realtyTimeShareTerm = realtyTimeShareTerm;
    }

    @Basic
    @Column(name = "RealtyFlatReceivedID")
    public Integer getRealtyFlatReceivedId() {
        return realtyFlatReceivedId;
    }

    public void setRealtyFlatReceivedId(Integer realtyFlatReceivedId) {
        this.realtyFlatReceivedId = realtyFlatReceivedId;
    }

    @Basic
    @Column(name = "RealtyBedCount")
    public Integer getRealtyBedCount() {
        return realtyBedCount;
    }

    public void setRealtyBedCount(Integer realtyBedCount) {
        this.realtyBedCount = realtyBedCount;
    }

    @Basic
    @Column(name = "RealtyRoomInfo")
    public String getRealtyRoomInfo() {
        return realtyRoomInfo;
    }

    public void setRealtyRoomInfo(String realtyRoomInfo) {
        this.realtyRoomInfo = realtyRoomInfo;
    }

    @Basic
    @Column(name = "RealtyIsPopulated")
    public Integer getRealtyIsPopulated() {
        return realtyIsPopulated;
    }

    public void setRealtyIsPopulated(Integer realtyIsPopulated) {
        this.realtyIsPopulated = realtyIsPopulated;
    }

    @Basic
    @Column(name = "RealtyIsStudentOrSingle")
    public Boolean getRealtyIsStudentOrSingle() {
        return realtyIsStudentOrSingle;
    }

    public void setRealtyIsStudentOrSingle(Boolean realtyIsStudentOrSingle) {
        this.realtyIsStudentOrSingle = realtyIsStudentOrSingle;
    }

    @Basic
    @Column(name = "RealtyFloorID")
    public Integer getRealtyFloorId() {
        return realtyFloorId;
    }

    public void setRealtyFloorId(Integer realtyFloorId) {
        this.realtyFloorId = realtyFloorId;
    }

    @Basic
    @Column(name = "RealtyFloorCount")
    public Integer getRealtyFloorCount() {
        return realtyFloorCount;
    }

    public void setRealtyFloorCount(Integer realtyFloorCount) {
        this.realtyFloorCount = realtyFloorCount;
    }

    @Basic
    @Column(name = "RealtyHeatingID")
    public Integer getRealtyHeatingId() {
        return realtyHeatingId;
    }

    public void setRealtyHeatingId(Integer realtyHeatingId) {
        this.realtyHeatingId = realtyHeatingId;
    }

    @Basic
    @Column(name = "RealtyFuelID")
    public Integer getRealtyFuelId() {
        return realtyFuelId;
    }

    public void setRealtyFuelId(Integer realtyFuelId) {
        this.realtyFuelId = realtyFuelId;
    }

    @Basic
    @Column(name = "RealtyBuildStateID")
    public Integer getRealtyBuildStateId() {
        return realtyBuildStateId;
    }

    public void setRealtyBuildStateId(Integer realtyBuildStateId) {
        this.realtyBuildStateId = realtyBuildStateId;
    }

    @Basic
    @Column(name = "RealtyUsageID")
    public Integer getRealtyUsageId() {
        return realtyUsageId;
    }

    public void setRealtyUsageId(Integer realtyUsageId) {
        this.realtyUsageId = realtyUsageId;
    }

    @Basic
    @Column(name = "RealtyBarterID")
    public Integer getRealtyBarterId() {
        return realtyBarterId;
    }

    public void setRealtyBarterId(Integer realtyBarterId) {
        this.realtyBarterId = realtyBarterId;
    }

    @Basic
    @Column(name = "RealtyByTransfer")
    public Boolean getRealtyByTransfer() {
        return realtyByTransfer;
    }

    public void setRealtyByTransfer(Boolean realtyByTransfer) {
        this.realtyByTransfer = realtyByTransfer;
    }

    @Basic
    @Column(name = "RealtyKeywords")
    public String getRealtyKeywords() {
        return realtyKeywords;
    }

    public void setRealtyKeywords(String realtyKeywords) {
        this.realtyKeywords = realtyKeywords;
    }

    @Basic
    @Column(name = "RealtyPrice")
    public BigDecimal getRealtyPrice() {
        return realtyPrice;
    }

    public void setRealtyPrice(BigDecimal realtyPrice) {
        this.realtyPrice = realtyPrice;
    }

    @Basic
    @Column(name = "RealtyPriceCurrencyID")
    public Integer getRealtyPriceCurrencyId() {
        return realtyPriceCurrencyId;
    }

    public void setRealtyPriceCurrencyId(Integer realtyPriceCurrencyId) {
        this.realtyPriceCurrencyId = realtyPriceCurrencyId;
    }

    @Basic
    @Column(name = "RealtyAge")
    public Integer getRealtyAge() {
        return realtyAge;
    }

    public void setRealtyAge(Integer realtyAge) {
        this.realtyAge = realtyAge;
    }

    @Basic
    @Column(name = "RealtyLandRegisterID")
    public Integer getRealtyLandRegisterId() {
        return realtyLandRegisterId;
    }

    public void setRealtyLandRegisterId(Integer realtyLandRegisterId) {
        this.realtyLandRegisterId = realtyLandRegisterId;
    }

    @Basic
    @Column(name = "RealtyRegisterID")
    public Integer getRealtyRegisterId() {
        return realtyRegisterId;
    }

    public void setRealtyRegisterId(Integer realtyRegisterId) {
        this.realtyRegisterId = realtyRegisterId;
    }

    @Basic
    @Column(name = "RealtyBuildID")
    public Integer getRealtyBuildId() {
        return realtyBuildId;
    }

    public void setRealtyBuildId(Integer realtyBuildId) {
        this.realtyBuildId = realtyBuildId;
    }

    @Basic
    @Column(name = "RealtyResidenceID")
    public Integer getRealtyResidenceId() {
        return realtyResidenceId;
    }

    public void setRealtyResidenceId(Integer realtyResidenceId) {
        this.realtyResidenceId = realtyResidenceId;
    }

    @Basic
    @Column(name = "RealtyStarID")
    public Integer getRealtyStarId() {
        return realtyStarId;
    }

    public void setRealtyStarId(Integer realtyStarId) {
        this.realtyStarId = realtyStarId;
    }

    @Basic
    @Column(name = "RealtySortOrder")
    public BigDecimal getRealtySortOrder() {
        return realtySortOrder;
    }

    public void setRealtySortOrder(BigDecimal realtySortOrder) {
        this.realtySortOrder = realtySortOrder;
    }

    @Basic
    @Column(name = "RealtyPublishID")
    public Integer getRealtyPublishId() {
        return realtyPublishId;
    }

    public void setRealtyPublishId(Integer realtyPublishId) {
        this.realtyPublishId = realtyPublishId;
    }

    @Basic
    @Column(name = "RealtyActivateID")
    public Integer getRealtyActivateId() {
        return realtyActivateId;
    }

    public void setRealtyActivateId(Integer realtyActivateId) {
        this.realtyActivateId = realtyActivateId;
    }

    @Basic
    @Column(name = "RealtyFirmUserID")
    public Integer getRealtyFirmUserId() {
        return realtyFirmUserId;
    }

    public void setRealtyFirmUserId(Integer realtyFirmUserId) {
        this.realtyFirmUserId = realtyFirmUserId;
    }

    @Basic
    @Column(name = "RealtyMapPublishID")
    public Integer getRealtyMapPublishId() {
        return realtyMapPublishId;
    }

    public void setRealtyMapPublishId(Integer realtyMapPublishId) {
        this.realtyMapPublishId = realtyMapPublishId;
    }

    @Basic
    @Column(name = "RealtyIsCityTransformation")
    public Boolean getRealtyIsCityTransformation() {
        return realtyIsCityTransformation;
    }

    public void setRealtyIsCityTransformation(Boolean realtyIsCityTransformation) {
        this.realtyIsCityTransformation = realtyIsCityTransformation;
    }

    @Basic
    @Column(name = "RealtyFirmTypeID")
    public Integer getRealtyFirmTypeId() {
        return realtyFirmTypeId;
    }

    public void setRealtyFirmTypeId(Integer realtyFirmTypeId) {
        this.realtyFirmTypeId = realtyFirmTypeId;
    }

    @Basic
    @Column(name = "RealtyFirmLogo")
    public String getRealtyFirmLogo() {
        return realtyFirmLogo;
    }

    public void setRealtyFirmLogo(String realtyFirmLogo) {
        this.realtyFirmLogo = realtyFirmLogo;
    }

    @Basic
    @Column(name = "RealtyFirmRealEstateOrganizationID")
    public int getRealtyFirmRealEstateOrganizationId() {
        return realtyFirmRealEstateOrganizationId;
    }

    public void setRealtyFirmRealEstateOrganizationId(int realtyFirmRealEstateOrganizationId) {
        this.realtyFirmRealEstateOrganizationId = realtyFirmRealEstateOrganizationId;
    }

    @Basic
    @Column(name = "RealtyCityID")
    public int getRealtyCityId() {
        return realtyCityId;
    }

    public void setRealtyCityId(int realtyCityId) {
        this.realtyCityId = realtyCityId;
    }

    @Basic
    @Column(name = "RealtyCityDescription")
    public String getRealtyCityDescription() {
        return realtyCityDescription;
    }

    public void setRealtyCityDescription(String realtyCityDescription) {
        this.realtyCityDescription = realtyCityDescription;
    }

    @Basic
    @Column(name = "RealtyCountyID")
    public int getRealtyCountyId() {
        return realtyCountyId;
    }

    public void setRealtyCountyId(int realtyCountyId) {
        this.realtyCountyId = realtyCountyId;
    }

    @Basic
    @Column(name = "RealtyCountyDescription")
    public String getRealtyCountyDescription() {
        return realtyCountyDescription;
    }

    public void setRealtyCountyDescription(String realtyCountyDescription) {
        this.realtyCountyDescription = realtyCountyDescription;
    }

    @Basic
    @Column(name = "RealtyDistrictID")
    public int getRealtyDistrictId() {
        return realtyDistrictId;
    }

    public void setRealtyDistrictId(int realtyDistrictId) {
        this.realtyDistrictId = realtyDistrictId;
    }

    @Basic
    @Column(name = "RealtyDistrictDescription")
    public String getRealtyDistrictDescription() {
        return realtyDistrictDescription;
    }

    public void setRealtyDistrictDescription(String realtyDistrictDescription) {
        this.realtyDistrictDescription = realtyDistrictDescription;
    }

    @Basic
    @Column(name = "RealtyAreaIDs")
    public String getRealtyAreaIDs() {
        return realtyAreaIDs;
    }

    public void setRealtyAreaIDs(String realtyAreaIDs) {
        this.realtyAreaIDs = realtyAreaIDs;
    }

    @Basic
    @Column(name = "RealtyPriceCurrency")
    public String getRealtyPriceCurrency() {
        return realtyPriceCurrency;
    }

    public void setRealtyPriceCurrency(String realtyPriceCurrency) {
        this.realtyPriceCurrency = realtyPriceCurrency;
    }

    @Basic
    @Column(name = "RealtyStartDateTime")
    public Timestamp getRealtyStartDateTime() {
        return realtyStartDateTime;
    }

    public void setRealtyStartDateTime(Timestamp realtyStartDateTime) {
        this.realtyStartDateTime = realtyStartDateTime;
    }

    @Basic
    @Column(name = "RealtyAdvertiseOwnerID")
    public Integer getRealtyAdvertiseOwnerId() {
        return realtyAdvertiseOwnerId;
    }

    public void setRealtyAdvertiseOwnerId(Integer realtyAdvertiseOwnerId) {
        this.realtyAdvertiseOwnerId = realtyAdvertiseOwnerId;
    }

    @Basic
    @Column(name = "RealtyMainCategoryID")
    public Integer getRealtyMainCategoryId() {
        return realtyMainCategoryId;
    }

    public void setRealtyMainCategoryId(Integer realtyMainCategoryId) {
        this.realtyMainCategoryId = realtyMainCategoryId;
    }

    @Basic
    @Column(name = "RealtyMainCategory")
    public String getRealtyMainCategory() {
        return realtyMainCategory;
    }

    public void setRealtyMainCategory(String realtyMainCategory) {
        this.realtyMainCategory = realtyMainCategory;
    }

    @Basic
    @Column(name = "RealtySubCategory")
    public String getRealtySubCategory() {
        return realtySubCategory;
    }

    public void setRealtySubCategory(String realtySubCategory) {
        this.realtySubCategory = realtySubCategory;
    }

    @Basic
    @Column(name = "RealtyPublish")
    public String getRealtyPublish() {
        return realtyPublish;
    }

    public void setRealtyPublish(String realtyPublish) {
        this.realtyPublish = realtyPublish;
    }

    @Basic
    @Column(name = "RealtyActivate")
    public String getRealtyActivate() {
        return realtyActivate;
    }

    public void setRealtyActivate(String realtyActivate) {
        this.realtyActivate = realtyActivate;
    }

    @Basic
    @Column(name = "RealtyResidence")
    public String getRealtyResidence() {
        return realtyResidence;
    }

    public void setRealtyResidence(String realtyResidence) {
        this.realtyResidence = realtyResidence;
    }

    @Basic
    @Column(name = "RealtyStar")
    public String getRealtyStar() {
        return realtyStar;
    }

    public void setRealtyStar(String realtyStar) {
        this.realtyStar = realtyStar;
    }

    @Basic
    @Column(name = "RealtyCategory")
    public String getRealtyCategory() {
        return realtyCategory;
    }

    public void setRealtyCategory(String realtyCategory) {
        this.realtyCategory = realtyCategory;
    }

    @Basic
    @Column(name = "RealtyAdvertiseOwner")
    public String getRealtyAdvertiseOwner() {
        return realtyAdvertiseOwner;
    }

    public void setRealtyAdvertiseOwner(String realtyAdvertiseOwner) {
        this.realtyAdvertiseOwner = realtyAdvertiseOwner;
    }

    @Basic
    @Column(name = "RealtyLandRegister")
    public String getRealtyLandRegister() {
        return realtyLandRegister;
    }

    public void setRealtyLandRegister(String realtyLandRegister) {
        this.realtyLandRegister = realtyLandRegister;
    }

    @Basic
    @Column(name = "RealtyFileCount")
    public int getRealtyFileCount() {
        return realtyFileCount;
    }

    public void setRealtyFileCount(int realtyFileCount) {
        this.realtyFileCount = realtyFileCount;
    }

    @Basic
    @Column(name = "RealtyImage")
    public String getRealtyImage() {
        return realtyImage;
    }

    public void setRealtyImage(String realtyImage) {
        this.realtyImage = realtyImage;
    }

    @Basic
    @Column(name = "RealtyVideo")
    public String getRealtyVideo() {
        return realtyVideo;
    }

    public void setRealtyVideo(String realtyVideo) {
        this.realtyVideo = realtyVideo;
    }

    @Basic
    @Column(name = "RealtyMultimediaIDs")
    public String getRealtyMultimediaIDs() {
        return realtyMultimediaIDs;
    }

    public void setRealtyMultimediaIDs(String realtyMultimediaIDs) {
        this.realtyMultimediaIDs = realtyMultimediaIDs;
    }

    @Basic
    @Column(name = "RealtyMultimedia")
    public String getRealtyMultimedia() {
        return realtyMultimedia;
    }

    public void setRealtyMultimedia(String realtyMultimedia) {
        this.realtyMultimedia = realtyMultimedia;
    }

    @Basic
    @Column(name = "RealtyProductCount")
    public int getRealtyProductCount() {
        return realtyProductCount;
    }

    public void setRealtyProductCount(int realtyProductCount) {
        this.realtyProductCount = realtyProductCount;
    }

    @Basic
    @Column(name = "RealtyProductBackgroundWithColor")
    public int getRealtyProductBackgroundWithColor() {
        return realtyProductBackgroundWithColor;
    }

    public void setRealtyProductBackgroundWithColor(int realtyProductBackgroundWithColor) {
        this.realtyProductBackgroundWithColor = realtyProductBackgroundWithColor;
    }

    @Basic
    @Column(name = "RealtyProductBoldTitle")
    public int getRealtyProductBoldTitle() {
        return realtyProductBoldTitle;
    }

    public void setRealtyProductBoldTitle(int realtyProductBoldTitle) {
        this.realtyProductBoldTitle = realtyProductBoldTitle;
    }

    @Basic
    @Column(name = "RealtyAttributeInIDs")
    public String getRealtyAttributeInIDs() {
        return realtyAttributeInIDs;
    }

    public void setRealtyAttributeInIDs(String realtyAttributeInIDs) {
        this.realtyAttributeInIDs = realtyAttributeInIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeOutIDs")
    public String getRealtyAttributeOutIDs() {
        return realtyAttributeOutIDs;
    }

    public void setRealtyAttributeOutIDs(String realtyAttributeOutIDs) {
        this.realtyAttributeOutIDs = realtyAttributeOutIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeLocationIDs")
    public String getRealtyAttributeLocationIDs() {
        return realtyAttributeLocationIDs;
    }

    public void setRealtyAttributeLocationIDs(String realtyAttributeLocationIDs) {
        this.realtyAttributeLocationIDs = realtyAttributeLocationIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeUsageIDs")
    public String getRealtyAttributeUsageIDs() {
        return realtyAttributeUsageIDs;
    }

    public void setRealtyAttributeUsageIDs(String realtyAttributeUsageIDs) {
        this.realtyAttributeUsageIDs = realtyAttributeUsageIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeInfrastructureIDs")
    public String getRealtyAttributeInfrastructureIDs() {
        return realtyAttributeInfrastructureIDs;
    }

    public void setRealtyAttributeInfrastructureIDs(String realtyAttributeInfrastructureIDs) {
        this.realtyAttributeInfrastructureIDs = realtyAttributeInfrastructureIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeRoomIDs")
    public String getRealtyAttributeRoomIDs() {
        return realtyAttributeRoomIDs;
    }

    public void setRealtyAttributeRoomIDs(String realtyAttributeRoomIDs) {
        this.realtyAttributeRoomIDs = realtyAttributeRoomIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeRoomSocialInstitutionIDs")
    public String getRealtyAttributeRoomSocialInstitutionIDs() {
        return realtyAttributeRoomSocialInstitutionIDs;
    }

    public void setRealtyAttributeRoomSocialInstitutionIDs(String realtyAttributeRoomSocialInstitutionIDs) {
        this.realtyAttributeRoomSocialInstitutionIDs = realtyAttributeRoomSocialInstitutionIDs;
    }

    @Basic
    @Column(name = "RealtyPriceTL")
    public BigDecimal getRealtyPriceTl() {
        return realtyPriceTl;
    }

    public void setRealtyPriceTl(BigDecimal realtyPriceTl) {
        this.realtyPriceTl = realtyPriceTl;
    }

    @Basic
    @Column(name = "RealtyTagProductIDs")
    public String getRealtyTagProductIDs() {
        return realtyTagProductIDs;
    }

    public void setRealtyTagProductIDs(String realtyTagProductIDs) {
        this.realtyTagProductIDs = realtyTagProductIDs;
    }

    @Basic
    @Column(name = "RealtyFirmHasVIPPacket")
    public boolean isRealtyFirmHasVipPacket() {
        return realtyFirmHasVipPacket;
    }

    public void setRealtyFirmHasVipPacket(boolean realtyFirmHasVipPacket) {
        this.realtyFirmHasVipPacket = realtyFirmHasVipPacket;
    }

    @Basic
    @Column(name = "RealtyFirmPackageCategoryTypeID")
    public Integer getRealtyFirmPackageCategoryTypeId() {
        return realtyFirmPackageCategoryTypeId;
    }

    public void setRealtyFirmPackageCategoryTypeId(Integer realtyFirmPackageCategoryTypeId) {
        this.realtyFirmPackageCategoryTypeId = realtyFirmPackageCategoryTypeId;
    }

    @Basic
    @Column(name = "RealtyHousingComplexName")
    public String getRealtyHousingComplexName() {
        return realtyHousingComplexName;
    }

    public void setRealtyHousingComplexName(String realtyHousingComplexName) {
        this.realtyHousingComplexName = realtyHousingComplexName;
    }

    @Basic
    @Column(name = "RealtyAttributeDormIDs")
    public String getRealtyAttributeDormIDs() {
        return realtyAttributeDormIDs;
    }

    public void setRealtyAttributeDormIDs(String realtyAttributeDormIDs) {
        this.realtyAttributeDormIDs = realtyAttributeDormIDs;
    }

    @Basic
    @Column(name = "RealtyAttributeDormLocationIDs")
    public String getRealtyAttributeDormLocationIDs() {
        return realtyAttributeDormLocationIDs;
    }

    public void setRealtyAttributeDormLocationIDs(String realtyAttributeDormLocationIDs) {
        this.realtyAttributeDormLocationIDs = realtyAttributeDormLocationIDs;
    }

    @Basic
    @Column(name = "RealtyDormitoryCapacity")
    public Integer getRealtyDormitoryCapacity() {
        return realtyDormitoryCapacity;
    }

    public void setRealtyDormitoryCapacity(Integer realtyDormitoryCapacity) {
        this.realtyDormitoryCapacity = realtyDormitoryCapacity;
    }

    @Basic
    @Column(name = "RealtyDormitoryRoomCapacityIDs")
    public String getRealtyDormitoryRoomCapacityIDs() {
        return realtyDormitoryRoomCapacityIDs;
    }

    public void setRealtyDormitoryRoomCapacityIDs(String realtyDormitoryRoomCapacityIDs) {
        this.realtyDormitoryRoomCapacityIDs = realtyDormitoryRoomCapacityIDs;
    }

    @Basic
    @Column(name = "RealtyDormitoryRoomPriceRangeIDs")
    public String getRealtyDormitoryRoomPriceRangeIDs() {
        return realtyDormitoryRoomPriceRangeIDs;
    }

    public void setRealtyDormitoryRoomPriceRangeIDs(String realtyDormitoryRoomPriceRangeIDs) {
        this.realtyDormitoryRoomPriceRangeIDs = realtyDormitoryRoomPriceRangeIDs;
    }

    @Basic
    @Column(name = "RealtyFirmBlackListTypeID")
    public int getRealtyFirmBlackListTypeId() {
        return realtyFirmBlackListTypeId;
    }

    public void setRealtyFirmBlackListTypeId(int realtyFirmBlackListTypeId) {
        this.realtyFirmBlackListTypeId = realtyFirmBlackListTypeId;
    }

    @Basic
    @Column(name = "RealtyIsHousingComplex")
    public boolean isRealtyIsHousingComplex() {
        return realtyIsHousingComplex;
    }

    public void setRealtyIsHousingComplex(boolean realtyIsHousingComplex) {
        this.realtyIsHousingComplex = realtyIsHousingComplex;
    }

    @Basic
    @Column(name = "RealtyIsAuthorizedRealtor")
    public Boolean getRealtyIsAuthorizedRealtor() {
        return realtyIsAuthorizedRealtor;
    }

    public void setRealtyIsAuthorizedRealtor(Boolean realtyIsAuthorizedRealtor) {
        this.realtyIsAuthorizedRealtor = realtyIsAuthorizedRealtor;
    }

    @Basic
    @Column(name = "RealtyImageCount")
    public Integer getRealtyImageCount() {
        return realtyImageCount;
    }

    public void setRealtyImageCount(Integer realtyImageCount) {
        this.realtyImageCount = realtyImageCount;
    }

    @Basic
    @Column(name = "SuperRealtyStartDateTime")
    public Timestamp getSuperRealtyStartDateTime() {
        return superRealtyStartDateTime;
    }

    public void setSuperRealtyStartDateTime(Timestamp superRealtyStartDateTime) {
        this.superRealtyStartDateTime = superRealtyStartDateTime;
    }

    @Basic
    @Column(name = "SuperRealtyEndDateTime")
    public Timestamp getSuperRealtyEndDateTime() {
        return superRealtyEndDateTime;
    }

    public void setSuperRealtyEndDateTime(Timestamp superRealtyEndDateTime) {
        this.superRealtyEndDateTime = superRealtyEndDateTime;
    }

    @Basic
    @Column(name = "SuperRealtyAreaID")
    public Integer getSuperRealtyAreaId() {
        return superRealtyAreaId;
    }

    public void setSuperRealtyAreaId(Integer superRealtyAreaId) {
        this.superRealtyAreaId = superRealtyAreaId;
    }

    @Basic
    @Column(name = "FirmShortName")
    public String getFirmShortName() {
        return firmShortName;
    }

    public void setFirmShortName(String firmShortName) {
        this.firmShortName = firmShortName;
    }

    @Basic
    @Column(name = "RealtyOcsProductIDs")
    public String getRealtyOcsProductIDs() {
        return realtyOcsProductIDs;
    }

    public void setRealtyOcsProductIDs(String realtyOcsProductIDs) {
        this.realtyOcsProductIDs = realtyOcsProductIDs;
    }

    @Basic
    @Column(name = "FirmScoreTotal")
    public Double getFirmScoreTotal() {
        return firmScoreTotal;
    }

    public void setFirmScoreTotal(Double firmScoreTotal) {
        this.firmScoreTotal = firmScoreTotal;
    }

    @Basic
    @Column(name = "RealtyScore")
    public Double getRealtyScore() {
        return realtyScore;
    }

    public void setRealtyScore(Double realtyScore) {
        this.realtyScore = realtyScore;
    }

    @Basic
    @Column(name = "RealtyCleanLandRegisterFilePath")
    public String getRealtyCleanLandRegisterFilePath() {
        return realtyCleanLandRegisterFilePath;
    }

    public void setRealtyCleanLandRegisterFilePath(String realtyCleanLandRegisterFilePath) {
        this.realtyCleanLandRegisterFilePath = realtyCleanLandRegisterFilePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealtySolrView that = (RealtySolrView) o;

        if (realtyId != that.realtyId) return false;
        if (realtyRoom != that.realtyRoom) return false;
        if (realtyLivingRoom != that.realtyLivingRoom) return false;
        if (realtyBathroom != that.realtyBathroom) return false;
        if (realtySqm != that.realtySqm) return false;
        if (realtyFirmRealEstateOrganizationId != that.realtyFirmRealEstateOrganizationId) return false;
        if (realtyCityId != that.realtyCityId) return false;
        if (realtyCountyId != that.realtyCountyId) return false;
        if (realtyDistrictId != that.realtyDistrictId) return false;
        if (realtyFileCount != that.realtyFileCount) return false;
        if (realtyProductCount != that.realtyProductCount) return false;
        if (realtyProductBackgroundWithColor != that.realtyProductBackgroundWithColor) return false;
        if (realtyProductBoldTitle != that.realtyProductBoldTitle) return false;
        if (realtyFirmHasVipPacket != that.realtyFirmHasVipPacket) return false;
        if (realtyFirmBlackListTypeId != that.realtyFirmBlackListTypeId) return false;
        if (realtyIsHousingComplex != that.realtyIsHousingComplex) return false;
        if (realtyNo != null ? !realtyNo.equals(that.realtyNo) : that.realtyNo != null) return false;
        if (realtyFirmId != null ? !realtyFirmId.equals(that.realtyFirmId) : that.realtyFirmId != null) return false;
        if (realtyCategoryId != null ? !realtyCategoryId.equals(that.realtyCategoryId) : that.realtyCategoryId != null)
            return false;
        if (realtySubCategoryId != null ? !realtySubCategoryId.equals(that.realtySubCategoryId) : that.realtySubCategoryId != null)
            return false;
        if (realtyTitle != null ? !realtyTitle.equals(that.realtyTitle) : that.realtyTitle != null) return false;
        if (realtyDescription != null ? !realtyDescription.equals(that.realtyDescription) : that.realtyDescription != null)
            return false;
        if (realtyPlace != null ? !realtyPlace.equals(that.realtyPlace) : that.realtyPlace != null) return false;
        if (realtyCreditId != null ? !realtyCreditId.equals(that.realtyCreditId) : that.realtyCreditId != null)
            return false;
        if (realtyPriceShow != null ? !realtyPriceShow.equals(that.realtyPriceShow) : that.realtyPriceShow != null)
            return false;
        if (realtyProjectId != null ? !realtyProjectId.equals(that.realtyProjectId) : that.realtyProjectId != null)
            return false;
        if (realtyCreatedDateTime != null ? !realtyCreatedDateTime.equals(that.realtyCreatedDateTime) : that.realtyCreatedDateTime != null)
            return false;
        if (realtyUpdatedDateTime != null ? !realtyUpdatedDateTime.equals(that.realtyUpdatedDateTime) : that.realtyUpdatedDateTime != null)
            return false;
        if (realtyEndDateTime != null ? !realtyEndDateTime.equals(that.realtyEndDateTime) : that.realtyEndDateTime != null)
            return false;
        if (realtyMapLatitude != null ? !realtyMapLatitude.equals(that.realtyMapLatitude) : that.realtyMapLatitude != null)
            return false;
        if (realtyMapLongtitude != null ? !realtyMapLongtitude.equals(that.realtyMapLongtitude) : that.realtyMapLongtitude != null)
            return false;
        if (realtyTimeShareTerm != null ? !realtyTimeShareTerm.equals(that.realtyTimeShareTerm) : that.realtyTimeShareTerm != null)
            return false;
        if (realtyFlatReceivedId != null ? !realtyFlatReceivedId.equals(that.realtyFlatReceivedId) : that.realtyFlatReceivedId != null)
            return false;
        if (realtyBedCount != null ? !realtyBedCount.equals(that.realtyBedCount) : that.realtyBedCount != null)
            return false;
        if (realtyRoomInfo != null ? !realtyRoomInfo.equals(that.realtyRoomInfo) : that.realtyRoomInfo != null)
            return false;
        if (realtyIsPopulated != null ? !realtyIsPopulated.equals(that.realtyIsPopulated) : that.realtyIsPopulated != null)
            return false;
        if (realtyIsStudentOrSingle != null ? !realtyIsStudentOrSingle.equals(that.realtyIsStudentOrSingle) : that.realtyIsStudentOrSingle != null)
            return false;
        if (realtyFloorId != null ? !realtyFloorId.equals(that.realtyFloorId) : that.realtyFloorId != null)
            return false;
        if (realtyFloorCount != null ? !realtyFloorCount.equals(that.realtyFloorCount) : that.realtyFloorCount != null)
            return false;
        if (realtyHeatingId != null ? !realtyHeatingId.equals(that.realtyHeatingId) : that.realtyHeatingId != null)
            return false;
        if (realtyFuelId != null ? !realtyFuelId.equals(that.realtyFuelId) : that.realtyFuelId != null) return false;
        if (realtyBuildStateId != null ? !realtyBuildStateId.equals(that.realtyBuildStateId) : that.realtyBuildStateId != null)
            return false;
        if (realtyUsageId != null ? !realtyUsageId.equals(that.realtyUsageId) : that.realtyUsageId != null)
            return false;
        if (realtyBarterId != null ? !realtyBarterId.equals(that.realtyBarterId) : that.realtyBarterId != null)
            return false;
        if (realtyByTransfer != null ? !realtyByTransfer.equals(that.realtyByTransfer) : that.realtyByTransfer != null)
            return false;
        if (realtyKeywords != null ? !realtyKeywords.equals(that.realtyKeywords) : that.realtyKeywords != null)
            return false;
        if (realtyPrice != null ? !realtyPrice.equals(that.realtyPrice) : that.realtyPrice != null) return false;
        if (realtyPriceCurrencyId != null ? !realtyPriceCurrencyId.equals(that.realtyPriceCurrencyId) : that.realtyPriceCurrencyId != null)
            return false;
        if (realtyAge != null ? !realtyAge.equals(that.realtyAge) : that.realtyAge != null) return false;
        if (realtyLandRegisterId != null ? !realtyLandRegisterId.equals(that.realtyLandRegisterId) : that.realtyLandRegisterId != null)
            return false;
        if (realtyRegisterId != null ? !realtyRegisterId.equals(that.realtyRegisterId) : that.realtyRegisterId != null)
            return false;
        if (realtyBuildId != null ? !realtyBuildId.equals(that.realtyBuildId) : that.realtyBuildId != null)
            return false;
        if (realtyResidenceId != null ? !realtyResidenceId.equals(that.realtyResidenceId) : that.realtyResidenceId != null)
            return false;
        if (realtyStarId != null ? !realtyStarId.equals(that.realtyStarId) : that.realtyStarId != null) return false;
        if (realtySortOrder != null ? !realtySortOrder.equals(that.realtySortOrder) : that.realtySortOrder != null)
            return false;
        if (realtyPublishId != null ? !realtyPublishId.equals(that.realtyPublishId) : that.realtyPublishId != null)
            return false;
        if (realtyActivateId != null ? !realtyActivateId.equals(that.realtyActivateId) : that.realtyActivateId != null)
            return false;
        if (realtyFirmUserId != null ? !realtyFirmUserId.equals(that.realtyFirmUserId) : that.realtyFirmUserId != null)
            return false;
        if (realtyMapPublishId != null ? !realtyMapPublishId.equals(that.realtyMapPublishId) : that.realtyMapPublishId != null)
            return false;
        if (realtyIsCityTransformation != null ? !realtyIsCityTransformation.equals(that.realtyIsCityTransformation) : that.realtyIsCityTransformation != null)
            return false;
        if (realtyFirmTypeId != null ? !realtyFirmTypeId.equals(that.realtyFirmTypeId) : that.realtyFirmTypeId != null)
            return false;
        if (realtyFirmLogo != null ? !realtyFirmLogo.equals(that.realtyFirmLogo) : that.realtyFirmLogo != null)
            return false;
        if (realtyCityDescription != null ? !realtyCityDescription.equals(that.realtyCityDescription) : that.realtyCityDescription != null)
            return false;
        if (realtyCountyDescription != null ? !realtyCountyDescription.equals(that.realtyCountyDescription) : that.realtyCountyDescription != null)
            return false;
        if (realtyDistrictDescription != null ? !realtyDistrictDescription.equals(that.realtyDistrictDescription) : that.realtyDistrictDescription != null)
            return false;
        if (realtyAreaIDs != null ? !realtyAreaIDs.equals(that.realtyAreaIDs) : that.realtyAreaIDs != null)
            return false;
        if (realtyPriceCurrency != null ? !realtyPriceCurrency.equals(that.realtyPriceCurrency) : that.realtyPriceCurrency != null)
            return false;
        if (realtyStartDateTime != null ? !realtyStartDateTime.equals(that.realtyStartDateTime) : that.realtyStartDateTime != null)
            return false;
        if (realtyAdvertiseOwnerId != null ? !realtyAdvertiseOwnerId.equals(that.realtyAdvertiseOwnerId) : that.realtyAdvertiseOwnerId != null)
            return false;
        if (realtyMainCategoryId != null ? !realtyMainCategoryId.equals(that.realtyMainCategoryId) : that.realtyMainCategoryId != null)
            return false;
        if (realtyMainCategory != null ? !realtyMainCategory.equals(that.realtyMainCategory) : that.realtyMainCategory != null)
            return false;
        if (realtySubCategory != null ? !realtySubCategory.equals(that.realtySubCategory) : that.realtySubCategory != null)
            return false;
        if (realtyPublish != null ? !realtyPublish.equals(that.realtyPublish) : that.realtyPublish != null)
            return false;
        if (realtyActivate != null ? !realtyActivate.equals(that.realtyActivate) : that.realtyActivate != null)
            return false;
        if (realtyResidence != null ? !realtyResidence.equals(that.realtyResidence) : that.realtyResidence != null)
            return false;
        if (realtyStar != null ? !realtyStar.equals(that.realtyStar) : that.realtyStar != null) return false;
        if (realtyCategory != null ? !realtyCategory.equals(that.realtyCategory) : that.realtyCategory != null)
            return false;
        if (realtyAdvertiseOwner != null ? !realtyAdvertiseOwner.equals(that.realtyAdvertiseOwner) : that.realtyAdvertiseOwner != null)
            return false;
        if (realtyLandRegister != null ? !realtyLandRegister.equals(that.realtyLandRegister) : that.realtyLandRegister != null)
            return false;
        if (realtyImage != null ? !realtyImage.equals(that.realtyImage) : that.realtyImage != null) return false;
        if (realtyVideo != null ? !realtyVideo.equals(that.realtyVideo) : that.realtyVideo != null) return false;
        if (realtyMultimediaIDs != null ? !realtyMultimediaIDs.equals(that.realtyMultimediaIDs) : that.realtyMultimediaIDs != null)
            return false;
        if (realtyMultimedia != null ? !realtyMultimedia.equals(that.realtyMultimedia) : that.realtyMultimedia != null)
            return false;
        if (realtyAttributeInIDs != null ? !realtyAttributeInIDs.equals(that.realtyAttributeInIDs) : that.realtyAttributeInIDs != null)
            return false;
        if (realtyAttributeOutIDs != null ? !realtyAttributeOutIDs.equals(that.realtyAttributeOutIDs) : that.realtyAttributeOutIDs != null)
            return false;
        if (realtyAttributeLocationIDs != null ? !realtyAttributeLocationIDs.equals(that.realtyAttributeLocationIDs) : that.realtyAttributeLocationIDs != null)
            return false;
        if (realtyAttributeUsageIDs != null ? !realtyAttributeUsageIDs.equals(that.realtyAttributeUsageIDs) : that.realtyAttributeUsageIDs != null)
            return false;
        if (realtyAttributeInfrastructureIDs != null ? !realtyAttributeInfrastructureIDs.equals(that.realtyAttributeInfrastructureIDs) : that.realtyAttributeInfrastructureIDs != null)
            return false;
        if (realtyAttributeRoomIDs != null ? !realtyAttributeRoomIDs.equals(that.realtyAttributeRoomIDs) : that.realtyAttributeRoomIDs != null)
            return false;
        if (realtyAttributeRoomSocialInstitutionIDs != null ? !realtyAttributeRoomSocialInstitutionIDs.equals(that.realtyAttributeRoomSocialInstitutionIDs) : that.realtyAttributeRoomSocialInstitutionIDs != null)
            return false;
        if (realtyPriceTl != null ? !realtyPriceTl.equals(that.realtyPriceTl) : that.realtyPriceTl != null)
            return false;
        if (realtyTagProductIDs != null ? !realtyTagProductIDs.equals(that.realtyTagProductIDs) : that.realtyTagProductIDs != null)
            return false;
        if (realtyFirmPackageCategoryTypeId != null ? !realtyFirmPackageCategoryTypeId.equals(that.realtyFirmPackageCategoryTypeId) : that.realtyFirmPackageCategoryTypeId != null)
            return false;
        if (realtyHousingComplexName != null ? !realtyHousingComplexName.equals(that.realtyHousingComplexName) : that.realtyHousingComplexName != null)
            return false;
        if (realtyAttributeDormIDs != null ? !realtyAttributeDormIDs.equals(that.realtyAttributeDormIDs) : that.realtyAttributeDormIDs != null)
            return false;
        if (realtyAttributeDormLocationIDs != null ? !realtyAttributeDormLocationIDs.equals(that.realtyAttributeDormLocationIDs) : that.realtyAttributeDormLocationIDs != null)
            return false;
        if (realtyDormitoryCapacity != null ? !realtyDormitoryCapacity.equals(that.realtyDormitoryCapacity) : that.realtyDormitoryCapacity != null)
            return false;
        if (realtyDormitoryRoomCapacityIDs != null ? !realtyDormitoryRoomCapacityIDs.equals(that.realtyDormitoryRoomCapacityIDs) : that.realtyDormitoryRoomCapacityIDs != null)
            return false;
        if (realtyDormitoryRoomPriceRangeIDs != null ? !realtyDormitoryRoomPriceRangeIDs.equals(that.realtyDormitoryRoomPriceRangeIDs) : that.realtyDormitoryRoomPriceRangeIDs != null)
            return false;
        if (realtyIsAuthorizedRealtor != null ? !realtyIsAuthorizedRealtor.equals(that.realtyIsAuthorizedRealtor) : that.realtyIsAuthorizedRealtor != null)
            return false;
        if (realtyImageCount != null ? !realtyImageCount.equals(that.realtyImageCount) : that.realtyImageCount != null)
            return false;
        if (superRealtyStartDateTime != null ? !superRealtyStartDateTime.equals(that.superRealtyStartDateTime) : that.superRealtyStartDateTime != null)
            return false;
        if (superRealtyEndDateTime != null ? !superRealtyEndDateTime.equals(that.superRealtyEndDateTime) : that.superRealtyEndDateTime != null)
            return false;
        if (superRealtyAreaId != null ? !superRealtyAreaId.equals(that.superRealtyAreaId) : that.superRealtyAreaId != null)
            return false;
        if (firmShortName != null ? !firmShortName.equals(that.firmShortName) : that.firmShortName != null)
            return false;
        if (realtyOcsProductIDs != null ? !realtyOcsProductIDs.equals(that.realtyOcsProductIDs) : that.realtyOcsProductIDs != null)
            return false;
        if (firmScoreTotal != null ? !firmScoreTotal.equals(that.firmScoreTotal) : that.firmScoreTotal != null)
            return false;
        if (realtyScore != null ? !realtyScore.equals(that.realtyScore) : that.realtyScore != null) return false;
        if (realtyCleanLandRegisterFilePath != null ? !realtyCleanLandRegisterFilePath.equals(that.realtyCleanLandRegisterFilePath) : that.realtyCleanLandRegisterFilePath != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = realtyId;
        result = 31 * result + (realtyNo != null ? realtyNo.hashCode() : 0);
        result = 31 * result + (realtyFirmId != null ? realtyFirmId.hashCode() : 0);
        result = 31 * result + (realtyCategoryId != null ? realtyCategoryId.hashCode() : 0);
        result = 31 * result + (realtySubCategoryId != null ? realtySubCategoryId.hashCode() : 0);
        result = 31 * result + (realtyTitle != null ? realtyTitle.hashCode() : 0);
        result = 31 * result + (realtyDescription != null ? realtyDescription.hashCode() : 0);
        result = 31 * result + (realtyPlace != null ? realtyPlace.hashCode() : 0);
        result = 31 * result + realtyRoom;
        result = 31 * result + realtyLivingRoom;
        result = 31 * result + realtyBathroom;
        result = 31 * result + realtySqm;
        result = 31 * result + (realtyCreditId != null ? realtyCreditId.hashCode() : 0);
        result = 31 * result + (realtyPriceShow != null ? realtyPriceShow.hashCode() : 0);
        result = 31 * result + (realtyProjectId != null ? realtyProjectId.hashCode() : 0);
        result = 31 * result + (realtyCreatedDateTime != null ? realtyCreatedDateTime.hashCode() : 0);
        result = 31 * result + (realtyUpdatedDateTime != null ? realtyUpdatedDateTime.hashCode() : 0);
        result = 31 * result + (realtyEndDateTime != null ? realtyEndDateTime.hashCode() : 0);
        result = 31 * result + (realtyMapLatitude != null ? realtyMapLatitude.hashCode() : 0);
        result = 31 * result + (realtyMapLongtitude != null ? realtyMapLongtitude.hashCode() : 0);
        result = 31 * result + (realtyTimeShareTerm != null ? realtyTimeShareTerm.hashCode() : 0);
        result = 31 * result + (realtyFlatReceivedId != null ? realtyFlatReceivedId.hashCode() : 0);
        result = 31 * result + (realtyBedCount != null ? realtyBedCount.hashCode() : 0);
        result = 31 * result + (realtyRoomInfo != null ? realtyRoomInfo.hashCode() : 0);
        result = 31 * result + (realtyIsPopulated != null ? realtyIsPopulated.hashCode() : 0);
        result = 31 * result + (realtyIsStudentOrSingle != null ? realtyIsStudentOrSingle.hashCode() : 0);
        result = 31 * result + (realtyFloorId != null ? realtyFloorId.hashCode() : 0);
        result = 31 * result + (realtyFloorCount != null ? realtyFloorCount.hashCode() : 0);
        result = 31 * result + (realtyHeatingId != null ? realtyHeatingId.hashCode() : 0);
        result = 31 * result + (realtyFuelId != null ? realtyFuelId.hashCode() : 0);
        result = 31 * result + (realtyBuildStateId != null ? realtyBuildStateId.hashCode() : 0);
        result = 31 * result + (realtyUsageId != null ? realtyUsageId.hashCode() : 0);
        result = 31 * result + (realtyBarterId != null ? realtyBarterId.hashCode() : 0);
        result = 31 * result + (realtyByTransfer != null ? realtyByTransfer.hashCode() : 0);
        result = 31 * result + (realtyKeywords != null ? realtyKeywords.hashCode() : 0);
        result = 31 * result + (realtyPrice != null ? realtyPrice.hashCode() : 0);
        result = 31 * result + (realtyPriceCurrencyId != null ? realtyPriceCurrencyId.hashCode() : 0);
        result = 31 * result + (realtyAge != null ? realtyAge.hashCode() : 0);
        result = 31 * result + (realtyLandRegisterId != null ? realtyLandRegisterId.hashCode() : 0);
        result = 31 * result + (realtyRegisterId != null ? realtyRegisterId.hashCode() : 0);
        result = 31 * result + (realtyBuildId != null ? realtyBuildId.hashCode() : 0);
        result = 31 * result + (realtyResidenceId != null ? realtyResidenceId.hashCode() : 0);
        result = 31 * result + (realtyStarId != null ? realtyStarId.hashCode() : 0);
        result = 31 * result + (realtySortOrder != null ? realtySortOrder.hashCode() : 0);
        result = 31 * result + (realtyPublishId != null ? realtyPublishId.hashCode() : 0);
        result = 31 * result + (realtyActivateId != null ? realtyActivateId.hashCode() : 0);
        result = 31 * result + (realtyFirmUserId != null ? realtyFirmUserId.hashCode() : 0);
        result = 31 * result + (realtyMapPublishId != null ? realtyMapPublishId.hashCode() : 0);
        result = 31 * result + (realtyIsCityTransformation != null ? realtyIsCityTransformation.hashCode() : 0);
        result = 31 * result + (realtyFirmTypeId != null ? realtyFirmTypeId.hashCode() : 0);
        result = 31 * result + (realtyFirmLogo != null ? realtyFirmLogo.hashCode() : 0);
        result = 31 * result + realtyFirmRealEstateOrganizationId;
        result = 31 * result + realtyCityId;
        result = 31 * result + (realtyCityDescription != null ? realtyCityDescription.hashCode() : 0);
        result = 31 * result + realtyCountyId;
        result = 31 * result + (realtyCountyDescription != null ? realtyCountyDescription.hashCode() : 0);
        result = 31 * result + realtyDistrictId;
        result = 31 * result + (realtyDistrictDescription != null ? realtyDistrictDescription.hashCode() : 0);
        result = 31 * result + (realtyAreaIDs != null ? realtyAreaIDs.hashCode() : 0);
        result = 31 * result + (realtyPriceCurrency != null ? realtyPriceCurrency.hashCode() : 0);
        result = 31 * result + (realtyStartDateTime != null ? realtyStartDateTime.hashCode() : 0);
        result = 31 * result + (realtyAdvertiseOwnerId != null ? realtyAdvertiseOwnerId.hashCode() : 0);
        result = 31 * result + (realtyMainCategoryId != null ? realtyMainCategoryId.hashCode() : 0);
        result = 31 * result + (realtyMainCategory != null ? realtyMainCategory.hashCode() : 0);
        result = 31 * result + (realtySubCategory != null ? realtySubCategory.hashCode() : 0);
        result = 31 * result + (realtyPublish != null ? realtyPublish.hashCode() : 0);
        result = 31 * result + (realtyActivate != null ? realtyActivate.hashCode() : 0);
        result = 31 * result + (realtyResidence != null ? realtyResidence.hashCode() : 0);
        result = 31 * result + (realtyStar != null ? realtyStar.hashCode() : 0);
        result = 31 * result + (realtyCategory != null ? realtyCategory.hashCode() : 0);
        result = 31 * result + (realtyAdvertiseOwner != null ? realtyAdvertiseOwner.hashCode() : 0);
        result = 31 * result + (realtyLandRegister != null ? realtyLandRegister.hashCode() : 0);
        result = 31 * result + realtyFileCount;
        result = 31 * result + (realtyImage != null ? realtyImage.hashCode() : 0);
        result = 31 * result + (realtyVideo != null ? realtyVideo.hashCode() : 0);
        result = 31 * result + (realtyMultimediaIDs != null ? realtyMultimediaIDs.hashCode() : 0);
        result = 31 * result + (realtyMultimedia != null ? realtyMultimedia.hashCode() : 0);
        result = 31 * result + realtyProductCount;
        result = 31 * result + realtyProductBackgroundWithColor;
        result = 31 * result + realtyProductBoldTitle;
        result = 31 * result + (realtyAttributeInIDs != null ? realtyAttributeInIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeOutIDs != null ? realtyAttributeOutIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeLocationIDs != null ? realtyAttributeLocationIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeUsageIDs != null ? realtyAttributeUsageIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeInfrastructureIDs != null ? realtyAttributeInfrastructureIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeRoomIDs != null ? realtyAttributeRoomIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeRoomSocialInstitutionIDs != null ? realtyAttributeRoomSocialInstitutionIDs.hashCode() : 0);
        result = 31 * result + (realtyPriceTl != null ? realtyPriceTl.hashCode() : 0);
        result = 31 * result + (realtyTagProductIDs != null ? realtyTagProductIDs.hashCode() : 0);
        result = 31 * result + (realtyFirmHasVipPacket ? 1 : 0);
        result = 31 * result + (realtyFirmPackageCategoryTypeId != null ? realtyFirmPackageCategoryTypeId.hashCode() : 0);
        result = 31 * result + (realtyHousingComplexName != null ? realtyHousingComplexName.hashCode() : 0);
        result = 31 * result + (realtyAttributeDormIDs != null ? realtyAttributeDormIDs.hashCode() : 0);
        result = 31 * result + (realtyAttributeDormLocationIDs != null ? realtyAttributeDormLocationIDs.hashCode() : 0);
        result = 31 * result + (realtyDormitoryCapacity != null ? realtyDormitoryCapacity.hashCode() : 0);
        result = 31 * result + (realtyDormitoryRoomCapacityIDs != null ? realtyDormitoryRoomCapacityIDs.hashCode() : 0);
        result = 31 * result + (realtyDormitoryRoomPriceRangeIDs != null ? realtyDormitoryRoomPriceRangeIDs.hashCode() : 0);
        result = 31 * result + realtyFirmBlackListTypeId;
        result = 31 * result + (realtyIsHousingComplex ? 1 : 0);
        result = 31 * result + (realtyIsAuthorizedRealtor != null ? realtyIsAuthorizedRealtor.hashCode() : 0);
        result = 31 * result + (realtyImageCount != null ? realtyImageCount.hashCode() : 0);
        result = 31 * result + (superRealtyStartDateTime != null ? superRealtyStartDateTime.hashCode() : 0);
        result = 31 * result + (superRealtyEndDateTime != null ? superRealtyEndDateTime.hashCode() : 0);
        result = 31 * result + (superRealtyAreaId != null ? superRealtyAreaId.hashCode() : 0);
        result = 31 * result + (firmShortName != null ? firmShortName.hashCode() : 0);
        result = 31 * result + (realtyOcsProductIDs != null ? realtyOcsProductIDs.hashCode() : 0);
        result = 31 * result + (firmScoreTotal != null ? firmScoreTotal.hashCode() : 0);
        result = 31 * result + (realtyScore != null ? realtyScore.hashCode() : 0);
        result = 31 * result + (realtyCleanLandRegisterFilePath != null ? realtyCleanLandRegisterFilePath.hashCode() : 0);
        return result;
    }
}
