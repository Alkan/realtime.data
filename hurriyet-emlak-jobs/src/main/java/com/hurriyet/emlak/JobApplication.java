package com.hurriyet.emlak;

import com.hurriyet.emlak.job.config.Environment;
import com.hurriyet.emlak.job.config.JobName;
import com.hurriyet.emlak.job.service.RealtyInteractionDetailMigrateJob;
import com.hurriyet.emlak.job.service.RealtySolrIndexJob;
import com.hurriyet.emlak.job.service.RealtyInteractionTotalMigrateJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.util.StopWatch;


@SpringBootApplication
public class JobApplication implements CommandLineRunner {


    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Environment environment;

    @Autowired
    private RealtySolrIndexJob realtySolrIndexJob;

    @Autowired
    private RealtyInteractionTotalMigrateJob realtyInteractionMigrateJob;

    @Autowired
    private RealtyInteractionDetailMigrateJob realtyInteractionDetailMigrateJob;


    public static void main(String[] args) throws Exception {
        SpringApplication.run(JobApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

        StopWatch stopWatch = new StopWatch("Job Progress Time:");
        JobName jobName =  environment.getJobName();
        switch (jobName) {
            case REALTY_SOLR:
                stopWatch.start("realty solr index");
                realtySolrIndexJob.run();
                stopWatch.stop();
                break;
            case VISIT:
                stopWatch.start("visit");
                realtyInteractionMigrateJob.visit();
                stopWatch.stop();
                break;
            case VISIT_DETAIL:
                stopWatch.start("visit detail");
                realtyInteractionDetailMigrateJob.visit();
                stopWatch.stop();
                break;
            case FAVORITE:
                stopWatch.start("favorite");
                realtyInteractionMigrateJob.favorite();
                stopWatch.stop();
                break;
            case FAVORITE_DETAIL:
                stopWatch.start("favorite detail");
                realtyInteractionDetailMigrateJob.favorite();
                stopWatch.stop();
                break;
            case MESSAGE:
                stopWatch.start("message");
                realtyInteractionMigrateJob.message();
                stopWatch.stop();
                break;
            case MESSAGE_DETAIL:
                stopWatch.start("message detail");
                realtyInteractionDetailMigrateJob.message();
                stopWatch.stop();
                break;
            case NOT_FOUND:
                logger.error("Not found job name");
                break;
        }

        System.out.println(stopWatch.prettyPrint());
        System.exit(1);
    }


}


