package com.hurriyet.emlak.job.config;


import com.hurriyet.emlak.job.service.RealtyInteractionDetailMigrateJob;
import com.hurriyet.emlak.service.CassandraRealtyInteractionService;

import com.hurriyet.emlak.service.Impl.CassandraRealtyInteractionServiceImpl;
import com.hurriyet.emlak.job.service.RealtySolrIndexJob;
import com.hurriyet.emlak.job.service.RealtyInteractionTotalMigrateJob;
import com.hurriyet.emlak.service.utility.CassandraSessionManager;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppBeans {


    @Autowired
    Environment environment;

    @Bean
    public CassandraSessionManager sessionManager() {
        return new CassandraSessionManager(environment.getCassandraContactPoints(), environment.getCassandraKeyspace());
    }

    @Bean
    public CassandraRealtyInteractionService realtyInteractionService() {
        return new CassandraRealtyInteractionServiceImpl(sessionManager().getSession());
    }

    @Bean
    public RealtySolrIndexJob realtySolrIndexJob() {
        return new RealtySolrIndexJob(environment.getSolrIndexType());
    }

    @Bean
    public SolrClient solrClient() {
        return new HttpSolrClient.Builder(environment.getSolrServer()).build();
    }

    @Bean
    public RealtyInteractionTotalMigrateJob realtyInteractionTotalMigrateJob() {
        return new RealtyInteractionTotalMigrateJob();
    }

    @Bean
    public RealtyInteractionDetailMigrateJob realtyInteractionDetailMigrateJob() {
        return new RealtyInteractionDetailMigrateJob();
    }

}
