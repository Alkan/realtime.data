package com.hurriyet.emlak.job.config;


public class Environment {

    private JobName jobNameName;

    private String cassandraContactPoints;

    private String cassandraPort;

    private String cassandraKeyspace;


    private String solrServer;

    private String solrIndexType;

    public Environment(String jobName,
                       String cassandraContactPoints,
                       String cassandraPort,
                       String cassandraKeyspace,
                       String solrServer,
                       String solrIndexType) {

        this.setJobName(jobName);
        this.setCassandraContactPoints(cassandraContactPoints);
        this.setCassandraPort(cassandraPort);
        this.setCassandraKeyspace(cassandraKeyspace);
        this.setSolrServer(solrServer);
        this.setSolrIndexType(solrIndexType);

    }

    public JobName getJobName() {
        return jobNameName;
    }

    public void setJobName(String jobName) {
        this.jobNameName = JobName.convert(jobName);
    }

    public String getCassandraContactPoints() {
        return cassandraContactPoints;
    }

    public void setCassandraContactPoints(String cassandraContactPoints) {
        this.cassandraContactPoints = cassandraContactPoints;
    }

    public String getCassandraPort() {
        return cassandraPort;
    }

    public void setCassandraPort(String cassandraPort) {
        this.cassandraPort = cassandraPort;
    }

    public String getCassandraKeyspace() {
        return cassandraKeyspace;
    }

    public void setCassandraKeyspace(String cassandraKeyspace) {
        this.cassandraKeyspace = cassandraKeyspace;
    }

    public String getSolrServer() {
        return solrServer;
    }

    public void setSolrServer(String solrUrl) {
        this.solrServer = solrUrl;
    }

    public String getSolrIndexType() {
        return solrIndexType;
    }

    public void setSolrIndexType(String solrIndexType) {
        this.solrIndexType = solrIndexType;
    }
}
