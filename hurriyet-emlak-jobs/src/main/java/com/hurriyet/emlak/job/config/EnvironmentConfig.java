package com.hurriyet.emlak.job.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.text.MessageFormat;


@Configuration
public class EnvironmentConfig {

    private final static Logger logger = Logger.getLogger(EnvironmentConfig.class);

    @Value("${job.name}")
    String jobName;

    @Value("${cassandra.contact.points}")
    String cassandraContactPoints;

    @Value("${cassandra.port}")
    String cassandraPort;

    @Value("${cassandra.keyspace}")
    String cassandraKeyspace;

    @Value("${solr.server}")
    String solrServer;

    @Value("${solr.index.type}")
    String solrIndexType;

    @Bean
    public Environment environment() {
        //logger.info(MessageFormat.format("Java Heap Size is : {0}", Runtime.getRuntime().totalMemory()));
        logger.info(MessageFormat.format("solr.server: {0}", solrServer));
        logger.info(MessageFormat.format("solr.index.type: {0}", solrIndexType));
        logger.info(MessageFormat.format("job.name: {0}", jobName));
        return new Environment(jobName, cassandraContactPoints, cassandraPort, cassandraKeyspace, solrServer, solrIndexType);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {

        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        String activeProfile = System.getProperty("spring.profiles.active");
        String configFile = "application-prod.properties";

        if (activeProfile == null){
            System.setProperty("spring.profiles.active", "prod");
        }

        if ("dev".equals(activeProfile)) {
            configFile = "application-dev.properties";
        }
        else if ("beta".equals(activeProfile)){
            configFile = "application-beta.properties";
        }

        logger.info(MessageFormat.format("Config file: {0}", configFile));
        Resource resource = new ClassPathResource(configFile);

        propertySourcesPlaceholderConfigurer.setLocation(resource);

        return propertySourcesPlaceholderConfigurer;
    }

}