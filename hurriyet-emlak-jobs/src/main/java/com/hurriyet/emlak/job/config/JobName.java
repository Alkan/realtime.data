package com.hurriyet.emlak.job.config;


import java.util.Objects;

public  enum JobName {
    VISIT, VISIT_DETAIL, FAVORITE, FAVORITE_DETAIL, MESSAGE, MESSAGE_DETAIL, REALTY_SOLR, NOT_FOUND;

    public static JobName convert(String name) {

        String jobName = name.replace("-", "_").toUpperCase();

        for (JobName demoType : JobName.values()) {
            if (Objects.equals(jobName, new String(demoType.toString()))) {
                return demoType;
            }
        }
        return NOT_FOUND;
    }
}