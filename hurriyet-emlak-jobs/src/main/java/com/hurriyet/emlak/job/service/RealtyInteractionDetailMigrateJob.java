package com.hurriyet.emlak.job.service;


import com.hurriyet.emlak.job.util.Helper;
import com.hurriyet.emlak.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by aesmer on 31.08.2016.
 */
public class RealtyInteractionDetailMigrateJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final static String START_MIGRATE_DATE = "2016-06-22";
    private final static String END_MIGRATE_DATE = "2016-08-30";
    private final static int BULK_INSERT_ITEM_COUNT = 200000;

    @Autowired
    RealtyService realtyService;

    @Autowired
    private RealtyVisitService realtyVisitService;

    @Autowired
    RealtyFavoriteService realtyFavoriteService;

    @Autowired
    RealtyMessageService realtyMessageService;

    @Autowired
    CassandraRealtyInteractionService cassandraRealtyInteractionService;

    public void visit() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from={} to={}", from, toIndex);
            List<Object[]> rows = realtyVisitService.spRealtyVisitDetail(from, toIndex, START_MIGRATE_DATE, END_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                BigInteger realtyCount = (BigInteger )row[1];
                LocalDate createDate = ((Timestamp) row[2]).toLocalDateTime().toLocalDate();
                cassandraRealtyInteractionService.InsertVisit(realtyId, realtyCount,createDate);
                System.out.println(MessageFormat.format("visit: id: {0} Count: {1} Date: {2}" , realtyId, realtyCount, createDate));
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);
    }

    public void favorite() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from={} to={}", from, toIndex);
            List<Object[]> rows = realtyFavoriteService.spRealtyFavoriteDetail(from, toIndex, START_MIGRATE_DATE, END_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                int realtyCount = (Integer )row[1];
                LocalDate createDate = Helper.getDate(row[2]);
                cassandraRealtyInteractionService.InsertFavorite(realtyId, realtyCount, createDate);
                System.out.println(MessageFormat.format("favorite: id: {0} Count: {1} Date: {2}" , realtyId, realtyCount, createDate));
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);
    }

    public void message() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from=" + from + " to=" + toIndex);
            List<Object[]> rows = realtyMessageService.spRealtyMessageDetail(from, toIndex, START_MIGRATE_DATE, END_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                int realtyCount = (Integer )row[1];
                LocalDate createDate = Helper.getDate(row[2]);
                cassandraRealtyInteractionService.InsertMessage(realtyId, realtyCount, createDate);
                System.out.println(MessageFormat.format("Message: id: {0} Count: {1} Date: {2}" , realtyId, realtyCount, createDate));
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);
    }
}
