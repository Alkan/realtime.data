package com.hurriyet.emlak.job.service;


import com.hurriyet.emlak.service.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by aesmer on 31.08.2016.
 */
public class RealtyInteractionTotalMigrateJob {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());
   private final static String START_MIGRATE_DATE = "2016-08-29";
   private final static int BULK_INSERT_ITEM_COUNT = 200000;

    @Autowired
    RealtyService realtyService;

    @Autowired
    private RealtyVisitService realtyVisitService;

    @Autowired
    RealtyFavoriteService realtyFavoriteService;

    @Autowired
    RealtyMessageService realtyMessageService;

    @Autowired
    CassandraRealtyInteractionService cassandraRealtyInteractionService;


    public void visit() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from=" + from + " to=" + toIndex);
            List<Object[]> rows = realtyVisitService.spRealtyVisitTotal(from, toIndex, START_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                BigInteger realtyCount = (BigInteger )row[1];
                cassandraRealtyInteractionService.IncrementVisit(realtyId, realtyCount);
                logger.info("visit: id=" + realtyId + " Count=" + realtyCount);
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);

    }

    public void favorite() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from=" + from + " to=" + toIndex);
            List<Object[]> rows = realtyFavoriteService.spRealtyFavoriteTotal(from, toIndex, START_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                int realtyCount = (Integer )row[1];
                cassandraRealtyInteractionService.IncrementFavorite(realtyId, realtyCount);
                logger.info("favorite: id=" + realtyId + " Count=" + realtyCount);
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);
    }

    public void message() {
        int realtyMaxId = realtyService.getRealtyMaxId();
        int from = 0;
        int to = 0;
        do {
            to++;
            int toIndex = BULK_INSERT_ITEM_COUNT * to;
            logger.info("migrate: from=" + from + " to=" + toIndex);
            List<Object[]> rows = realtyMessageService.spRealtyMessageTotal(from, toIndex, START_MIGRATE_DATE);

            for (Object[] row : rows) {
                int realtyId = (Integer)row[0];
                int realtyCount = (Integer )row[1];
                cassandraRealtyInteractionService.IncrementMessage(realtyId, realtyCount);
                logger.info("Message: id=" + realtyId + " Count=" + realtyCount);
            }

            from += BULK_INSERT_ITEM_COUNT;
        } while (from < realtyMaxId);
    }
}
