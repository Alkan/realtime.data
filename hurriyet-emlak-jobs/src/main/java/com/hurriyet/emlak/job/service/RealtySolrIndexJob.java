package com.hurriyet.emlak.job.service;


import com.hurriyet.emlak.domain.view.RealtySolrView;
import com.hurriyet.emlak.job.util.*;
import com.hurriyet.emlak.service.RealtySolrService;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.common.SolrInputDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;


public class RealtySolrIndexJob {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static int INITIAL_PAGE_SIZE = 150_000;
    private final static int INITIAL_INDEX_SIZE = 1000;

    @Autowired
    private SolrClient solrClient;

    @Autowired
    private RealtySolrService realtySolrService;

    @Autowired
    private MailingUtil mailingUtil;

    private String solrIndexType;


    public RealtySolrIndexJob(String solrIndexType) {
        this.solrIndexType = solrIndexType;
    }

    public void run() {

        try {
            System.out.println(MessageFormat.format("Solr {0} index started {1}", solrIndexType, Helper.getDateTime()));
            startDeleteIndex();

            if (solrIndexType.equals("full")) {
                startFullIndex();
            }
            else if (solrIndexType.equals("delta")){
                startDeltaIndex();
            }
        }
        catch (Exception ex){
            logger.error(ex.toString());
            mailingUtil.sendForSolr(ex.toString(), solrIndexType);
        }
    }

    private void startDeleteIndex () throws Exception {
        LocalDateTime lastIndexTime = FileUtil.get(LastTime.DELETE);
        List<String> lstRealtyDeleted = realtySolrService.getDeletedList(lastIndexTime);
        FileUtil.save(LastTime.DELETE);
        logger.info("Delete count:{}", lstRealtyDeleted.size());

        if (lstRealtyDeleted.size() > 0) {
            solrClient.deleteById(lstRealtyDeleted);
        }
    }

    private void startDeltaIndex() {
        LocalDateTime lastIndexTime = FileUtil.get(LastTime.INDEX);
        List<RealtySolrView> realtySolrViewList = realtySolrService.realtyListByDate(lastIndexTime);
        logger.info("Delta index count:{}", realtySolrViewList.size());
        sendToSolr(realtySolrViewList);
    }

    private void startFullIndex() throws Exception {

        int nPageSize = INITIAL_PAGE_SIZE;
        int nProcessed = 0;
        int nErrorPosition = 0;
        long nRecordCount = realtySolrService.count();
        logger.info("Full index count:{}", nRecordCount);

        while (nProcessed < nRecordCount){

            try {
                //System.out.println(MessageFormat.format("From Database: {0} To: {1} Count: {2}", nProcessed, nPageSize, nRecordCount));
                logger.info("From Database: {} To: {} Count: {}", nProcessed, nPageSize, nRecordCount);
                sendToSolr(realtySolrService.realtyList(nProcessed, nPageSize));
            }
            catch (Exception ex){
                nErrorPosition = (nProcessed + nPageSize);
                logger.error("Database Progress: {}", ex);
                if (nPageSize >= 10) {
                    nPageSize = nPageSize / 10;
                    continue;
                }

                nPageSize = INITIAL_PAGE_SIZE;
                nProcessed++;
                continue;
            }

            nProcessed += nPageSize;

            if (nProcessed > nErrorPosition){
                nPageSize = INITIAL_PAGE_SIZE;
            }
        }

        solrClient.optimize();
    }

    private void sendToSolr(List<RealtySolrView> lstRealty ) {

        FileUtil.save(LastTime.INDEX);
        int nPageSize = INITIAL_INDEX_SIZE;
        int nFromIndex = 0;
        int nToIndex = 0;
        int nErrorCount = 0;
        int nErrorPosition = 0;

        while (nFromIndex < lstRealty.size()){
            try {

                nToIndex = (nFromIndex + nPageSize);
                if (nToIndex > lstRealty.size()){
                    nToIndex = lstRealty.size();
                }

                Collection<SolrInputDocument> lstDocument = new ArrayList<>();

                System.out.println(MessageFormat.format("From Index: {0} To Index: {1} Page Size: {2}", nFromIndex, nToIndex, nPageSize));

                for(RealtySolrView realty : lstRealty.subList(nFromIndex, nToIndex)) {
                    lstDocument.add(getSolrDocument(realty));
                }

                if (lstDocument.size() > 0) {
                    solrClient.add(lstDocument);
                    //solrClient.commit();
                }
            }
            catch (Exception ex){
                nErrorPosition = nToIndex;
                logger.error("Solr Index Exception: {}", ex);
                if (nPageSize >= 10) {
                    nPageSize = nPageSize / 10;
                    continue;
                }

                nErrorCount++;
                nPageSize = INITIAL_INDEX_SIZE;
                nFromIndex++;
                continue;
            }

            nFromIndex += nPageSize;

            if (nFromIndex > nErrorPosition){
                nPageSize = INITIAL_INDEX_SIZE;
            }
        }

        if(nErrorCount > 0){
            logger.error("error count: {}", nErrorCount);
        }
    }

    private SolrInputDocument getSolrDocument(RealtySolrView realty) {

        SolrInputDocument document = new SolrInputDocument();

        document.addField("RealtyID", realty.getRealtyId());
        document.addField("RealtyFirmID", realty.getRealtyFirmId());
        document.addField("RealtyFirmLogo", realty.getRealtyFirmLogo());
        document.addField("RealtyNo", realty.getRealtyNo());
        document.addField("RealtyTitle", realty.getRealtyTitle());
        document.addField("RealtyDescription", SolrUtil.regexClearHtml(realty.getRealtyDescription()));
        document.addField("RealtyCategory", realty.getRealtyCategory());
        document.addField("RealtyCategoryID", realty.getRealtyCategoryId());
        document.addField("RealtyMainCategory", realty.getRealtyMainCategory());
        document.addField("RealtyMainCategoryID", realty.getRealtyMainCategoryId());
        document.addField("RealtyBathRoom", realty.getRealtyBathroom());
        document.addField("RealtyLivingRoom", realty.getRealtyLivingRoom());
        document.addField("RealtyCityID", realty.getRealtyCityId());
        document.addField("RealtyCityDescription", realty.getRealtyCityDescription());
        document.addField("RealtyCountyDescription", realty.getRealtyCountyDescription());
        document.addField("RealtyCountyID", realty.getRealtyCountyId());
        document.addField("RealtyDistrictDescription", realty.getRealtyDistrictDescription());
        document.addField("RealtyDistrictID", realty.getRealtyDistrictId());
        document.addField("RealtyAreaIDs", SolrUtil.splitToList(realty.getRealtyAreaIDs()));
        document.addField("RealtyPriceTL",  realty.getRealtyPriceTl() != null ? realty.getRealtyPriceTl().doubleValue() : 0.0 );
        document.addField("RealtyPrice", realty.getRealtyPrice() != null ? realty.getRealtyPrice().doubleValue() : 0.0 );
        document.addField("RealtyPriceCurrency", realty.getRealtyPriceCurrency());
        document.addField("RealtyPriceCurrencyID", realty.getRealtyPriceCurrencyId());
        document.addField("RealtyPriceShow", realty.getRealtyPriceShow());
        document.addField("RealtyRoomInfo", realty.getRealtyRoomInfo());
        document.addField("RealtyRoom", realty.getRealtyRoom());
        document.addField("RealtySqm", realty.getRealtySqm());
        document.addField("RealtyCreatedDateTime", realty.getRealtyCreatedDateTime());
        document.addField("RealtyUpdatedDateTime", realty.getRealtyUpdatedDateTime());
        document.addField("RealtyStartDateTime", realty.getRealtyStartDateTime());
        document.addField("RealtyEndDateTime", realty.getRealtyEndDateTime());
        document.addField("RealtySortOrder", realty.getRealtySortOrder() != null ? realty.getRealtySortOrder().doubleValue() : 0.0 );
        document.addField("RealtyProductBackgroundWithColor", realty.getRealtyProductBackgroundWithColor());
        document.addField("RealtyProductBoldTitle", realty.getRealtyProductBoldTitle());
        document.addField("RealtyProductCount", realty.getRealtyProductCount());
        document.addField("RealtyActivate", realty.getRealtyActivate());
        document.addField("RealtyActivateID", realty.getRealtyActivateId());
        document.addField("RealtyAdvertiseOwner", realty.getRealtyAdvertiseOwner());
        document.addField("RealtyAdvertiseOwnerID", realty.getRealtyAdvertiseOwnerId());
        document.addField("RealtyAge", realty.getRealtyAge());
        document.addField("RealtyAttributeInIDs", SolrUtil.splitToList(realty.getRealtyAttributeInIDs()));
        document.addField("RealtyAttributeLocationIDs", SolrUtil.splitToList(realty.getRealtyAttributeLocationIDs()));
        document.addField("RealtyAttributeOutIDs", SolrUtil.splitToList(realty.getRealtyAttributeOutIDs()));
        document.addField("RealtyAttributeInfrastructureIDs", SolrUtil.splitToList(realty.getRealtyAttributeInfrastructureIDs()));
        document.addField("RealtyBuildID", realty.getRealtyBuildId());
        document.addField("RealtyBuildStateID", realty.getRealtyBuildStateId());
        document.addField("RealtyCreditID", realty.getRealtyCreditId());
        document.addField("RealtyFileCount", realty.getRealtyFileCount());
        document.addField("RealtyFirmRealEstateOrganizationID", realty.getRealtyFirmRealEstateOrganizationId());
        document.addField("RealtyFirmTypeID", realty.getRealtyFirmTypeId());
        document.addField("RealtyFirmUserID", realty.getRealtyFirmUserId());
        document.addField("RealtyFlatReceivedID", realty.getRealtyFlatReceivedId());
        document.addField("RealtyRegisterID", realty.getRealtyRegisterId());
        document.addField("RealtyFloorCount", realty.getRealtyFloorCount());
        document.addField("RealtyFloorID", realty.getRealtyFloorId());
        document.addField("RealtyFuelID", realty.getRealtyFuelId());
        document.addField("RealtyHeatingID", realty.getRealtyHeatingId());
        document.addField("RealtyImage", realty.getRealtyImage());
        document.addField("RealtyIsPopulated", realty.getRealtyIsPopulated());
        document.addField("RealtyIsStudentOrSingle", realty.getRealtyIsStudentOrSingle());
        document.addField("RealtyKeywords", realty.getRealtyKeywords());
        document.addField("RealtyMultimediaIDs", SolrUtil.splitToList(realty.getRealtyMultimediaIDs()));
        document.addField("RealtyProjectID", realty.getRealtyProjectId());
        document.addField("RealtyPublishID", realty.getRealtyPublishId());
        document.addField("RealtyResidenceID", realty.getRealtyResidenceId());
        document.addField("RealtyStar", realty.getRealtyStar());
        document.addField("RealtySubCategory", realty.getRealtySubCategory());
        document.addField("RealtySubCategoryID", realty.getRealtySubCategoryId());
        document.addField("RealtyTimeShareTerm", realty.getRealtyTimeShareTerm());
        document.addField("RealtyUsageID", realty.getRealtyUsageId());
        document.addField("RealtyVideo", realty.getRealtyVideo());
        document.addField("RealtyPlace", realty.getRealtyPlace());
        document.addField("RealtyBarterID", realty.getRealtyBarterId());
        document.addField("RealtyTagProductIDs", SolrUtil.splitToList(realty.getRealtyTagProductIDs()));
        document.addField("RealtyByTransfer", realty.getRealtyByTransfer());
        document.addField("RealtyFirmHasVIPPacket", realty.isRealtyFirmHasVipPacket());
        document.addField("RealtyHousingComplexName", realty.getRealtyHousingComplexName());
        document.addField("RealtyLatitudeAndLongtitude", SolrUtil.getRealtyMapLocation(realty.getRealtyMapLatitude(), realty.getRealtyMapLongtitude()));
        document.addField("RealtyMapPublishID",  SolrUtil.getRealtyMapPublishId(realty));
        document.addField("RealtyIsCityTransformation", realty.getRealtyIsCityTransformation());
        document.addField("RealtyRoomAndLivingRoom", SolrUtil.getRealtyRoomAndLivingRoomTypeId(realty.getRealtyRoom(), realty.getRealtyLivingRoom()));
        document.addField("RealtyStarID", realty.getRealtyStarId());
        document.addField("RealtyBedCount", realty.getRealtyBedCount());
        document.addField("RealtyAttributeRoomSocialInstitutionIDs", SolrUtil.splitToList(realty.getRealtyAttributeRoomSocialInstitutionIDs()));
        document.addField("RealtyAttributeDormIDs", SolrUtil.splitToList(realty.getRealtyAttributeDormIDs()));
        document.addField("RealtyAttributeDormLocationIDs", realty.getRealtyAttributeDormLocationIDs());
        document.addField("RealtyAttributeRoomIDs", SolrUtil.splitToList(realty.getRealtyAttributeRoomIDs()));
        document.addField("RealtyDormitoryCapacity", realty.getRealtyDormitoryCapacity());
        document.addField("RealtyDormitoryRoomCapacityIDs", SolrUtil.splitToList(realty.getRealtyDormitoryRoomCapacityIDs()));
        document.addField("RealtyDormitoryRoomPriceRangeIDs", SolrUtil.splitToList(realty.getRealtyDormitoryRoomPriceRangeIDs()));
        document.addField("RealtyAttributeUsageIDs", SolrUtil.splitToList(realty.getRealtyAttributeUsageIDs()));
        document.addField("RealtyFirmBlackListTypeID", realty.getRealtyFirmBlackListTypeId());
        document.addField("RealtyIsHousingComplex", realty.isRealtyIsHousingComplex());
        document.addField("RealtyIsAuthorizedRealtor", realty.getRealtyIsAuthorizedRealtor());
        document.addField("RealtyImageCount", realty.getRealtyImageCount());
        document.addField("SuperRealtyStartDateTime", realty.getSuperRealtyStartDateTime());
        document.addField("SuperRealtyEndDateTime", realty.getSuperRealtyEndDateTime());
        document.addField("SuperRealtyAreaID", realty.getSuperRealtyAreaId());
        document.addField("IsSuperRealty", SolrUtil.isSuperRealty(realty.getSuperRealtyStartDateTime(), realty.getSuperRealtyEndDateTime()));
        document.addField("RealtyTitleSearchField", SolrUtil.getLoremIpsum(realty.getRealtyTitle()));
        document.addField("RealtyFirmShortName", realty.getFirmShortName());
        document.addField("RealtyOcsProductIDs", SolrUtil.splitToList(realty.getRealtyOcsProductIDs()));
        document.addField("RealtyFirmPackageCategoryTypeID", realty.getRealtyFirmPackageCategoryTypeId());
        document.addField("FirmScore", realty.getFirmScoreTotal());
        document.addField("RealtyScore", realty.getRealtyScore());
        document.addField("RealtyIsCreatedToday", SolrUtil.isRealtyIsCreatedToday(realty.getRealtyCreatedDateTime()));
        document.addField("RealtyCleanLandRegisterFilePath", realty.getRealtyCleanLandRegisterFilePath());
        document.addField("RealtyDateScore",  SolrUtil.getDateScore(realty.getRealtyCreatedDateTime(), realty.getRealtyUpdatedDateTime()));
        document.addField("RealtyIsCleanLanding", !SolrUtil.isNullOrBlank(realty.getRealtyCleanLandRegisterFilePath()));

        Map<String, Object> clsFavoriteScore = new HashMap<>();
        clsFavoriteScore.put("inc", 0);
        document.addField("RealtyFavoriteScore", clsFavoriteScore);

        Map<String, Object> clsMessageScore = new HashMap<>();
        clsMessageScore.put("inc", 0);
        document.addField("RealtyMessageScore", clsMessageScore);

        Map<String, Object> clsRealtyVisitScore = new HashMap<>();
        clsRealtyVisitScore.put("inc", 0);
        document.addField("RealtyVisitScore", clsRealtyVisitScore);
        return document;
    }

}
