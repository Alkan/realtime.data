package com.hurriyet.emlak.job.util;


import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class FileUtil {

    protected static String fileName;




    private static void setFileName(LastTime lastTime) {

        switch (lastTime) {
            case INDEX:
                fileName = "last-index-time.txt";
                break;
            case DELETE:
                fileName = "last-delete-time.txt";
                break;
        }
    }

    public static void save(LastTime lastTime) {

        setFileName(lastTime);

        try {
            FileWriter fileWriter = new FileWriter(fileName);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(Helper.getDateTime());
            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println(ex);
        }
    }

    public static LocalDateTime get(LastTime lastTime){

        setFileName(lastTime);

        String lastDateTime = null;

        try {

            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            lastDateTime = bufferedReader.readLine();
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }

        return lastDateTime != null? Helper.getDateTime(lastDateTime).minusMinutes(1): LocalDateTime.now().minusMinutes(15);
    }

}
