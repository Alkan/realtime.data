package com.hurriyet.emlak.job.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ahmet on 18/09/16.
 */
public class Helper {


    protected static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    protected static DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public final static String getDateTime() {
        LocalDateTime dateTime = LocalDateTime.now().minusMinutes(2);
        return dateTime.format(formatter);
    }

    public final static LocalDateTime getDateTime(String dateTime) {
        return LocalDateTime.parse(dateTime, formatter);
    }

    public final static LocalDate getDate(Object dateTime) {
        return LocalDate.parse(dateTime.toString(), formatterDate);
    }
}
