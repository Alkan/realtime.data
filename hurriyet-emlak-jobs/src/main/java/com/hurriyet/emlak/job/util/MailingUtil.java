package com.hurriyet.emlak.job.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;
import java.time.LocalDateTime;

/**
 * Created by aesmer on 16.09.2016.
 */
public class MailingUtil {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String SUBJECT_MAIL_SOLR_EXCEPTION = "Solr Index Exception";

    @Autowired
    private JavaMailSender javaMailSender;

    private String fromEmail;
    private String[] toSolrExceptionEmailAddress;


    public MailingUtil(String fromEmail, String[] toSolrExceptionEmailAddress){
        this.fromEmail = fromEmail;
        this.toSolrExceptionEmailAddress = toSolrExceptionEmailAddress;
    }


    public void sendForSolr(String exception, String solrIndexType)  {

        try {
            String sBody = MessageFormat.format("Solr ERROR!!!</br> Index Type: {0} </br>  SentDate: {1} </br> Error: {2}<br> ",
                    solrIndexType, LocalDateTime.now(), exception);

            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
            mimeMessage.setContent(sBody, "text/html");
            helper.setTo(toSolrExceptionEmailAddress);
            helper.setFrom(fromEmail);
            helper.setSubject(SUBJECT_MAIL_SOLR_EXCEPTION);
            javaMailSender.send(mimeMessage);
        }
        catch (Exception ex){
            logger.error(ex.toString());
        }
    }

}
