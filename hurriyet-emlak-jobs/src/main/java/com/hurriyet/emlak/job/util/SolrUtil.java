package com.hurriyet.emlak.job.util;

import com.hurriyet.emlak.domain.view.RealtySolrView;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by ahmet on 08/09/16.
 */
public class SolrUtil {

    public static String getRealtyMapLocation(BigDecimal dLatitude, BigDecimal dLongtitude) {
        if (dLatitude != null && dLongtitude != null ) {
            return String.format("%1$s,%2$s", dLatitude.doubleValue(), dLongtitude.doubleValue());
        }
        return null;
    }

    public static boolean isSuperRealty(Timestamp startDateTime, Timestamp endDateTime) {
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        return !isNullOrBlank(startDateTime) && !isNullOrBlank(endDateTime) && now.getTime() > startDateTime.getTime() && now.getTime() < endDateTime.getTime();
    }

    public static int getDateScore(Timestamp createdDateTime, Timestamp updatedDateTime) {
        int days = (int) ChronoUnit.DAYS.between(createdDateTime.toLocalDateTime(), LocalDateTime.now());
        if (days < 30) {
            if (days < 1) {
                return 70;
            }
            else if (days < 3) {
                return 60;
            }
            else if (days < 7) {
                return 50;
            }
            else if (days < 14) {
                return 40;
            }
            else {
                return 30;
            }
        }
        else if (!isNullOrBlank(updatedDateTime)) {
            days = (int) ChronoUnit.DAYS.between(updatedDateTime.toLocalDateTime(), LocalDateTime.now());
            if (days < 14) {
                if (days < 1) {
                    return 30;
                }
                else if (days < 3) {
                    return 25;
                }
                else if (days < 7) {
                    return 20;
                }
                else {
                    return 15;
                }
            }
        }
        return 0;
    }

    public static boolean isRealtyIsCreatedToday(Timestamp createdDateTime) {
        return createdDateTime != null && createdDateTime.toLocalDateTime().toLocalDate() == LocalDate.now();
    }

    public static boolean isNullOrBlank(Object param) {
        return param == null || param.toString().trim().length() == 0;
    }

    public static Integer getRealtyMapPublishId(RealtySolrView realty) {

        if (realty.getRealtyMapLatitude() == null || realty.getRealtyMapLongtitude() == null){
            return 100405;
        }
        else {
            return  realty.getRealtyMapPublishId();
        }
    }

    public static String regexClearHtml(String objType) {
        return objType == null ? "" : objType.replaceAll("<.*?>", "");
    }

    public static List<String> splitToList(String values) {
        List<String> list = new ArrayList<String>();
        if (!isNullOrBlank(values)){
            for(String s : values.split(",")) {
                list.add(s.trim());
            }
        }
        return list;
    }

    public static int getRealtyRoomAndLivingRoomTypeId(int nRealtyRoom, int nRealtyLivingRoom) {
        int nReturn;

        if (nRealtyRoom == 1 && nRealtyLivingRoom == 0) {
            nReturn = 180401; //1
        }
        else if (nRealtyRoom == 1 && nRealtyLivingRoom >= 1) {
            nReturn = 180402; //1+1
        }
        else if (nRealtyRoom == 2 && nRealtyLivingRoom == 0) {
            nReturn = 180403; //2
        }
        else if (nRealtyRoom == 2 && nRealtyLivingRoom == 1) {
            nReturn = 180404; //2+1
        }
        else if (nRealtyRoom == 2 && nRealtyLivingRoom >= 2) {
            nReturn = 180405; //2+2
        }
        else if (nRealtyRoom == 3 && nRealtyLivingRoom == 0) {
            nReturn = 180406; //3
        }
        else if (nRealtyRoom == 3 && nRealtyLivingRoom == 1) {
            nReturn = 180407; //3+1
        }
        else if (nRealtyRoom == 3 && nRealtyLivingRoom == 2) {
            nReturn = 180408; //3+2
        }
        else if (nRealtyRoom == 3 && nRealtyLivingRoom >= 3) {
            nReturn = 180409; //3+3
        }
        else if (nRealtyRoom == 4 && nRealtyLivingRoom == 0) {
            nReturn = 180410; //4
        }
        else if (nRealtyRoom == 4 && nRealtyLivingRoom == 1) {
            nReturn = 180411; //4+1
        }
        else if (nRealtyRoom == 4 && nRealtyLivingRoom == 2) {
            nReturn = 180412; //4+2
        }
        else if (nRealtyRoom == 4 && nRealtyLivingRoom == 3) {
            nReturn = 180413; //4+3
        }
        else if (nRealtyRoom == 4 && nRealtyLivingRoom >= 4) {
            nReturn = 180414; //4+3
        }
        else if (nRealtyRoom == 5 && nRealtyLivingRoom == 0) {
            nReturn = 180415; //5
        }
        else if (nRealtyRoom == 5 && nRealtyLivingRoom == 1) {
            nReturn = 180416; //5+1
        }
        else if (nRealtyRoom == 5 && nRealtyLivingRoom == 2) {
            nReturn = 180417; //5+2
        }
        else if (nRealtyRoom == 5 && nRealtyLivingRoom == 3) {
            nReturn = 180418; //5+3
        }
        else if (nRealtyRoom == 5 && nRealtyLivingRoom >= 4) {
            nReturn = 180419; //5+4
        }
        else if (nRealtyRoom == 6 && nRealtyLivingRoom == 0) {
            nReturn = 180420; //6
        }
        else if (nRealtyRoom == 6 && nRealtyLivingRoom == 1) {
            nReturn = 180421; //6+1
        }
        else if (nRealtyRoom == 6 && nRealtyLivingRoom == 2) {
            nReturn = 180422; //6+2
        }
        else if (nRealtyRoom == 6 && nRealtyLivingRoom == 3) {
            nReturn = 180423; //6+3
        }
        else if (nRealtyRoom == 6 && nRealtyLivingRoom >= 4) {
            nReturn = 180424; //6+4
        }
        else if (nRealtyRoom == 7 && nRealtyLivingRoom == 0) {
            nReturn = 180425; //7
        }
        else if (nRealtyRoom == 7 && nRealtyLivingRoom == 1) {
            nReturn = 180426; //7+1
        }
        else if (nRealtyRoom == 7 && nRealtyLivingRoom == 2) {
            nReturn = 180427; //7+2
        }
        else if (nRealtyRoom == 7 && nRealtyLivingRoom == 3) {
            nReturn = 180428; //7+3
        }
        else if (nRealtyRoom == 7 && nRealtyLivingRoom >= 4) {
            nReturn = 180429; //7+4
        }
        else if (nRealtyRoom == 8 && nRealtyLivingRoom == 0) {
            nReturn = 180430; //8
        }
        else if (nRealtyRoom == 8 && nRealtyLivingRoom == 1) {
            nReturn = 180431; //8+1
        }
        else if (nRealtyRoom == 8 && nRealtyLivingRoom == 2) {
            nReturn = 180432; //8+2
        }
        else if (nRealtyRoom == 8 && nRealtyLivingRoom == 3) {
            nReturn = 180433; //8+3
        }
        else if (nRealtyRoom == 8 && nRealtyLivingRoom >= 4) {
            nReturn = 180434; //8+4
        }
        else {
            nReturn = 180435; //9 ve üzeri
        }

        return nReturn;
    }

    public static String getLoremIpsum(String sTitle) {
        if (!SolrUtil.isNullOrBlank(sTitle) ){
            String[] words = {"lorem", "ipsum","fthgns", "dolor", "sit", "amet", "consectetuer", "adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod", "tincidunt", "ut", "laoreet", "dolore", "magna", "aliquam", "erat","reprehenderit","veniam","cillum","sunt"};

            StringBuilder sbTitle = new StringBuilder();
            sbTitle.append(sTitle);
            int nCount = sTitle.split(" ").length;
            for (int i = 0; i <= 25 - nCount; i++) {
                sbTitle.append(" ");
                sbTitle.append(words[i]);
            }
            return sbTitle.toString();
        }else{
            return sTitle;
        }
    }

}
