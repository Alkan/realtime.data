
package com.hurriyet.emlak.repository;

import com.hurriyet.emlak.domain.entity.RealtyFavoriteEntity;
import org.springframework.data.repository.CrudRepository;


public interface RealtyFavoriteRepository extends CrudRepository<RealtyFavoriteEntity, Integer> {

}
