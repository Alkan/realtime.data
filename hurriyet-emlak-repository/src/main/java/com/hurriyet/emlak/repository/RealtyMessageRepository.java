
package com.hurriyet.emlak.repository;


import com.hurriyet.emlak.domain.entity.RealtyMessageEntity;
import org.springframework.data.repository.CrudRepository;


public interface RealtyMessageRepository extends CrudRepository<RealtyMessageEntity, Integer> {

}
