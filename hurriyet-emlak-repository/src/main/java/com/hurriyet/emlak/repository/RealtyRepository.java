
package com.hurriyet.emlak.repository;


import com.hurriyet.emlak.domain.entity.RealtyEntity;
import org.springframework.data.repository.CrudRepository;

public interface RealtyRepository extends CrudRepository<RealtyEntity, Integer> {

     RealtyEntity findTop1ByOrderByRealtyIdDesc();
}



