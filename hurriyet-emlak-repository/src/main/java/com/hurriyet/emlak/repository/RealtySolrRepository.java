package com.hurriyet.emlak.repository;


import com.hurriyet.emlak.domain.view.RealtySolrView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true)
public interface RealtySolrRepository extends JpaRepository<RealtySolrView, Integer> {

   /* @Query("select count(p) from vwRealtySolr p where p.RealtyUpdatedDateTime >?1")
    Long countByUpdateDate(Timestamp RealtyUpdatedDateTime);*/

}

