package com.hurriyet.emlak.service;


import com.hurriyet.emlak.domain.model.RealtyInteraction;
import com.hurriyet.emlak.domain.model.RealtyTotalInteraction;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

public interface CassandraRealtyInteractionService {

    void IncrementVisit(int realtyId, BigInteger totalCount);
    void InsertVisit(int realtyId, BigInteger totalCount, LocalDate createDate);

    void IncrementFavorite(int realtyId, int totalCount);
    void InsertFavorite(int realtyId, int totalCount, LocalDate createDate);

    void IncrementMessage(int realtyId, int totalCount);
    void InsertMessage(int realtyId, int totalCount, LocalDate createDate);

    List<RealtyTotalInteraction> getList(List<Integer> realtyIDs);

    List<RealtyInteraction> getListByDate(int realtyId, int dayBefore);
}
