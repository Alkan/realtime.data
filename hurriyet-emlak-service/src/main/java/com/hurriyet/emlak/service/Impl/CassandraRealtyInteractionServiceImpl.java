package com.hurriyet.emlak.service.Impl;

import com.datastax.driver.core.*;
import com.hurriyet.emlak.domain.model.RealtyInteraction;
import com.hurriyet.emlak.domain.model.RealtyTotalInteraction;
import com.hurriyet.emlak.service.CassandraRealtyInteractionService;


import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CassandraRealtyInteractionServiceImpl implements CassandraRealtyInteractionService {

    private Session session;

    public CassandraRealtyInteractionServiceImpl(Session session) {
        this.session = session;
    }


    @Override
    public void IncrementVisit(int realtyId, BigInteger totalCount) {
        String cqlScript = String.format("UPDATE realty_total_interaction_counts SET realty_visit_count = realty_visit_count + %1$d WHERE realty_id=%2$d;",
                totalCount, realtyId);
        session.execute(cqlScript);
    }

    @Override
    public void InsertVisit(int realtyId, BigInteger totalCount, LocalDate createDate) {
        String cqlScript = String.format("UPDATE realty_interaction_counts SET realty_visit_count = realty_visit_count + %1$d  WHERE realty_id=%2$d AND realty_interaction_date ='%3$s';",
                totalCount, realtyId, createDate);

        session.execute(cqlScript);
    }

    @Override
    public void IncrementFavorite(int realtyId, int totalCount) {
        String cqlScript = String.format("UPDATE realty_total_interaction_counts SET realty_favorite_count = realty_favorite_count + %1$d WHERE realty_id=%2$d;",
                totalCount, realtyId);
        session.execute(cqlScript);
    }

    @Override
    public void InsertFavorite(int realtyId, int totalCount, LocalDate createDate) {
        String cqlScript = String.format("UPDATE realty_interaction_counts SET realty_favorite_count = realty_favorite_count + %1$d  WHERE realty_id=%2$d AND realty_interaction_date ='%3$s';",
                totalCount, realtyId, createDate);
        session.execute(cqlScript);
    }

    @Override
    public void IncrementMessage(int realtyId, int totalCount) {
        String cqlScript = String.format("UPDATE realty_total_interaction_counts SET realty_message_count = realty_message_count + %1$d WHERE realty_id=%2$d;",
                totalCount, realtyId);
        session.execute(cqlScript);
    }

    @Override
    public void InsertMessage(int realtyId, int totalCount, LocalDate createDate) {
        String cqlScript = String.format("UPDATE realty_interaction_counts SET realty_message_count = realty_message_count + %1$d  WHERE realty_id=%2$d AND realty_interaction_date ='%3$s';",
                totalCount, realtyId, createDate);

        session.execute(cqlScript);
    }

    @Override
    public List<RealtyTotalInteraction> getList(List<Integer> realtyIDs) {

        ArrayList<RealtyTotalInteraction> realtyInteraction = new ArrayList<RealtyTotalInteraction>();

        String commaSeparatedNumbers = realtyIDs.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(","));

         String cqlScript = String.format("SELECT realty_id, realty_visit_count, realty_message_count, realty_favorite_count " +
                "FROM realty_total_interaction_counts WHERE realty_id IN (%1$s)", commaSeparatedNumbers);

        ResultSet resultSet = session.execute(cqlScript);

        for(Row row :resultSet){
            Long visit = row.isNull("realty_visit_count") ? 0  : row.getLong("realty_visit_count");
            Long message = row.isNull("realty_message_count") ? 0  : row.getLong("realty_message_count");
            Long favorite = row.isNull("realty_favorite_count") ? 0  : row.getLong("realty_favorite_count");
            realtyInteraction.add(new RealtyTotalInteraction(row.getInt("realty_id"), visit, message, favorite));
        }

        return realtyInteraction;
    }

    @Override
    public List<RealtyInteraction> getListByDate(int realtyId, int dayBefore){
        ArrayList<RealtyInteraction> realtyInteraction = new ArrayList<RealtyInteraction>();
        LocalDate dtEnd = LocalDate.now();
        LocalDate dtStart = dtEnd.plusDays(-dayBefore);

        String cqlScript = String.format("SELECT realty_id, realty_interaction_date, realty_visit_count, realty_message_count, realty_favorite_count, realty_phone_click_count " +
                "FROM realty_interaction_counts WHERE realty_id = %1$d AND realty_interaction_date >= '%2$s';", realtyId, dtStart);
        ResultSet resultSet = session.execute(cqlScript);

        for(Row row :resultSet){
            /*
            DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            com.datastax.driver.core.LocalDate dt = row.getDate("realty_interaction_date");
            LocalDate localDate = LocalDate.parse(dt.toString(), DATE_FORMAT);
            */
            com.datastax.driver.core.LocalDate dt = row.getDate("realty_interaction_date");

            Long visit = row.isNull("realty_visit_count") ? 0  : row.getLong("realty_visit_count");
            Long message = row.isNull("realty_message_count") ? 0  : row.getLong("realty_message_count");
            Long favorite = row.isNull("realty_favorite_count") ? 0  : row.getLong("realty_favorite_count");
            Long phoneClick = row.isNull("realty_phone_click_count") ? 0  : row.getLong("realty_phone_click_count");
            String realtyDate = row.isNull("realty_interaction_date") ? null  : row.getDate("realty_interaction_date").toString();
            realtyInteraction.add(new RealtyInteraction(row.getInt("realty_id"), visit, message, favorite, phoneClick, realtyDate));
        }
        return realtyInteraction;
    }
}

