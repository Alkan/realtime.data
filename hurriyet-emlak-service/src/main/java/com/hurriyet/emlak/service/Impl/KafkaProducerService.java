package com.hurriyet.emlak.service.Impl;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;


import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaProducerService implements com.hurriyet.emlak.service.KafkaProducerService {

    private String brokerList;

    private boolean async;

    private Producer<String, String> producer;

    public KafkaProducerService(String brokerList, boolean async) {
        this.brokerList = brokerList;
        this.async = async;
    }

    @PostConstruct
    public void initSpringBootKafkaProducer() {

        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", brokerList);
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("acks", "0");
        kafkaProps.put("retries", "1");
        kafkaProps.put("linger.ms", 5);

        producer = new KafkaProducer<>(kafkaProps);
    }

    @Override
    public void send(String topic, String value) throws ExecutionException, InterruptedException {

        if (async) {
            sendAsync(topic, value);
        } else {
            sendSync(topic, value);
        }
    }

    private void sendSync(String topic, String value) throws ExecutionException, InterruptedException {
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, value);
        producer.send(record).get();

    }

    private void sendAsync(String topic, String value) {
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, value);
        producer.send(record, (RecordMetadata recordMetadata, Exception e) -> {
            if (e != null) {
                e.printStackTrace();
            }
        });
    }

}
