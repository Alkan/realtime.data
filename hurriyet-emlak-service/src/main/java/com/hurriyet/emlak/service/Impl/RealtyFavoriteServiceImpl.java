
package com.hurriyet.emlak.service.Impl;

import com.hurriyet.emlak.domain.entity.RealtyFavoriteEntity;
import com.hurriyet.emlak.repository.RealtyFavoriteRepository;
import com.hurriyet.emlak.service.RealtyFavoriteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;


@Service
public class RealtyFavoriteServiceImpl implements RealtyFavoriteService {

    @Resource
    private RealtyFavoriteRepository realtyFavoriteRepository;

    @PersistenceContext
    private EntityManager manager;

    @Override
    @Transactional
    public RealtyFavoriteEntity findById(int realtyVisitId) {
        return realtyFavoriteRepository.findOne(realtyVisitId);
    }

    @Override
    public List spRealtyFavoriteTotal(int realtyStartId, int realtyEndId, String createdDate) {
        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyFavoriteTotal");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("CreatedDate", createdDate);
        return query.getResultList();
    }

    @Override
    public List spRealtyFavoriteDetail(int realtyStartId, int realtyEndId, String startDate, String endDate) {
        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyFavoriteDetail");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("StartDate", startDate);
        query.setParameter("EndDate", endDate);
        return query.getResultList();
    }

}
