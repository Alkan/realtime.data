
package com.hurriyet.emlak.service.Impl;


import com.hurriyet.emlak.domain.entity.RealtyMessageEntity;
import com.hurriyet.emlak.repository.RealtyMessageRepository;
import com.hurriyet.emlak.service.CassandraRealtyInteractionService;
import com.hurriyet.emlak.service.RealtyMessageService;
import com.hurriyet.emlak.service.RealtyService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;



@Service
public class RealtyMessageServiceImpl implements RealtyMessageService {


    @Resource
    private RealtyMessageRepository realtyMessageRepository;

    @PersistenceContext
    private EntityManager manager;

    @Override
    @Transactional
    public RealtyMessageEntity findById(int realtyVisitId) {
        return realtyMessageRepository.findOne(realtyVisitId);
    }

    @Override
    public List spRealtyMessageTotal(int realtyStartId, int realtyEndId, String createdDate) {
        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyMessageTotal");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("CreatedDate", createdDate);
        return query.getResultList();
    }

    @Override
    public List spRealtyMessageDetail(int realtyStartId, int realtyEndId, String startDate, String endDate) {
        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyMessageDetail");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("StartDate", startDate);
        query.setParameter("EndDate", endDate);
        return query.getResultList();
    }

}
