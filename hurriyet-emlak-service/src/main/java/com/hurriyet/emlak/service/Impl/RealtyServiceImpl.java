
package com.hurriyet.emlak.service.Impl;


import com.hurriyet.emlak.domain.entity.RealtyEntity;
import com.hurriyet.emlak.repository.RealtyRepository;
import com.hurriyet.emlak.repository.RealtySolrRepository;
import com.hurriyet.emlak.service.RealtyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class RealtyServiceImpl implements RealtyService {

    @Resource
    private RealtyRepository realtyRepository;

    @Resource
    private RealtySolrRepository  realtySolrRepository;

//    @PersistenceContext
//    private EntityManager manager;

    @Override
    @Transactional
    public RealtyEntity findById(int realtyId) {
        return realtyRepository.findOne(realtyId);
    }

    @Override
    public int getRealtyMaxId() {
        RealtyEntity realtyEntity = realtyRepository.findTop1ByOrderByRealtyIdDesc();
        return realtyEntity.getRealtyId();
    }

}
