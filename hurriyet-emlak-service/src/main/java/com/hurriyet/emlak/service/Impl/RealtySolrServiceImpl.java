
package com.hurriyet.emlak.service.Impl;


import com.hurriyet.emlak.domain.view.RealtySolrView;
import com.hurriyet.emlak.repository.RealtySolrRepository;
import com.hurriyet.emlak.service.RealtySolrService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
public class RealtySolrServiceImpl implements RealtySolrService {


    @Resource
    private RealtySolrRepository  realtySolrRepository;

    @PersistenceContext
    private EntityManager manager;

    @Override
    public RealtySolrView findById(int realtyId) {
        return realtySolrRepository.findOne(realtyId);
    }

    @Override
    public Long count() {
        return realtySolrRepository.count();
    }

    @Override
    public Page<RealtySolrView> listAllByPage(int pageNumber, int pageSize) {
        PageRequest pageable = new PageRequest(pageNumber - 1, pageSize);
       return realtySolrRepository.findAll(pageable);
    }

    public List<RealtySolrView> realtyList() {
        return realtySolrRepository.findAll();
    }

    public List<RealtySolrView> realtyList(int recordPosition, int recordsPerRoundTrip ) {
        return manager.createNamedQuery("RealtySolrView.findAll")
                .setFirstResult(recordPosition )
                .setMaxResults(recordsPerRoundTrip )
                .getResultList();
    }

    public List<RealtySolrView> realtyListByRange(int startRealtyId, int endRealtyId) {
        return manager.createNamedQuery("RealtySolrView.Between")
                .setParameter("startRealty", startRealtyId)
                .setParameter("endRealty", endRealtyId)
                .getResultList();
    }

    public List<String> getDeletedList(LocalDateTime dateTime) {
        TypedQuery<String> query = manager.createNamedQuery("RealtySolrDeleted.findByDate", String.class);
        query.setParameter("date", getDateTime(dateTime));
        return query.getResultList();
    }

    public List<RealtySolrView> realtyListByDate(LocalDateTime dateTime) {
        return manager.createNamedQuery("RealtySolrView.findByDate")
                .setParameter("date", Timestamp.valueOf(dateTime), TemporalType.TIMESTAMP)
                .getResultList();
    }

    private String getDateTime(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return localDateTime.format(formatter);
    }

}
