
package com.hurriyet.emlak.service.Impl;

import com.hurriyet.emlak.service.RealtyVisitService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.hurriyet.emlak.domain.entity.RealtyVisitEntity;
import com.hurriyet.emlak.repository.RealtyVisitRepository;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;


@Service
public class RealtyVisitServiceImpl implements RealtyVisitService {


    @Resource
    private RealtyVisitRepository realtyVisitRepository;

    @PersistenceContext
    private EntityManager manager;

    @Override
    @Transactional
    public RealtyVisitEntity create(RealtyVisitEntity realtyVisitEntity) {
        return realtyVisitRepository.save(realtyVisitEntity);
    }

    @Override
    @Transactional
    public RealtyVisitEntity findById(int realtyVisitId) {
        return realtyVisitRepository.findOne(realtyVisitId);
    }


    @Override
    public List spRealtyVisitTotal(int realtyStartId, int realtyEndId, String createdDate) {

        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyVisitTotal");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("CreatedDate", createdDate);
        return query.getResultList();
    }

    @Override
    public List spRealtyVisitDetail(int realtyStartId, int realtyEndId, String startDate, String endDate) {
        StoredProcedureQuery query = this.manager.createNamedStoredProcedureQuery("spRealtyVisitDetail");
        query.setParameter("RealtyStartID", realtyStartId);
        query.setParameter("RealtyEndID", realtyEndId);
        query.setParameter("StartDate", startDate);
        query.setParameter("EndDate", endDate);
        return query.getResultList();
    }

}
