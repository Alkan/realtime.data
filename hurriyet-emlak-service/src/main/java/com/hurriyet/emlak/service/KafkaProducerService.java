package com.hurriyet.emlak.service;

import java.util.concurrent.ExecutionException;

public interface KafkaProducerService {

    void send(String topic, String value) throws ExecutionException, InterruptedException;
}
