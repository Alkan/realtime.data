
package com.hurriyet.emlak.service;


import com.hurriyet.emlak.domain.entity.RealtyFavoriteEntity;
import java.util.List;

public interface RealtyFavoriteService {

    RealtyFavoriteEntity findById(int realtyVisitId);

    List spRealtyFavoriteTotal(int realtyStartId, int realtyEndId, String createdDate);
    List spRealtyFavoriteDetail(int realtyStartId, int realtyEndId, String startDate, String endDate);

}
