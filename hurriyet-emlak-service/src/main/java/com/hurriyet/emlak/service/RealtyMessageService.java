
package com.hurriyet.emlak.service;

import com.hurriyet.emlak.domain.entity.RealtyMessageEntity;
import java.util.List;

public interface RealtyMessageService {

    RealtyMessageEntity findById(int realtyVisitId);
    List spRealtyMessageTotal(int realtyStartId, int realtyEndId, String createdDate);
    List spRealtyMessageDetail(int realtyStartId, int realtyEndId, String startDate, String endDate);

}
