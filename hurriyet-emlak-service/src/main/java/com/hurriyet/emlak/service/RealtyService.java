
package com.hurriyet.emlak.service;


import com.hurriyet.emlak.domain.entity.RealtyEntity;

public interface RealtyService {

    RealtyEntity findById(int realtyId);

    int getRealtyMaxId();


}
