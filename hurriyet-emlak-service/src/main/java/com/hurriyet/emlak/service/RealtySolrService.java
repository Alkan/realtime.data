
package com.hurriyet.emlak.service;


import com.hurriyet.emlak.domain.view.RealtySolrView;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.List;

public interface RealtySolrService {

    RealtySolrView findById(int realtyId);

    Long count();

    Page<RealtySolrView> listAllByPage(int pageNumber, int pageSize);

   /* Long countByUpdateDate(Timestamp realtyUpdatedDateTime);*/

    List<RealtySolrView> realtyList();

    List<RealtySolrView> realtyList(int recordPosition, int recordsPerRoundTrip);

    List<RealtySolrView> realtyListByRange(int startRealtyId, int endRealtyId);

    List<String> getDeletedList(LocalDateTime dateTime);


    List realtyListByDate(LocalDateTime dateTime);


}
