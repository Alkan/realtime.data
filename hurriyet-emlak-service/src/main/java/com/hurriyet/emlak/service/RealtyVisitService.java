package com.hurriyet.emlak.service;

import com.hurriyet.emlak.domain.entity.RealtyVisitEntity;

import java.util.List;


public interface RealtyVisitService {

    RealtyVisitEntity create(RealtyVisitEntity realtyVisitEntity);

    RealtyVisitEntity findById(int realtyVisitId);

    List spRealtyVisitTotal(int realtyStartId, int realtyEndId, String createdDate);
    List spRealtyVisitDetail(int realtyStartId, int realtyEndId, String startDate, String endDate);

}
