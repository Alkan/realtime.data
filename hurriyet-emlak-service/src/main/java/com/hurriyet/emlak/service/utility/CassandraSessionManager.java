package com.hurriyet.emlak.service.utility;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ProtocolOptions;
import com.datastax.driver.core.Session;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class CassandraSessionManager {

    private Session session;
    private Cluster cluster;
    private String contactPoint;
    private String keyspace;

    public CassandraSessionManager(String contactPoint, String keyspace) {
        this.contactPoint = contactPoint;
        this.keyspace = keyspace;
    }

    public Session getSession() {
        return session;
    }

    @PostConstruct
    public void initIt() {
        String[] points = contactPoint.replace(" ", "").split(",");
        cluster = Cluster.builder().addContactPoints(points).withCompression(ProtocolOptions.Compression.LZ4).build();
        session = cluster.connect(keyspace);
    }

    @PreDestroy
    public void destroy() {
        if (session != null) {
            session.close();
        }
        if (cluster != null) {
            cluster.close();
        }
    }
}
