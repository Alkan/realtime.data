package com.hurriyet.emlak.storm.bolt;

import com.hurriyet.emlak.Keys;
import org.apache.log4j.Logger;

import java.util.Properties;


public class BoltBuilder {
	
	public Properties configs = null;
	final static Logger logger = Logger.getLogger(BoltBuilder.class);
	
	public BoltBuilder(Properties configs) {
		this.configs = configs;
	}
	
	public JsonParserBolt buildSinkTypeBolt() {
		return new JsonParserBolt();
	}

	public PhoneClickPersistBold phoneClickPersist() {
		String[] cassandraServer = configs.getProperty(Keys.CASSANDRA_SERVERS).split(",");
		int cassandraServerPort = Integer.parseInt(configs.getProperty(Keys.CASSANDRA_SERVER_PORT));
		String cassandraKeyspace = configs.getProperty(Keys.CASSANDRA_KEYSPACE);
		return new PhoneClickPersistBold(cassandraServer, cassandraServerPort, cassandraKeyspace);
	}


}
