package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.hurriyet.emlak.storm.PhoneClickTopology;


import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonParserBolt extends BaseRichBolt {

    final static Logger logger = Logger.getLogger(JsonParserBolt.class);
    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    JSONObject eventJson = null;

    public void execute(Tuple tuple)  {
        try {
            JSONParser parser = new JSONParser();
            eventJson = (JSONObject) parser.parse(tuple.getString(0));
            int nRealtyId = 0;
            int nFirmId = 0;

            if(eventJson.get("RealtyId") != null) {
                nRealtyId = Integer.parseInt(eventJson.get("RealtyId").toString());
            }
            else if (eventJson.get("FirmId") != null){
                nFirmId = Integer.parseInt(eventJson.get("FirmId").toString());
            }
            String sUserIp =  eventJson.get("UserIp").toString();
            String sSessionId =  eventJson.get("SessionId").toString();
            String sDeviceType = eventJson.get("DeviceType").toString();

            collector.emit(PhoneClickTopology.PHONE_CLICK_STREAM,new Values(nRealtyId, nFirmId, sUserIp, sSessionId, sDeviceType));
            collector.ack(tuple);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(PhoneClickTopology.PHONE_CLICK_STREAM, new Fields( "RealtyId","FirmId", "UserIp", "SessionId", "DeviceType"));
    }
}
