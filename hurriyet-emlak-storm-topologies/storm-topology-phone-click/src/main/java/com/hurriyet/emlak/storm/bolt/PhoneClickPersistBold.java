

package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.hurriyet.emlak.storm.PhoneClickTopology;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;


public class PhoneClickPersistBold extends BaseRichBolt {

    final static Logger logger = Logger.getLogger(PhoneClickPersistBold.class);
    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    private static Session session = null;
    private Cluster cluster = null;
    String[] contactPoints;
    int port;
    String keyspace;


    public PhoneClickPersistBold(String[] contactPoints, int port, String keyspace) {
        this.contactPoints = contactPoints;
        this.port = port;
        this.keyspace = keyspace;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        cluster = Cluster.builder().addContactPoints(contactPoints).build();
        session = cluster.connect(keyspace);
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {
//"RealtyId","FirmId", "UserIp", "SessionId", "DeviceType"
        int nRealtyId =  (Integer)tuple.getValueByField("RealtyId");
        int nFirmId = (Integer)tuple.getValueByField("FirmId");
        String sUserIp = (String)tuple.getValueByField("UserIp");
        String sSessionId = (String)tuple.getValueByField("SessionId");
        String sDeviceType = (String)tuple.getValueByField("DeviceType");

        try {
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS+0000");//dd/MM/yyyy
            Date now = new Date();
            String todayDatetime = sdfDate.format(now);
            LocalDate todayDate = LocalDate.now();

//
//            LocalDateTime today = LocalDateTime.now();
//
//            final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
//            today = LocalDateTime.parse(today.toString(), DATE_FORMAT);

            String sLog = String.format("INSERT INTO realty_phone_click_log (pk, realty_id, user_ip, session_id , device_type , visit_date ) " +
                    "values (uuid(), %1$d, '%2$s', '%3$s', '%4$s','%5$s'); ", nRealtyId, sUserIp, sSessionId, sDeviceType, todayDatetime);

            String sTotal = String.format("UPDATE realty_total_interaction_counts SET realty_phone_click_count = realty_phone_click_count + 1 WHERE realty_id=%1$d; ", nRealtyId);
            String sIncrement = String.format("UPDATE realty_interaction_counts SET realty_phone_click_count = realty_phone_click_count + 1 WHERE realty_id=%1$d AND realty_interaction_date ='%2$s'; ", nRealtyId, todayDate);

            StringBuilder sbQuery = new StringBuilder();
            sbQuery.append("BEGIN UNLOGGED BATCH ");
            //sbQuery.append(sLog); //Counter and non-counter mutations cannot exist in the same batch
            sbQuery.append(sTotal);
            sbQuery.append(sIncrement);
            sbQuery.append("APPLY BATCH;");

            session.execute(sLog);
            session.execute(sbQuery.toString());
        } catch (Exception e) {
            logger.error(e);
        }

        //collector.emit(PhoneClickTopology.PHONE_CLICK_STREAM, new Values(nRealtyId, nCategoryId, nDistrictId));
        collector.ack(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
}



