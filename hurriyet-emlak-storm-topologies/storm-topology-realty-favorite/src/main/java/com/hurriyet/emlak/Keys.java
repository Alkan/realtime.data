package com.hurriyet.emlak;


import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;


public class Keys {

	public static final String TOPOLOGY_NAME = "topology";
	public static final String TOPOLOGY_WORKERS_COUNT = "topology.workers.count";

	//kafka spout
	public static final String KAFKA_SPOUT_ID = "kafka-spout";
	public static final String KAFKA_ZOOKEEPERS = "kafka.zookeepers";
	public static final String KAFKA_TOPIC = "kafka.topic";
	public static final String KAFKA_ZKROOT = "kafka.zkRoot";
	public static final String KAFKA_CONSUMER_GROUP = "kafka.consumer.group";
	public static final String KAFKA_SPOUT_COUNT = "kafkaspout.count";
		
	//sink bolt
	public static final String SINK_TYPE_BOLT_ID = "sink-type-bolt";
	public static final String SINK_BOLT_COUNT = "sinkbolt.count";
	
	//solr bolt
	public static final String SOLR_BOLT_ID = "solr-bolt";
	public static final String SOLR_BOLT_COUNT = "solrbolt.count";
	public static final String SOLR_COLLECTION = "solr.collection";
	public static final String SOLR_SERVER = "solr.url";

    //redis bolt
	public static final String REDIS_SERVER = "redis.server";
	public static final String REDIS_SERVER_PORT = "redis.server.port";

	public static final String CASSANDRA_SERVERS = "cassandra.servers";
	public static final String CASSANDRA_SERVER_PORT = "cassandra.server.port";
	public static final String CASSANDRA_KEYSPACE = "cassandra.keyspace";


	public static String getRealtyFavoriteForRedis(int sRealtyId) {
		return String.format("realty:%1$s:favorite:days", sRealtyId);
	}

	public static String getDistrictFavoriteForRedis(int sCategoryId, int sDistrictId) {
		return String.format("category:%1$s:district:%2$s:favorite", sCategoryId, sDistrictId);
	}

	public static int getDayForRedis() {

		LocalDate today = LocalDate.now(); //.plusDays(8);
		LocalDate startday = LocalDate.of(2016, Month.JANUARY, 1);

		return (int) ChronoUnit.DAYS.between(startday, today);
	}
}
