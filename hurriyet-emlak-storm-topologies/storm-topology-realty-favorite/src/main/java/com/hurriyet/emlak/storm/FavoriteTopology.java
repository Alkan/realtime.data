package com.hurriyet.emlak.storm;

import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.bolt.*;
import org.apache.log4j.Logger;
import storm.kafka.KafkaSpout;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;

import java.util.Properties;
import org.apache.commons.cli.*;
import java.text.MessageFormat;

import com.hurriyet.emlak.storm.spout.SpoutBuilder;


public class FavoriteTopology {
	
	public Properties configs;
	public BoltBuilder boltBuilder;
	public SpoutBuilder spoutBuilder;
	public static final String SOLR_STREAM = "solr-stream";
	final static Logger logger = Logger.getLogger(FavoriteTopology.class);


	public static void main(String[] args) throws Exception {

		CommandLine cmdLine = getCommandLine(args);
		String env = cmdLine.hasOption("environment") ? cmdLine.getOptionValue("environment"): "prod";
		String topologyEnvironment = cmdLine.hasOption("topology") ? cmdLine.getOptionValue("topology"): "server";

		String configFile = String.format("/%1$s.config.properties", env);
		logger.info(MessageFormat.format("Config File: {0}", configFile));
		logger.info(MessageFormat.format("FavoriteTopology Environment: {0}", topologyEnvironment));

		FavoriteTopology ingestionTopology = new FavoriteTopology(configFile);
		ingestionTopology.submitTopology(topologyEnvironment);
	}

	public FavoriteTopology(String configFile) throws Exception {
		configs = new Properties();
		try {
			configs.load(FavoriteTopology.class.getResourceAsStream(configFile));
			boltBuilder = new BoltBuilder(configs);
			spoutBuilder = new SpoutBuilder(configs);
		} catch (Exception ex) {
			logger.info(ex);
			System.exit(0);
		}
	}

	private void submitTopology(String topologyEnvironment) throws Exception {
		TopologyBuilder builder = new TopologyBuilder();
		KafkaSpout kafkaSpout = spoutBuilder.buildKafkaSpout();

		JsonParserBolt sinkTypeBolt = boltBuilder.buildSinkTypeBolt();
		FavoriteTotalIncrementBold cassandraTotalIncrementBold = boltBuilder.cassandraTotalIncrement();

		FavoriteIncrementBold redisIncrementBold = boltBuilder.buildRedisIncrementBolt();
		FavoriteScoreComputationBolt realtyScoreComputationBolt = boltBuilder.buildRealtyScoreComputationBolt();
		FavoritePersistentBolt solrBolt = boltBuilder.buildSolrBolt();


		//set the kafkaSpout to topology
		int kafkaSpoutCount = Integer.parseInt(configs.getProperty(Keys.KAFKA_SPOUT_COUNT));
		builder.setSpout("favorite-kafka-spout", kafkaSpout, kafkaSpoutCount);

		int sinkBoltCount = Integer.parseInt(configs.getProperty(Keys.SINK_BOLT_COUNT));
		builder.setBolt("favorite-json-parser",sinkTypeBolt,sinkBoltCount).shuffleGrouping("favorite-kafka-spout");
		builder.setBolt("favorite-total-increment-bolt", cassandraTotalIncrementBold, 1).shuffleGrouping("favorite-json-parser",SOLR_STREAM);
		builder.setBolt("favorite-increment-bolt", redisIncrementBold,1).shuffleGrouping("favorite-total-increment-bolt",SOLR_STREAM);

		builder.setBolt("favorite-score-computation-bolt", realtyScoreComputationBolt,1).shuffleGrouping("favorite-increment-bolt",SOLR_STREAM);
		int solrBoltCount = Integer.parseInt(configs.getProperty(Keys.SOLR_BOLT_COUNT));
		builder.setBolt("favorite-solr-bolt", solrBolt,solrBoltCount).shuffleGrouping("favorite-score-computation-bolt",SOLR_STREAM);


		Config conf = new Config();
		String topologyName = configs.getProperty(Keys.TOPOLOGY_NAME);
		conf.setNumWorkers(Integer.parseInt(configs.getProperty(Keys.TOPOLOGY_WORKERS_COUNT)));

		if (topologyEnvironment.contentEquals("local")) {
			LocalCluster cluster = new LocalCluster();
			conf.setDebug(true);
			cluster.submitTopology(topologyName, conf, builder.createTopology());
		} else {
			StormSubmitter.submitTopology(topologyName, conf, builder.createTopology());
		}
	}


	private static CommandLine getCommandLine(String[] args) throws Exception {
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();

		options.addOption(OptionBuilder.withLongOpt("environment")
				.withDescription("dev or prod")
				.hasArg()
				.withArgName("env")
				.create() );

		options.addOption(OptionBuilder.withLongOpt("topology")
				.withDescription("local or server")
				.hasArg()
				.withArgName("topology")
				.create() );

		return parser.parse(options, args);
	}
}
