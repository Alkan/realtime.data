package com.hurriyet.emlak.storm.bolt;

import com.hurriyet.emlak.Keys;
import org.apache.log4j.Logger;

import java.util.Properties;


public class BoltBuilder {
	
	public Properties configs = null;

	final static Logger logger = Logger.getLogger(BoltBuilder.class);

	public BoltBuilder(Properties configs) {
		this.configs = configs;
	}
	
	public JsonParserBolt buildSinkTypeBolt() {
		return new JsonParserBolt();
	}
	

	public FavoritePersistentBolt buildSolrBolt() {
		String solrServerUlr = configs.getProperty(Keys.SOLR_SERVER);
		String collection = configs.getProperty(Keys.SOLR_COLLECTION);
		FavoritePersistentBolt solrBolt = new FavoritePersistentBolt(solrServerUlr+collection);
		return solrBolt;
	}


	public FavoriteIncrementBold buildRedisIncrementBolt() {
		String redisServer = configs.getProperty(Keys.REDIS_SERVER);
		int redisServerPort = Integer.parseInt(configs.getProperty(Keys.REDIS_SERVER_PORT));

		FavoriteIncrementBold redisBolt = new FavoriteIncrementBold(redisServer, redisServerPort);
		return redisBolt;
	}

	public FavoriteScoreComputationBolt buildRealtyScoreComputationBolt() {
		String redisServer = configs.getProperty(Keys.REDIS_SERVER);
		FavoriteScoreComputationBolt scoreComputationBolt = new FavoriteScoreComputationBolt(redisServer);
		return scoreComputationBolt;
	}

	public FavoriteTotalIncrementBold cassandraTotalIncrement() {
		String[] cassandraServer = configs.getProperty(Keys.CASSANDRA_SERVERS).split(",");
		int cassandraServerPort = Integer.parseInt(configs.getProperty(Keys.CASSANDRA_SERVER_PORT));
		String cassandraKeyspace = configs.getProperty(Keys.CASSANDRA_KEYSPACE);

		return new FavoriteTotalIncrementBold(cassandraServer, cassandraServerPort, cassandraKeyspace);
	}

}
