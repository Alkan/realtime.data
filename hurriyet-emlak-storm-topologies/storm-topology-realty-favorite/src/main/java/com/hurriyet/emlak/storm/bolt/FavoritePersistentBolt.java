package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FavoritePersistentBolt extends BaseRichBolt {

	final static Logger logger = Logger.getLogger(FavoritePersistentBolt.class);
	private static final long serialVersionUID = 1L;
	private OutputCollector collector;
	SolrClient solrClient;
	String solrAddress;


	public FavoritePersistentBolt(String solrAddress) {
		this.solrAddress = solrAddress;
	}
	
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.solrClient = new HttpSolrClient(solrAddress);
	}

	public void execute(Tuple input) {

		SolrInputDocument document = getSolrInputDocumentForInput(input);
		try{
			solrClient.add(document);
			collector.ack(input);
		}catch(Exception e) {
			logger.error(e);
		}
	}


	public SolrInputDocument getSolrInputDocumentForInput(Tuple input) {

		int sRealtyId = input.getIntegerByField("realtyId");
		double nRealtyScore = input.getDoubleByField("realtyScore");

		SolrInputDocument document = new SolrInputDocument();
		Map<String, Object> clsMessageScore = new HashMap<String, Object>();
		clsMessageScore.put("set", nRealtyScore);
		document.addField("RealtyID", Integer.toString(sRealtyId));
		document.addField("RealtyFavoriteScore", clsMessageScore);
		document.addField("_version_", 1);  // Document must exist

		return document;
	}
	
	@Override
	public void cleanup() {
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

}

