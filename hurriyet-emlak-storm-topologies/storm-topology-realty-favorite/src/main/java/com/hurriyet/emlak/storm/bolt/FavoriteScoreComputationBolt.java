package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.FavoriteTopology;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import org.apache.log4j.Logger;

import java.util.Map;


public class FavoriteScoreComputationBolt extends BaseRichBolt
{
    final static Logger logger = Logger.getLogger(FavoriteScoreComputationBolt.class);
    private static final long serialVersionUID = 1L;
    transient RedisConnection<String,String> redis;
    private OutputCollector collector;
    String redisAddress;

    public FavoriteScoreComputationBolt(String redisAddress) {
        this.redisAddress = redisAddress;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        RedisClient client = new RedisClient(redisAddress,6379);
        redis = client.connect();
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");

        String sDistrictFavoriteKey = Keys.getDistrictFavoriteForRedis(nCategoryId, nDistrictId);
        String sRealtyFavoriteKey = Keys.getRealtyFavoriteForRedis(nRealtyId);

        int nDistrictMaxScore =  getDistrictMaxCountInRedis(sDistrictFavoriteKey);
        int nRealtyTotalCount = getRealtyFavoriteTotalCountInRedis(sRealtyFavoriteKey);

        double dRealtyScore = 0;
        if (nRealtyTotalCount > nDistrictMaxScore ){
            // mahalle sayısı sürekli en yüksek ile güncellenecek.
            redis.set(sDistrictFavoriteKey, Integer.toString(nRealtyTotalCount));
            dRealtyScore = 100;
        } else {
            dRealtyScore = (double)(nRealtyTotalCount * 100) / nDistrictMaxScore;
        }

        collector.emit(FavoriteTopology.SOLR_STREAM,new Values(nRealtyId, nCategoryId, nDistrictId, dRealtyScore));
        collector.ack(tuple);
    }

    public int getDistrictMaxCountInRedis(String redisDistrictKey) {
        String sDistrictMaxScore = redis.get(redisDistrictKey);
        if(sDistrictMaxScore == null){
            sDistrictMaxScore = "0";
        }
        return Integer.parseInt(sDistrictMaxScore);
    }

    public int getRealtyFavoriteTotalCountInRedis(String sRealtyFavoriteKey) {

        final int REALTY_FAVORITE_LAST_DAY = 10;
        int nFavoriteTotalCount = 0;
        int nDays = Keys.getDayForRedis();
        int nRealtyVisitLastDayLimit = nDays - REALTY_FAVORITE_LAST_DAY;
        Map<String, String> favorites = redis.hgetall(sRealtyFavoriteKey);

        for (Map.Entry<String, String> favorite : favorites.entrySet()) {
            if (Integer.parseInt(favorite.getKey()) > nRealtyVisitLastDayLimit) {
                nFavoriteTotalCount +=  Integer.parseInt(favorite.getValue());
            }
            else {
                redis.hdel(sRealtyFavoriteKey, favorite.getKey());
            }
        }

        return nFavoriteTotalCount;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(FavoriteTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId", "realtyScore"));
    }
}