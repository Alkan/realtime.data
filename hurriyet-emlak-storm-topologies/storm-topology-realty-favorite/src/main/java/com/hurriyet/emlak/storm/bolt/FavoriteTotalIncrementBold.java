
package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.datastax.driver.core.*;
import com.hurriyet.emlak.storm.FavoriteTopology;
import org.apache.log4j.Logger;

import java.time.format.DateTimeFormatter;
import java.time.LocalDate;
import java.util.Map;

public class FavoriteTotalIncrementBold extends BaseRichBolt
{
    final static Logger logger = Logger.getLogger(FavoriteTotalIncrementBold.class);
    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    private static Session session = null;
    private Cluster cluster = null;
    String[] contactPoints;
    int port;
    String keyspace;


    public FavoriteTotalIncrementBold(String[] contactPoints, int port, String keyspace) {
        this.contactPoints = contactPoints;
        this.port = port;
        this.keyspace = keyspace;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        cluster = Cluster.builder().addContactPoints(contactPoints).build();
        session = cluster.connect(keyspace);
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");
        boolean bFavoriteIsAdded = (Boolean) tuple.getValueByField("favoriteIsAdded");
        String sCreatedDate = (String) tuple.getValueByField("createdDate");

        try {

            String sFavoriteOperator = bFavoriteIsAdded ? "+" : "-";
            LocalDate createdDate = LocalDate.now();

            // Favorilerden kaldırma durumlarında, kullanıcının favoriye eklediği tarihten eksiltme olacak.
            if (bFavoriteIsAdded == false){
                final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                createdDate = LocalDate.parse(sCreatedDate, DATE_FORMAT);
            }

            String sTotal = String.format("UPDATE realty_total_interaction_counts SET realty_favorite_count = realty_favorite_count %1$s 1 WHERE realty_id=%2$d; ",
                    sFavoriteOperator, nRealtyId);
            String sIncrement = String.format("UPDATE realty_interaction_counts SET realty_favorite_count = realty_favorite_count %1$s 1 WHERE realty_id=%2$d AND realty_interaction_date ='%3$s'; ",
                    sFavoriteOperator, nRealtyId, createdDate);

            StringBuilder sbQuery = new StringBuilder();
            sbQuery.append("BEGIN UNLOGGED BATCH ");
            sbQuery.append(sTotal);
            sbQuery.append(sIncrement);
            sbQuery.append("APPLY BATCH;");

            session.execute(sbQuery.toString());

        } catch (Exception e) {
            logger.error(e);
        }

       if (bFavoriteIsAdded) {
           collector.emit(FavoriteTopology.SOLR_STREAM, new Values(nRealtyId, nCategoryId, nDistrictId));
           collector.ack(tuple);
       }
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(FavoriteTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId"));
    }
}



