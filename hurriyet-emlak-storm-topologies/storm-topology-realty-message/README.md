

#### Development Arguments Settings
**Path**: _Edit configuration => Application => MessageTopology => Program  arguments:_

```
-environment dev -topology local 
```

##### Storm Command Line Parameters 

| Parameter Name | Valid Values        | Default Value    |
| -------------- | ------------------- |----------------- |
| -environment   | [dev, prod]         | prod             |
| -topology      | [local, server]     | server           |
| -Dmaven.scope  | [provided] for prod |                  |


##### Topology Deployment: Development

```
$ storm jar ~/storm-repo/realty-message-storm-topology-1.0.jar com.hurriyet.emlak.storm.MessageTopology -environment dev 
```


##### Topology Deployment: Production
```
$ storm jar ~/storm-repo/realty-message-storm-topology-1.0.jar com.hurriyet.emlak.storm.MessageTopology
```