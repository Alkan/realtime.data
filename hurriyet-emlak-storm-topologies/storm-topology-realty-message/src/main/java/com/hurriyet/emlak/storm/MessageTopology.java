package com.hurriyet.emlak.storm;

import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.bolt.*;
import org.apache.log4j.Logger;
import storm.kafka.KafkaSpout;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;

import java.util.Properties;
import org.apache.commons.cli.*;
import java.text.MessageFormat;

import com.hurriyet.emlak.storm.spout.SpoutBuilder;


public class MessageTopology {
	
	public Properties configs;
	public BoltBuilder boltBuilder;
	public SpoutBuilder spoutBuilder;
	public static final String SOLR_STREAM = "solr-stream";
	final static Logger logger = Logger.getLogger(MessageTopology.class);


	public static void main(String[] args) throws Exception {

		CommandLine cmdLine = getCommandLine(args);
		String env = cmdLine.hasOption("environment") ? cmdLine.getOptionValue("environment"): "prod";
		String topologyEnvironment = cmdLine.hasOption("topology") ? cmdLine.getOptionValue("topology"): "server";

		String configFile = String.format("/%1$s.config.properties", env);
		logger.info(MessageFormat.format("Config File: {0}", configFile));
		logger.info(MessageFormat.format("MessageTopology Environment: {0}", topologyEnvironment));

		MessageTopology ingestionTopology = new MessageTopology(configFile);
		ingestionTopology.submitTopology(topologyEnvironment);
	}

	public MessageTopology(String configFile) throws Exception {
		configs = new Properties();
		try {
			configs.load(MessageTopology.class.getResourceAsStream(configFile));
			boltBuilder = new BoltBuilder(configs);
			spoutBuilder = new SpoutBuilder(configs);
		} catch (Exception ex) {
			logger.info(ex);
			System.exit(0);
		}
	}

	private void submitTopology(String topologyEnvironment) throws Exception {
		TopologyBuilder builder = new TopologyBuilder();
		KafkaSpout kafkaSpout = spoutBuilder.buildKafkaSpout();

		JsonParserBolt sinkTypeBolt = boltBuilder.buildSinkTypeBolt();

		MessageTotalIncrementBold cassandraTotalIncrementBold = boltBuilder.cassandraTotalIncrement();
		MessageIncrementBold redisIncrementBold = boltBuilder.buildRedisIncrementBolt();

		MessageScoreComputationBolt realtyScoreComputationBolt = boltBuilder.buildRealtyScoreComputationBolt();
		MessagePersistentBolt solrBolt = boltBuilder.buildSolrBolt();


		//set the kafkaSpout to topology
		int kafkaSpoutCount = Integer.parseInt(configs.getProperty(Keys.KAFKA_SPOUT_COUNT));
		builder.setSpout("message-kafka-spout", kafkaSpout, kafkaSpoutCount);

		int sinkBoltCount = Integer.parseInt(configs.getProperty(Keys.SINK_BOLT_COUNT));
		builder.setBolt("message-json-parser",sinkTypeBolt,sinkBoltCount).shuffleGrouping("message-kafka-spout");
		builder.setBolt("message-total-increment-bolt", cassandraTotalIncrementBold, 1).shuffleGrouping("message-json-parser",SOLR_STREAM);

		builder.setBolt("message-increment-bolt", redisIncrementBold, 1).shuffleGrouping("message-total-increment-bolt",SOLR_STREAM);

		builder.setBolt("message-score-computation-bolt", realtyScoreComputationBolt,1).shuffleGrouping("message-increment-bolt",SOLR_STREAM);
		int solrBoltCount = Integer.parseInt(configs.getProperty(Keys.SOLR_BOLT_COUNT));
		builder.setBolt("message-solr-bolt", solrBolt,solrBoltCount).shuffleGrouping("message-score-computation-bolt",SOLR_STREAM);


		Config conf = new Config();
		String topologyName = configs.getProperty(Keys.TOPOLOGY_NAME);
		//Defines how many worker processes have to be created for the topology in the cluster.

		conf.setNumWorkers(Integer.parseInt(configs.getProperty(Keys.TOPOLOGY_WORKERS_COUNT)));

		if (topologyEnvironment.contentEquals("local")) {
			LocalCluster cluster = new LocalCluster();
			conf.setDebug(true);
			cluster.submitTopology(topologyName, conf, builder.createTopology());
		} else {
			StormSubmitter.submitTopology(topologyName, conf, builder.createTopology());
		}
	}


	private static CommandLine getCommandLine(String[] args) throws Exception {
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();

		options.addOption(OptionBuilder.withLongOpt("environment")
				.withDescription("dev or prod")
				.hasArg()
				.withArgName("env")
				.create() );

		options.addOption(OptionBuilder.withLongOpt("topology")
				.withDescription("local or server")
				.hasArg()
				.withArgName("topology")
				.create() );

		return parser.parse(options, args);
	}
}
