package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.hurriyet.emlak.storm.MessageTopology;


import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonParserBolt extends BaseRichBolt {

    final static Logger logger = Logger.getLogger(JsonParserBolt.class);
    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    JSONObject eventJson = null;

    public void execute(Tuple tuple)  {
        try {
            JSONParser parser = new JSONParser();
            eventJson = (JSONObject) parser.parse(tuple.getString(0));
            int sRealtyId =  Integer.parseInt(eventJson.get("realtyId").toString()) ;
            int sCategoryId = Integer.parseInt(eventJson.get("categoryId").toString());
            int sDistrictId = Integer.parseInt(eventJson.get("districtId").toString());

            collector.emit(MessageTopology.SOLR_STREAM,new Values(sRealtyId, sCategoryId, sDistrictId));
            collector.ack(tuple);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(MessageTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId"));
    }
}
