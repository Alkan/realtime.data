

package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.MessageTopology;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;


public class MessageIncrementBold extends BaseRichBolt
{
    final static Logger logger = Logger.getLogger(MessageIncrementBold.class);
    private static final long serialVersionUID = 1L;
    private transient RedisConnection<String,String> redis;
    private OutputCollector collector;

    String redisAddress;
    int redisPort;

    public MessageIncrementBold(String redisAddress, int redisPort) {
        this.redisAddress = redisAddress;
        this.redisPort = redisPort;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        RedisClient client = new RedisClient(redisAddress,6379);
        redis = client.connect();
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");

        long retMessageValue = redis.hincrby(Keys.getRealtyMessageForRedis(nRealtyId), Integer.toString(Keys.getDayForRedis()), 1);

        if (retMessageValue == 1){
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.DATE, 10);
            redis.expireat(Keys.getRealtyMessageForRedis(nRealtyId), cal.getTime());
        }

        collector.emit(MessageTopology.SOLR_STREAM,new Values(nRealtyId, nCategoryId, nDistrictId));
        collector.ack(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(MessageTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId"));
    }
}



