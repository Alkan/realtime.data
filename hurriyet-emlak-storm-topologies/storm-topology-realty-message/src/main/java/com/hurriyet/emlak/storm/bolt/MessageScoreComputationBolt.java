package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.MessageTopology;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import org.apache.log4j.Logger;

import java.util.Map;


public class MessageScoreComputationBolt extends BaseRichBolt
{
    final static Logger logger = Logger.getLogger(MessageScoreComputationBolt.class);
    private static final long serialVersionUID = 1L;
    transient RedisConnection<String,String> redis;
    private OutputCollector collector;

    String redisAddress;

    public MessageScoreComputationBolt(String redisAddress) {
        this.redisAddress = redisAddress;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        RedisClient client = new RedisClient(redisAddress,6379);
        redis = client.connect();
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");

        String sDistrictKey = Keys.getDistrictMessageForRedis(nCategoryId, nDistrictId);
        String sRealtyKey = Keys.getRealtyMessageForRedis(nRealtyId);

        int nDistrictMaxScore =  getDistrictMaxCountInRedis(sDistrictKey);
        int nRealtyTotalCount = getRealtyMessageTotalCountInRedis(sRealtyKey);

        double dRealtyScore = 0;
        if (nRealtyTotalCount > nDistrictMaxScore ){
            // mahalle sayısı sürekli en yüksek ile güncellenecek.
            redis.set(sDistrictKey, Integer.toString(nRealtyTotalCount));
            dRealtyScore = 100;
        } else {
            dRealtyScore = (double)(nRealtyTotalCount * 100) / nDistrictMaxScore;
        }

        collector.emit(MessageTopology.SOLR_STREAM,new Values(nRealtyId, nCategoryId, nDistrictId, dRealtyScore));
        collector.ack(tuple);
    }

    public int getDistrictMaxCountInRedis(String redisDistrictKey) {

        String sDistrictMaxScore = redis.get(redisDistrictKey);
        if(sDistrictMaxScore == null){
            sDistrictMaxScore = "0";
        }

        return Integer.parseInt(sDistrictMaxScore);
    }

    public int getRealtyMessageTotalCountInRedis(String sRealtyMessageKey) {

        final int REALTY_MESSAGE_LAST_DAY = 10;
        int nMessageTotalCount = 0;
        int nDays = Keys.getDayForRedis();
        int nRealtyVisitLastDayLimit = nDays - REALTY_MESSAGE_LAST_DAY;
        Map<String, String> messages = redis.hgetall(sRealtyMessageKey);

        for (Map.Entry<String, String> message : messages.entrySet()) {
            if (Integer.parseInt(message.getKey()) > nRealtyVisitLastDayLimit) {
                nMessageTotalCount +=  Integer.parseInt(message.getValue());
            }
            else {
                redis.hdel(sRealtyMessageKey, message.getKey());
            }
        }

        return nMessageTotalCount;
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(MessageTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId", "realtyScore"));
    }
}