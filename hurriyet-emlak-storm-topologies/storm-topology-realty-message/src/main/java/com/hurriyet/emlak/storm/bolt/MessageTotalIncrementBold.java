

package com.hurriyet.emlak.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.hurriyet.emlak.storm.MessageTopology;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.util.Map;


public class MessageTotalIncrementBold extends BaseRichBolt {

    final static Logger logger = Logger.getLogger(MessageTotalIncrementBold.class);
    private static final long serialVersionUID = 1L;
    private OutputCollector collector;
    private static Session session = null;
    private Cluster cluster = null;
    String[] contactPoints;
    int port;
    String keyspace;


    public MessageTotalIncrementBold(String[] contactPoints, int port, String keyspace) {
        this.contactPoints = contactPoints;
        this.port = port;
        this.keyspace = keyspace;
    }

    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        cluster = Cluster.builder().addContactPoints(contactPoints).build();
        session = cluster.connect(keyspace);
        collector = outputCollector;
    }

    public void execute(Tuple tuple)  {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");

        try {
            LocalDate today = LocalDate.now();

            String sTotal = String.format("UPDATE realty_total_interaction_counts SET realty_message_count = realty_message_count + 1 WHERE realty_id=%1$d; ", nRealtyId);
            String sIncrement = String.format("UPDATE realty_interaction_counts SET realty_message_count = realty_message_count + 1 WHERE realty_id=%1$d AND realty_interaction_date ='%2$s'; ", nRealtyId, today);

            StringBuilder sbQuery = new StringBuilder();
            sbQuery.append("BEGIN UNLOGGED BATCH ");
            sbQuery.append(sTotal);
            sbQuery.append(sIncrement);
            sbQuery.append("APPLY BATCH;");

            session.execute(sbQuery.toString());
        } catch (Exception e) {
            logger.error(e);
        }

        collector.emit(MessageTopology.SOLR_STREAM, new Values(nRealtyId, nCategoryId, nDistrictId));
        collector.ack(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream(MessageTopology.SOLR_STREAM, new Fields( "realtyId","categoryId", "districtId"));
    }
}



