package com.hurriyet.emlak.storm;

import com.hurriyet.emlak.Keys;
import com.hurriyet.emlak.storm.operations.*;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.tuple.Fields;

import java.time.*;
import java.util.Properties;
import org.apache.commons.cli.*;
import java.text.MessageFormat;

import com.hurriyet.emlak.storm.spout.SpoutBuilder;
import org.apache.log4j.Logger;
import storm.trident.TridentTopology;
import storm.kafka.trident.OpaqueTridentKafkaSpout;


public class VisitTopology {

	final static Logger logger = Logger.getLogger(VisitTopology.class);
	
	private Properties configs;
	private OperationBuilder operationBuilder;
	private SpoutBuilder spoutBuilder;


	public static void main(String[] args) throws Exception {

		CommandLine cmdLine = getCommandLine(args);
		String env = cmdLine.hasOption("environment") ? cmdLine.getOptionValue("environment"): "prod";
		String topologyEnvironment = cmdLine.hasOption("topology") ? cmdLine.getOptionValue("topology"): "server";

		String configFile = String.format("/%1$s.config.properties", env);
		System.out.println(MessageFormat.format("Config File: {0}", configFile));
		System.out.println(MessageFormat.format("VisitTopology Environment: {0}", topologyEnvironment));


		ZoneId istanbul = ZoneId.of("Asia/Istanbul");
		ZonedDateTime zonedDateTime = ZonedDateTime.now(istanbul);
		LocalDateTime localDateTime = zonedDateTime.toLocalDateTime();
		LocalDate localDate = localDateTime.toLocalDate(); // 2014-04-05
		LocalTime localTime = localDateTime.toLocalTime(); // 22:33:16.331

		logger.info("Date: " + localDate);
		logger.info("Time: " + localTime);

		VisitTopology ingestionTopology = new VisitTopology(configFile);
		ingestionTopology.submitTopology(topologyEnvironment);

	}

	public VisitTopology(String configFile) throws Exception {
		configs = new Properties();
		try {
			configs.load(VisitTopology.class.getResourceAsStream(configFile));
			operationBuilder = new OperationBuilder(configs);
			spoutBuilder = new SpoutBuilder(configs);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.exit(0);
		}
	}

	private void submitTopology(String topologyEnvironment) throws Exception {

		TridentTopology topology = new TridentTopology();

		OpaqueTridentKafkaSpout kafkaSpout = spoutBuilder.KafkaSpout();
		RealtyKeyParserFunction realtyKeyParserFunction = operationBuilder.realtyKeyParser();
		RealtyVisitIncrementFilter redisIncrementFilter = operationBuilder.redisIncerement();
		RealtyVisitTotalIncrementFilter  cassandraTotalIncrementFilter = operationBuilder.cassandraTotalIncrement();
		RealtyScoreComputationFunction realtyScoreComputationFunction = operationBuilder.realtyScoreComputation();
		SolrStateFactory solrStateFactory = operationBuilder.solrStateFactory();


		topology.newStream("kafka-stream", kafkaSpout)
				.parallelismHint(3)
				.each(kafkaSpout.getOutputFields(), new RealtyJsonParserFunction(), new Fields("realtyKey"))
				.groupBy(new Fields("realtyKey"))
				.aggregate(new Fields("realtyKey"), new RealtyAggregator(), new Fields("key", "count"))
				.each(new Fields("key","count"), realtyKeyParserFunction, new Fields( "realtyId","categoryId", "districtId"))
				.each(new Fields("realtyId","count"), cassandraTotalIncrementFilter)
				.each(new Fields("realtyId","count", "categoryId"), redisIncrementFilter)
				.each(new Fields("realtyId","categoryId", "districtId"), realtyScoreComputationFunction, new Fields( "realtyScore"))
		        .partitionPersist(solrStateFactory, new Fields("realtyId", "realtyScore"), new SolrStateUpdater());


		Config config = new Config();
		config.put("transactional.zookeeper.root", configs.getProperty(Keys.KAFKA_ZKROOT));
		String topologyName = configs.getProperty(Keys.TOPOLOGY_NAME);
		config.setNumWorkers(Integer.parseInt(configs.getProperty(Keys.TOPOLOGY_WORKERS_COUNT)));

		if (topologyEnvironment.contentEquals("local")) {
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology(topologyName, config, topology.build());

		} else {
			StormSubmitter.submitTopology(topologyName, config, topology.build());
		}
	}

	private static CommandLine getCommandLine(String[] args) throws Exception {
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();

		options.addOption(OptionBuilder.withLongOpt("environment")
				.withDescription("dev or prod")
				.hasArg()
				.withArgName("env")
				.create() );

		options.addOption(OptionBuilder.withLongOpt("topology")
				.withDescription("local or server")
				.hasArg()
				.withArgName("topology")
				.create() );

		return parser.parse(options, args);
	}






}




