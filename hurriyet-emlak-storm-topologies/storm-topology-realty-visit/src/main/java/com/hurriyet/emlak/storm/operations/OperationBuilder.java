package com.hurriyet.emlak.storm.operations;

import com.hurriyet.emlak.Keys;
import java.util.Properties;


public class OperationBuilder {
	
	public Properties configs = null;
	
	public OperationBuilder(Properties configs) {
		this.configs = configs;
	}


	public  SolrStateFactory solrStateFactory() {
		String solrServerUlr = configs.getProperty(Keys.SOLR_SERVER);
		String collection = configs.getProperty(Keys.SOLR_COLLECTION);
		return new SolrStateFactory(solrServerUlr+collection);
	}

	public RealtyVisitIncrementFilter redisIncerement() {
		String redisServer = configs.getProperty(Keys.REDIS_SERVER);
		int redisServerPort = Integer.parseInt(configs.getProperty(Keys.REDIS_SERVER_PORT));
		return new RealtyVisitIncrementFilter(redisServer,redisServerPort);
	}

	public RealtyKeyParserFunction realtyKeyParser() {
	return 	new RealtyKeyParserFunction();
	}

	public RealtyScoreComputationFunction realtyScoreComputation() {
		String redisServer = configs.getProperty(Keys.REDIS_SERVER);
		int redisServerPort = Integer.parseInt(configs.getProperty(Keys.REDIS_SERVER_PORT));
		return new RealtyScoreComputationFunction(redisServer, redisServerPort);
	}


	public RealtyVisitTotalIncrementFilter cassandraTotalIncrement() {
		String[] cassandraServer = configs.getProperty(Keys.CASSANDRA_SERVERS).split(",");
		int cassandraServerPort = Integer.parseInt(configs.getProperty(Keys.CASSANDRA_SERVER_PORT));
		String cassandraKeyspace = configs.getProperty(Keys.CASSANDRA_KEYSPACE);
		return new RealtyVisitTotalIncrementFilter(cassandraServer, cassandraServerPort, cassandraKeyspace);
	}

}
