package com.hurriyet.emlak.storm.operations;

import backtype.storm.tuple.Values;
import org.apache.commons.collections.MapUtils;
import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ahmet on 19/06/16.
 */
public class RealtyAggregator extends BaseAggregator<Map<String, Integer>> {

    private static final long serialVersionUID = 1L;

    public Map<String, Integer> init(Object batchId, TridentCollector collector) {
        return new HashMap<String, Integer>();
    }

    public void aggregate(Map<String, Integer> val, TridentTuple tuple, TridentCollector collector) {
        String location = tuple.getString(0);
        val.put(location, MapUtils.getInteger(val, location, 0) + 1);
    }

    public void complete(Map<String, Integer> val, TridentCollector collector) {

        for (Map.Entry<String, Integer> entry : val.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            collector.emit(new Values(key, value));
        }
    }
}
