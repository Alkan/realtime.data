package com.hurriyet.emlak.storm.operations;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;
import backtype.storm.tuple.Values;

public class RealtyJsonParserFunction extends BaseFunction {

    final static Logger logger = Logger.getLogger(RealtyJsonParserFunction.class);
    private static final long serialVersionUID = 1L;
    JSONObject eventJson = null;

    public void prepare(java.util.Map conf, TridentOperationContext context){

    }

    public void execute(TridentTuple tuple, TridentCollector collector) {

        try {

            JSONParser parser = new JSONParser();
            eventJson = (JSONObject) parser.parse(tuple.getString(0));
            int sRealtyId =  Integer.parseInt(eventJson.get("realtyId").toString()) ;
            int sCategoryId = Integer.parseInt(eventJson.get("categoryId").toString());
            int sDistrictId = Integer.parseInt(eventJson.get("districtId").toString());

            collector.emit(new Values(sRealtyId +"-"+ sCategoryId +"-" + sDistrictId));

        } catch (Exception e) {
            logger.error(e);
        }

    }
}