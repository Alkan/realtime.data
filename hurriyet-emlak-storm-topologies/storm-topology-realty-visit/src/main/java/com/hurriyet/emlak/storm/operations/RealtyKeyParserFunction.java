

package com.hurriyet.emlak.storm.operations;

import backtype.storm.tuple.Values;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;


public class RealtyKeyParserFunction extends BaseFunction
{

    final static Logger logger = Logger.getLogger(RealtyScoreComputationFunction.class);
    private static final long serialVersionUID = 1L;

    public void execute(TridentTuple tuple, TridentCollector tridentCollector) {

        String key = (String) tuple.getValueByField("key");
        int nCount = (Integer)tuple.getValueByField("count");

        String[] parts = key.split("-");
        int nRealtyId =  Integer.parseInt(parts[0]);
        int nCategoryId = Integer.parseInt(parts[1]);
        int nDistrictId = Integer.parseInt(parts[2]);

        //System.out.println(String.format("Increment: %1$s", nRealtyId));
       tridentCollector.emit(new Values(nRealtyId, nCategoryId, nDistrictId, nCount));
    }

}



