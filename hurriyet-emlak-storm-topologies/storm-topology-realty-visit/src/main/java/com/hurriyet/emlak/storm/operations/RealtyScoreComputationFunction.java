package com.hurriyet.emlak.storm.operations;

import backtype.storm.tuple.Values;
import com.hurriyet.emlak.Keys;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;
import java.util.Map;


public class RealtyScoreComputationFunction extends BaseFunction {

    // place holder to keep the connection to redis
    transient RedisConnection<String,String> redis;
    final static Logger logger = Logger.getLogger(RealtyScoreComputationFunction.class);
    private static final long serialVersionUID = 1L;

    String redisAddress;
    int redisPort;

    public RealtyScoreComputationFunction(String redisAddress, int redisPort) {
        this.redisAddress = redisAddress;
        this.redisPort = redisPort;
    }

    public void prepare(java.util.Map conf, TridentOperationContext context){
        RedisClient client = new RedisClient(redisAddress,redisPort);
        redis = client.connect();
    }

    public void execute(TridentTuple tuple, TridentCollector collector) {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");
        int nDistrictId = (Integer)tuple.getValueByField("districtId");

        String sDistrictKey = Keys.getDistrictForRedis(nCategoryId, nDistrictId);
        String sRealtyKey = Keys.getRealtyForRedis(nRealtyId);

        int nDistrictMaxScore =  getDistrictMaxCountInRedis(sDistrictKey);
        int nRealtyTotalCount = getRealtyTotalCountInRedis(sRealtyKey);

        double dRealtyScore = 0;
        if (nRealtyTotalCount > nDistrictMaxScore ){
            // mahalle sayısı sürekli en yüksek ile güncellenecek.
            redis.set(sDistrictKey, Integer.toString(nRealtyTotalCount));
            dRealtyScore = 100;
        } else {
            dRealtyScore = (double)(nRealtyTotalCount * 100) / nDistrictMaxScore;
        }

        //logger.info(nRealtyId);
        //logger.info(String.format("Computation: district max=%1$s realty total= %2$s score= %3$s", nDistrictMaxScore, nRealtyTotalCount, dRealtyScore));

        collector.emit(new Values(dRealtyScore));
    }

    public int getDistrictMaxCountInRedis(String redisDistrictKey) {

        String sDistrictMaxScore = redis.get(redisDistrictKey);
        if(sDistrictMaxScore == null){
            sDistrictMaxScore = "0";
        }

        return Integer.parseInt(sDistrictMaxScore);
    }

    public int getRealtyTotalCountInRedis(String sRealtyKey) {

        int nVizitTotalCount = 0;
        final int REALTY_VISIT_LAST_DAY = 7;

        Map<String, String> mapRealtyVisit = redis.hgetall(sRealtyKey);

        int nDays = Keys.getDayForRedis();

        int nRealtyVisitLastDayLimit = nDays - REALTY_VISIT_LAST_DAY;

        for (Map.Entry<String, String> entry : mapRealtyVisit.entrySet()) {

            int nKey = Integer.parseInt(entry.getKey());

            if (nKey > nRealtyVisitLastDayLimit){
                nVizitTotalCount +=  Integer.parseInt(entry.getValue());
            }
            else {
                redis.hdel(sRealtyKey, entry.getKey());
            }
        }

        return nVizitTotalCount;
    }

}