package com.hurriyet.emlak.storm.operations;

import com.hurriyet.emlak.Keys;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Calendar;
import java.util.Date;

public class RealtyVisitIncrementFilter implements Filter
{
    private static final long serialVersionUID = 1L;
    transient RedisConnection<String,String> redis;

    String redisAddress;
    int redisPort;

    public RealtyVisitIncrementFilter(String redisAddress, int redisPort) {
        this.redisAddress = redisAddress;
        this.redisPort = redisPort;
    }

    public void prepare(java.util.Map conf, TridentOperationContext context){
        RedisClient client = new RedisClient(redisAddress, redisPort);
        redis = client.connect();
    }

    public boolean isKeep(TridentTuple tuple) {

        int nRealtyId =  (Integer)tuple.getValueByField("realtyId");
        int nCount = (Integer)tuple.getValueByField("count");
        int nCategoryId = (Integer)tuple.getValueByField("categoryId");

        Boolean bIsRedisIncrement = true;

//        switch (nCategoryId) {
//            case 1000020301 : // Turistik İşletme Satılık
//                bIsRedisIncrement = true;
//                break;
//            case 1000020302 : // Turistik İşletme Kiralık
//                bIsRedisIncrement = true;
//                break;
//            case 1000020201 : // Arsa Satılık
//                bIsRedisIncrement = true;
//                break;
//            case 1000020202 : // Arsa Kiralık
//                bIsRedisIncrement = true;
//                break;
//            case 1000020102 : // Konut Kiralık
//                bIsRedisIncrement = true;
//                break;
//            /*case 1000010101 : // !!!Konut Satılık silinecek
//                bIsRedisIncrement = true;
//                break;*/
//        }

        if (bIsRedisIncrement){
            long retValue = redis.hincrby(Keys.getRealtyForRedis(nRealtyId), Integer.toString(Keys.getDayForRedis()), nCount);
            if (retValue == 1){
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.DATE, 7);
                redis.expireat(Keys.getRealtyForRedis(nRealtyId), cal.getTime());
            }
        }

        return bIsRedisIncrement;
    }

    public void cleanup() {

    }

}



