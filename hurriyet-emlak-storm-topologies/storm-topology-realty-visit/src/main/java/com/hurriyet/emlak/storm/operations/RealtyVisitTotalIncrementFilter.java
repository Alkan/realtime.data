package com.hurriyet.emlak.storm.operations;

import com.datastax.driver.core.*;
import org.apache.log4j.Logger;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.time.*;
import java.time.LocalDate;


public class RealtyVisitTotalIncrementFilter implements Filter
{
    private static final long serialVersionUID = 1L;
    final static Logger logger = Logger.getLogger(RealtyVisitTotalIncrementFilter.class);
    private static Session session = null;
    private Cluster cluster = null;
    String[] contactPoints;
    int port;
    String keyspace;


    public RealtyVisitTotalIncrementFilter(String[] contactPoints, int port, String keyspace) {
        this.contactPoints = contactPoints;
        this.port = port;
        this.keyspace = keyspace;
    }

    public void prepare(java.util.Map conf, TridentOperationContext context){
        cluster = Cluster.builder().addContactPoints(contactPoints).build();
        session = cluster.connect(keyspace);
    }

    public boolean isKeep(TridentTuple tuple) {

        try {
            int realtyId =  (Integer)tuple.getValueByField("realtyId");
            long count = Long.valueOf((Integer)tuple.getValueByField("count"));

            LocalDate today = LocalDate.now();

            String sTotal = String.format("UPDATE realty_total_interaction_counts SET realty_visit_count = realty_visit_count + %1$d WHERE realty_id=%2$d; ", count, realtyId);
            String sIncrement = String.format("UPDATE realty_interaction_counts SET realty_visit_count = realty_visit_count +  %1$d  WHERE realty_id=%2$d AND realty_interaction_date ='%3$s'; ", count, realtyId, today);

            StringBuilder sbQuery = new StringBuilder();
            sbQuery.append("BEGIN UNLOGGED BATCH ");
            sbQuery.append(sTotal);
            sbQuery.append(sIncrement);
            sbQuery.append("APPLY BATCH;");

            session.execute(sbQuery.toString());

        } catch (Exception e) {
            logger.error(e);
        }

        return true;
    }

    public void cleanup() {

    }

}