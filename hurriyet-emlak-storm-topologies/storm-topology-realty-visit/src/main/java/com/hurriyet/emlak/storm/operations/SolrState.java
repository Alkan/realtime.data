package com.hurriyet.emlak.storm.operations;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;
import storm.trident.state.State;

import java.util.ArrayList;
import java.util.Collection;

public class SolrState implements State {

    final static Logger logger = Logger.getLogger(SolrState.class);
    SolrClient solrClient;

    public SolrState(String solrAddress) {
        this.solrClient = new HttpSolrClient(solrAddress);
    }

    public void beginCommit(Long txid) {
    }

    public void commit(Long txid) {
    }

    public void setScoreBulk(Collection<SolrInputDocument> lstDocument) {

        try {
            if (lstDocument.size() > 0){
                //logger.info(lstDocument.size());
                solrClient.add(lstDocument);
            }
        }
        catch(Exception e) {

            String sErrorMessage = e.getMessage();
            String[] arrRealtyId = sErrorMessage.split("id=", 2);

            if (arrRealtyId != null && arrRealtyId.length == 2) {
                reSetSolrDocument(lstDocument, arrRealtyId[1]);
                logger.error(sErrorMessage);
            }
            else{
                logger.error(e);
            }
        }
    }


    private void reSetSolrDocument(Collection<SolrInputDocument> lstDocument, String errorId){

        boolean bAddItem = false;
        Collection<SolrInputDocument> lstNewDocument = new ArrayList<SolrInputDocument>();

        //logger.error("**"+ errorId);
        for(SolrInputDocument document: lstDocument) {
            String sRealtyId = document.getFieldValue("RealtyID").toString();

            if (bAddItem == false){
                bAddItem = sRealtyId.equals(errorId);
            }

            if (bAddItem && !sRealtyId.equals(errorId)){
                lstNewDocument.add(document);
                //logger.error(sRealtyId);
            }
        }

        setScoreBulk(lstNewDocument);
    }

}