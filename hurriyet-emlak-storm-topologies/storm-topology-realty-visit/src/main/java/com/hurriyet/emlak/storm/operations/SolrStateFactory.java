package com.hurriyet.emlak.storm.operations;

import backtype.storm.task.IMetricsContext;
import storm.trident.state.State;
import storm.trident.state.StateFactory;

import java.util.Map;

public class SolrStateFactory implements StateFactory {

    private static final long serialVersionUID = 1L;
    String solrAddress;

    public SolrStateFactory(String solrAddress) {
        this.solrAddress = solrAddress;
    }

    public State makeState(Map map, IMetricsContext iMetricsContext, int i, int i1) {
        return new SolrState(solrAddress);
    }
}