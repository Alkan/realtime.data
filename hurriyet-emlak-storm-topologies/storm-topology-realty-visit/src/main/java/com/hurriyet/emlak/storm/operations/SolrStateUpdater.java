package com.hurriyet.emlak.storm.operations;

import org.apache.solr.common.SolrInputDocument;
import storm.trident.operation.TridentCollector;
import storm.trident.state.BaseStateUpdater;
import storm.trident.tuple.TridentTuple;

import java.util.*;

public class SolrStateUpdater extends BaseStateUpdater<SolrState> {

    private static final long serialVersionUID = 1L;

    public void updateState(SolrState state, List<TridentTuple> tuples, TridentCollector collector) {

        Collection<SolrInputDocument> lstDocument = new ArrayList<SolrInputDocument>();

        for(TridentTuple t: tuples) {
            lstDocument.add(getSolrInputDocumentForInput(t));
        }

        state.setScoreBulk(lstDocument);
    }

    public SolrInputDocument getSolrInputDocumentForInput(TridentTuple input) {

        int sRealtyId = input.getIntegerByField("realtyId");
        double nRealtyScore = input.getDoubleByField("realtyScore");

        SolrInputDocument document = new SolrInputDocument();
        Map<String, Object> clsRealtyVisitScore = new HashMap<String, Object>();
        clsRealtyVisitScore.put("set", nRealtyScore);
        document.addField("RealtyID", Integer.toString(sRealtyId));
        document.addField("RealtyVisitScore", clsRealtyVisitScore);
        document.addField("_version_", 1);  // Document must exist


        return document;
    }
}