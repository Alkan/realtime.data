package com.hurriyet.emlak.storm.spout;

import com.hurriyet.emlak.Keys;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;
import java.util.Properties;
import backtype.storm.spout.SchemeAsMultiScheme;
import storm.kafka.trident.OpaqueTridentKafkaSpout;
import storm.kafka.trident.TridentKafkaConfig;


public class SpoutBuilder {
	
	public Properties configs = null;
	
	public SpoutBuilder(Properties configs) {
		this.configs = configs;
	}

	public OpaqueTridentKafkaSpout KafkaSpout() {
		BrokerHosts hosts = new ZkHosts(configs.getProperty(Keys.KAFKA_ZOOKEEPERS));
		String topic = configs.getProperty(Keys.KAFKA_TOPIC);
		String zkRoot = configs.getProperty(Keys.KAFKA_ZKROOT);
		String groupId = configs.getProperty(Keys.KAFKA_CONSUMERGROUP);
		//SpoutConfig spoutConfig = new SpoutConfig(hosts, topic, zkRoot, groupId);
		//KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

		TridentKafkaConfig spoutConfig = new TridentKafkaConfig(hosts, topic, groupId);

		//spoutConfig.fetchMaxWait = 900000000;

		// Bir defada okunacak varsayılan veri boyutu 1 MB
        // 19400 mesaj 1.25 MB yapıyor.
		//spoutConfig.fetchSizeBytes = 1048576;
		//spoutConfig.fetchSizeBytes = 100;

		//spoutConfig.foforceFromStart = true;
		spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
		OpaqueTridentKafkaSpout spout = new OpaqueTridentKafkaSpout(spoutConfig);
		return spout;
	}
}
